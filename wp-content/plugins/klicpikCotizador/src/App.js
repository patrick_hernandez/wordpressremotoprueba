import React from 'react';
import CotizadorKlicpik from './components/CotizadorKlicpik';

function App() {
    return(
        <React.Fragment>
            <CotizadorKlicpik />
        </React.Fragment>
    )
}
export default App;