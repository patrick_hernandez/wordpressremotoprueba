import React, { useState, useEffect } from 'react';
// Importacion de estilos del componente
import './FormKlicpiks.css';
// Importacion de BootstrapReact
import { Form, Button, Tooltip, FormControl } from 'react-bootstrap';
// Importacion de imagenes
import imagPallet from "../../../images/icono__pallet.svg";
import imageCarton from "../../../images/icono__carton.svg";
import imageCartonPallet from "../../../images/icono__combo.svg";
// Importaciones de componentes
import ModalMail from '../ModalMail/ModalMail';
// Importacion de axios para las peticiones a las APIs
import axios from 'axios';

const FormKlicpik = ( { adminValoresCotizacion } ) => {

    // Estados de la aplicacion
    const [valorSku, seValorSku]                                    = useState( '' );
    const [valorPallet, setValorPallet]                             = useState( '' );
    const [valorCarton, setValorCarton]                             = useState( '' );
    const [valorEntrada, setValorEntrada]                           = useState( '' );
    const [valorSalida, setValorSalida]                             = useState( '' );
    const [showModalMail, setShowModalMail]                         = useState( false );
    const [inputsCombo, setInputsCombo]                             = useState( false );
    const [radioSeleccionado, setRadioSeleccionado]                 = useState( null );
    const [adminValoresRanges, setAdminValoresRanges]               = useState( null );
    const [alertValorSku, setalertValorSku]                         = useState( false );
    const [alertValorPallet, setalertValorPallet]                   = useState( false );
    const [alertValorCarton, setalertValorCarton]                   = useState( false );
    const [alertValorEntrada, setalertValorEntrada]                 = useState( false );
    const [alertValorSalida, setalertValorSalida]                   = useState( false );
    const [alertaNoDatos, setAlertaNoDatos]                         = useState( false );
    const [cotizacionAlmacenajeTotal, setCotizacionAlmacenajeTotal] = useState( null );
    const [cotizacionRadiosTotal, setCotizacionRadiosTotal]         = useState( null );
    const [cotizacionRadiosPallet, setCotizacionRadiosPallet]       = useState( null );
    const [cotizacionRadiosCarton, setCotizacionRadiosCarton]       = useState( null );

    // Peticiones APIs
    const apiBase                     = appLocalizer.apiUrl.replace("wp-json", "?rest_route=");
    const valoresMaxRanges            = `${ apiBase }/wprk/klicpik/v1/ranges_quote`;

    useEffect( () => {
        peticionesApis();
    }, []);

    useEffect( () => {
        operacionesCotizacion();
    }, [ valorSku, valorPallet, valorCarton, valorEntrada, valorSalida ])

    // Funcion que se encarga de enlistar las peticiones que se haran mediante axios a las APIs
    const peticionesApis = async () => {

        let valoresRanges = await axios.get( valoresMaxRanges );
        await setAdminValoresRanges( valoresRanges.data[0] );

    }

    // Funcion para obtener el valor del range y despues imprimirlo
    const valoresRange = ( valorRange, valor ) => {

        if ( valor === 0 ) {
            setalertValorSku( false );
            seValorSku( valorRange );
            // operacionesCotizacion(); 
        } else if ( valor === 1 ) {
            setalertValorPallet( false )
            setValorPallet( valorRange );
        } else if ( valor === 2 ) {
            setalertValorCarton( false );
            setValorCarton( valorRange );
            // operacionesCotizacion(); 
        } else if ( valor === 3 ) {
            setalertValorEntrada( false );
            setValorEntrada( valorRange );
            // operacionesCotizacion(); 
        } else if ( valor === 4 ) {
            setalertValorSalida( false );
            setValorSalida( valorRange );
            // operacionesCotizacion(); 
        }

    }

    //  Funcion que captura el valor ingresado en los inputs y valida que sean los datos inferiores o iguales a
    //  los ingresados en el panel de administracion del plugin en WP
    const valoresInputs = ( e ) => {
        
        e.preventDefault();
        let valorObenido = e.target.value;
        let nombreValor = e.target.name

        if ( nombreValor == 'sku' ) {
            seValorSku( valorObenido );
            if ( Number( valorObenido ) > Number( adminValoresRanges.num_sku ) || Number( valorObenido ) == 0 ) {
                setalertValorSku( true );
            } else {
                setalertValorSku( false );
            }
        } else if ( nombreValor == 'pallet' ) {
            setValorPallet( valorObenido );
            if ( Number( valorObenido ) > Number( adminValoresRanges.promedio_pallet ) || Number( valorObenido ) == 0 ) {
                setalertValorPallet( true );
            } else {
                setalertValorPallet( false );
            }
        } else if ( nombreValor == 'carton' ) {
            setValorCarton( valorObenido );
            if ( Number( valorObenido ) > Number( adminValoresRanges.promedio_carton ) || Number( valorObenido ) == 0 ) {
                setalertValorCarton( true );
            } else {
                setalertValorCarton( false );
            }
        } else if ( nombreValor == 'entrada' ) {
            setValorEntrada( valorObenido )
            if ( Number( valorObenido ) > Number( adminValoresRanges.max_entrada ) || Number( valorObenido ) == 0 ) {
                setalertValorEntrada( true );
            } else {
                setalertValorEntrada( false );
            }
        } else if ( nombreValor == 'salida' ) {
            setValorSalida( valorObenido );
            if ( Number( valorObenido ) > Number( adminValoresRanges.max_salida ) || Number( valorObenido ) == 0 ) {
                setalertValorSalida( true );
            } else {
                setalertValorSalida( false );
            }
        }

    }

    // Funcion que evalua que opcion ha sido seleccionada en los radios
    const inputsRadioSeleccionado = async ( radioSeleccionado ) => {

        setRadioSeleccionado( radioSeleccionado );

        if ( Number( radioSeleccionado ) === 3 ) {

            setInputsCombo( true );

        }

    }

    const operacionesCotizacion = async () => {
        
        // Pallet => 1
        // Carton => 2
        // Combo  => 3

        if ( Number( radioSeleccionado ) === 1 ) {

            let cotizacionSkuPallet = ( Number( adminValoresCotizacion.pallet ) * Number( valorSku ) );
            await setCotizacionRadiosTotal( cotizacionSkuPallet );

        } else if ( Number( radioSeleccionado ) === 2 ) {

            let cotizacionSkuCarton = Number( adminValoresCotizacion.carton_flow ) * Number( valorSku );
            await setCotizacionRadiosTotal( cotizacionSkuCarton );

        } else if ( Number( radioSeleccionado ) === 3 ) {

            let cotizacionMediaCarton = ( Number( valorCarton ) * Number( adminValoresCotizacion.carton_flow ) );
            let cotizacionMediaPallet = ( Number( valorPallet ) * Number( adminValoresCotizacion.pallet ) );
            let cotizacionCombo = ( cotizacionMediaCarton + cotizacionMediaPallet );
            await setCotizacionRadiosTotal( cotizacionCombo );
            await setCotizacionRadiosPallet( cotizacionMediaPallet );
            await setCotizacionRadiosCarton( cotizacionMediaCarton );

        }

        let cotizacionAlmacenaje = ( Number( valorEntrada ) * Number( adminValoresCotizacion.entrada ) + ( Number( valorSalida ) * Number( adminValoresCotizacion.salida ) ) );
        await setCotizacionAlmacenajeTotal( cotizacionAlmacenaje );

    }

    // Funcion para abrir el modal para ingresar el nombre de usuario y el correo para enviar la cotizacion
    const openModalMail = () => {

        if ( inputsCombo ) {
            if (
                radioSeleccionado != null  &&
                alertValorSku     == false &&
                alertValorPallet  == false &&
                alertValorCarton  == false &&
                alertValorEntrada == false &&
                alertValorSalida  == false &&
                valorSku          != ''    &&
                valorPallet       != ''    &&
                valorCarton       != ''    &&
                valorEntrada      != ''    &&
                valorSalida       != ''
            ) {
                setAlertaNoDatos( false );
                setShowModalMail( true );
            } else {
                setAlertaNoDatos( true );
            }
        } else {
            if (
                radioSeleccionado != null  &&
                alertValorSku     == false &&
                alertValorEntrada == false &&
                alertValorSalida  == false &&
                valorSku          != ''    &&
                valorEntrada      != ''    &&
                valorSalida       != ''
            ) {
                setAlertaNoDatos( false );
                setShowModalMail( true );
            } else {
                setAlertaNoDatos( true );
            }
        }
    }

    const onlyNumeric = (e) => {
        let value = e.key;
        if (/^[0-9]*$/.test(value) || Number(value) || value ==='') {
            return true;
        } else {
            e.preventDefault();
            return false;
        }
    }

    return (
        <React.Fragment>
            <Form>
                <Form.Group>
                    <p className="form__titulo">Necesidades</p>
                    <br />
                    <div className="form__contenedor-check">
                        <img className="contenedor-check__img" src={ imagPallet } alt="Icono de pallet"/>
                        <Form.Check
                            inline
                            name="group1"
                            type="radio"
                            id="1"
                            onChange={ ( e ) => inputsRadioSeleccionado( e.target.id ) }
                        />
                        <span className="contenedor-check__texto">
                            Posiciones de pallets con medidas 1.10 X 1.20 X 1.50&nbsp; 
                            <span className="texto-mini">
                                (ideal venta&nbsp; 
                                <span
                                    data-toggle="tooltip" 
                                    data-placement="right" 
                                    title="FBA: Amazon se encarga de la recepción, el embalaje, el envío, el servicio al cliente y las devoluciones."
                                >
                                    FBA&nbsp;
                                </span>
                                , productos voluminosos e inventarios altos)
                            </span>
                        </span>
                    </div>
                    <div className="form__contenedor-check">
                        <img className="contenedor-check__img" src={ imageCarton } alt="Icono de pallet" />
                        <Form.Check
                            inline
                            name="group1"
                            type="radio"
                            id="2"
                            onChange={ ( e ) => inputsRadioSeleccionado( e.target.id ) }
                        />
                        <span className="contenedor-check__texto">
                            Posiciones Carton Flow&nbsp;
                            <span className="texto-mini">
                                (ideal productos pequeños, de alta rotación, venta en línea o&nbsp;
                                <span
                                    data-toggle="tooltip" 
                                    data-placement="right" 
                                    title="FBM: los el vendedor en Amazon se encarga de la recepción, el embalaje, el envío el sergicio al cliente y las devoluciones."
                                >
                                    FBM&nbsp;
                                </span>)
                            </span>
                            , de una a 7 cajas, dependiendo de la velocidad de rotación.
                        </span>
                    </div>
                    <div className="form__contenedor-check">
                        <img className="contenedor-check__img" src={ imageCartonPallet } alt="Icono de pallet" />
                        <Form.Check
                            inline
                            name="group1"
                            type="radio"
                            id="3"
                            onChange={ ( e ) => inputsRadioSeleccionado( e.target.id ) }
                        />
                        <span className="contenedor-check__texto">
                            Ambas (mix canales de distribución y productos).
                        </span>
                    </div>
                </Form.Group>
                <br />
                <p className="form__titulo">Posiciones</p>
                <br />
                <Form.Group className="mb-3">
                    <label className="form__label">Número de SKUs</label>
                    <div className="form__contenedor-range">
                        <Form.Range 
                            min="1" 
                            max={ adminValoresRanges != null ? adminValoresRanges.num_sku : 100 } 
                            step="1" 
                            name="sku" 
                            onChange={ ( e ) => valoresRange( e.target.value, 0 ) } 
                        />
                        <FormControl 
                            type="phone"
                            aria-label="Small" 
                            aria-describedby="inputGroup-sizing-sm"
                            name="sku"
                            value={ valorSku }
                            className="contenedor-range__valor" 
                            onChange={ valoresInputs }
                        />
                    </div>
                    {
                        ( 
                            alertValorSku &&
                                <span className="alert-date">
                                    La cantidad ingresada es mayor a la aceptada o es igual a 0, por favor ingrese otra
                                </span>
                        )
                    }
                    {
                        inputsCombo &&
                        <React.Fragment>
                            <label className="form__label">Necesidad promedio estimada de posiciones de pallet al mes</label>
                            <div className="form__contenedor-range">
                                <Form.Range 
                                    min="1"
                                    max={ adminValoresRanges != null ? adminValoresRanges.promedio_pallet : 100 }   
                                    step="1" 
                                    name="pallet" 
                                    onChange={ ( e ) => valoresRange( e.target.value, 1 ) } 
                                />
                                <FormControl 
                                    type="phone"
                                    aria-label="Small" 
                                    aria-describedby="inputGroup-sizing-sm"
                                    name="pallet"
                                    value={ valorPallet }
                                    className="contenedor-range__valor" 
                                    onChange={ valoresInputs }
                                />
                            </div>
                            {
                                ( 
                                    alertValorPallet &&
                                        <span className="alert-date">
                                            La cantidad ingresada es mayor a la aceptada o es igual a 0, por favor ingrese otra
                                        </span>
                                )
                            }
                            <label className="form__label">Necesidad promedio estimada de posiciones de carton flow al mes</label>
                            <div className="form__contenedor-range">
                                <Form.Range 
                                    min="1" 
                                    max={ adminValoresRanges != null ? adminValoresRanges.promedio_carton : 100 }  
                                    step="1" 
                                    name="carton" 
                                    onChange={ ( e ) => valoresRange( e.target.value, 2 ) } 
                                />
                                <FormControl 
                                    type="phone"
                                    aria-label="Small" 
                                    aria-describedby="inputGroup-sizing-sm"
                                    name="carton"
                                    value={ valorCarton }
                                    className="contenedor-range__valor" 
                                    onChange={ valoresInputs }
                                />
                            </div>
                            {
                                ( 
                                    alertValorCarton &&
                                        <span className="alert-date">
                                            La cantidad ingresada es mayor a la aceptada o es igual a 0, por favor ingrese otra
                                        </span>
                                )
                            }
                        </React.Fragment>
                    }
                </Form.Group>
                <br />
                <p className="form__titulo">Entradas - salidas</p>
                <br />
                <Form.Group className="mb-3">
                    <label className="form__label">Entrada promedio de material a la semana</label>
                    <div className="form__contenedor-range">
                        <Form.Range 
                            min="1" 
                            max={ adminValoresRanges != null ? adminValoresRanges.max_entrada : 100 } 
                            step="1" 
                            name="pallet" 
                            onChange={ ( e ) => valoresRange( e.target.value, 3 ) }
                        />
                        <FormControl 
                            type="phone"
                            aria-label="Small" 
                            aria-describedby="inputGroup-sizing-sm"
                            name="entrada"
                            value={ valorEntrada }
                            className="contenedor-range__valor" 
                            onChange={ valoresInputs }
                        />
                    </div>
                    {
                        ( 
                            alertValorEntrada &&
                                <span className="alert-date">
                                    La cantidad ingresada es mayor a la aceptada o es igual a 0, por favor ingrese otra
                                </span>
                        )
                    }
                    <label className="form__label">Salida promedio de material a la semana</label>
                    <div className="form__contenedor-range">
                        <Form.Range 
                            min="1" 
                            max={ adminValoresRanges != null ? adminValoresRanges.max_salida : 100 } 
                            step="1" 
                            name="pallet" 
                            onChange={ ( e ) => valoresRange( e.target.value, 4 ) }
                        />
                        <FormControl 
                            type="phone"
                            aria-label="Small" 
                            aria-describedby="inputGroup-sizing-sm"
                            name="salida"
                            value={ valorSalida }
                            className="contenedor-range__valor" 
                            onChange={ valoresInputs }
                        />
                    </div>
                    {
                        ( 
                            alertValorSalida &&
                                <span className="alert-date">
                                    La cantidad ingresada es mayor a la aceptada o es igual a 0, por favor ingrese otra
                                </span>
                        )
                    }
                </Form.Group>
                {
                    alertaNoDatos && <span className="alert-date complement-alert">Por favor llene todos los campos o verifique la información</span>
                }
                <div className="contenedor-boton">
                    <Button variant="primary" onClick={ openModalMail } >
                        Cotizar
                    </Button>
                </div>
            </Form>
            <ModalMail 
                showModalMail={ showModalMail } 
                setShowModalMail={ setShowModalMail }
                cotizacionAlmacenajeTotal={ cotizacionAlmacenajeTotal }
                cotizacionRadiosTotal={ cotizacionRadiosTotal }
                cotizacionRadiosPallet={ cotizacionRadiosPallet }
                cotizacionRadiosCarton={ cotizacionRadiosCarton }
            />
        </React.Fragment>
    )
}

export default FormKlicpik;