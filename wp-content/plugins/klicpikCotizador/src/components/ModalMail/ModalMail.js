import React, { useState, useEffect } from 'react';
// Importacion de BootstrapReact
import { Form, Button, Modal, Spinner } from 'react-bootstrap';
// Importacion de axios para las peticiones a las APIs
import axios from 'axios';
// Importacion de hoja de estilos
import './ModalMail.css';

const ModalMail = ( 
    { 
        showModalMail, 
        setShowModalMail,
        cotizacionAlmacenajeTotal,
        cotizacionRadiosTotal,
        cotizacionRadiosPallet,
        cotizacionRadiosCarton,
    } 
) => {

    const [nombreMail, setNombreMail]                               = useState( null );
    const [correoMail, setCorreoMail]                               = useState( null );
    const [loadingMail, setLoadingMail]                             = useState( false );
    const [errorMail, seErrorMail]                                  = useState( false );
    const [errorMailIncorrecto, setErrorMailIncorrecto]             = useState( false );
    const [errorDatosVacios, setErrorDatosVacios]                   = useState( false );

    // Peticiones APIs
    const apiBase          = appLocalizer.apiUrl.replace("wp-json", "?rest_route=");
    const enviarMailCotizacion = `${ apiBase }/wprk/klicpik/v1/send_mail`;

    const emailRexes = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    const closeModalMail = () => {
        setShowModalMail( false );
    }

    const dataForm = ( e ) => {

        let name  = e.target.name;
        let value = e.target.value;

        if ( name === 'correo' ) {
            setCorreoMail( value );
        } else if ( name === 'nombre' ) {
            setNombreMail( value );
        }

    }

    const sendMail = async () => {

        setErrorDatosVacios( false );

        if ( nombreMail != null && nombreMail != '' && correoMail != null && correoMail != '' ) {

            setLoadingMail( true );
            setErrorMailIncorrecto( false );
            seErrorMail( false );
    
            if ( emailRexes.test( correoMail ) ) {
    
                try {
    
                    let arregloMandar = {
                        email: correoMail,
                        name: nombreMail,
                        apiBase: apiBase,
                        cotizacionAlmacenajeTotal: cotizacionAlmacenajeTotal,
                        cotizacionRadiosTotal: cotizacionRadiosTotal,
                        cotizacionRadiosPallet: cotizacionRadiosPallet,
                        cotizacionRadiosCarton: cotizacionRadiosCarton
                    }
    
                    await axios.post( enviarMailCotizacion, arregloMandar, {
                        headers: {
                            'content-type': 'application/json',
                            'X-WP-NONCE': appLocalizer.nonce
                        }
                    });
    
                    setLoadingMail( false );
                    closeModalMail();
    
                } catch (e) {
    
                    setLoadingMail( false );
                    seErrorMail( true );
    
                }
                
            } else {
    
                setLoadingMail( false );
                setErrorMailIncorrecto( true );
            
            }
            
        } else {
            setErrorDatosVacios( true );
        }

    }

    return (
        <React.Fragment>
            <Modal
                show={ showModalMail }
                onHide={ closeModalMail }
                backdrop="static"
                keyboard={ false }
                centered
                className="contenedor-modal"
            >
                <Modal.Body>
                    <Form>
                        <Form.Group>
                            <label className="form__label">Nombre(s)</label>
                            <Form.Control className="contenedor-modal__input" type="text" placeholder="Nombre(s)" name="nombre" onChange={ dataForm } />
                            <label className="form__label">Correo electrónico</label>
                            <Form.Control className="contenedor-modal__input" type="email" placeholder="Correo electrónico" name="correo" onChange={ dataForm } />
                        </Form.Group>
                    </Form>
                    {
                        errorMailIncorrecto && <span className="alert-date complement-alert">Correo electrónico inválido (debe de contener por lo menos un . y un @)</span>
                    }
                    {
                        errorMail && <span className="alert-date complement-alert">La cotización no pudo enviarse, por favor intente más tarde</span>
                    }
                    {
                        errorDatosVacios && <span className="alert-date complement-alert">Por favor llene todos los campos</span>
                    }
                </Modal.Body>
                <Modal.Footer>
                    {
                        loadingMail
                            ?   <Button variant="primary"  className="contenedor-modal__button-aceptar" >
                                    <Spinner
                                        as="span"
                                        animation="border"
                                        size="sm"
                                        role="status"
                                        aria-hidden="true"
                                        className="mr-1"
                                    />
                                    Cotizando...
                                </Button>
                            :   <Button 
                                    className="contenedor-modal__button-aceptar" 
                                    variant="primary"
                                    onClick={ sendMail } 
                                >
                                    Cotizar
                                </Button>
                    }
                    <Button className="contenedor-modal__button-cancelar" variant="primary" onClick={ closeModalMail }>Cancelar</Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    )
}

export default ModalMail;