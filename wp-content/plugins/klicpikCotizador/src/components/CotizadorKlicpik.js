import React, { useState, useEffect } from 'react';
import { Col, Row, Container } from 'react-bootstrap';
import FormKlicpik from './Form/FormKlicpik';
import './style_react.css'
// Importacion de axios para las peticiones a las APIs
import axios from 'axios';


const CotizadorKlicpik = () => {

    const [adminValoresCotizacion, setAdminValoresCotizacion] = useState( null );

    const apiBase                     = appLocalizer.apiUrl.replace("wp-json", "?rest_route=");
    const valoresConstantesCotizacion = `${ apiBase }/wprk/klicpik/v1/values_quote`;

    useEffect(() => {
        peticionesApis();
    })

    const peticionesApis = async () => {

        let valoresCotizacion = await axios.get( valoresConstantesCotizacion );
        await setAdminValoresCotizacion( valoresCotizacion.data[0] );

    }

    return (
        <React.Fragment>
            <Container>
                <Row>
                    <Col>
                        <section className="pt-5 pb-5">
                            <div className="contenedor-formulario__contenedor-boton">
                                <a 
                                    className="btn btn-warning boton__texto" 
                                    data-toggle="collapse" 
                                    href="#collapseExample" 
                                    role="button" 
                                    aria-expanded="false" 
                                    aria-controls="collapseExample"
                                >
                                    Cotizar
                                </a>
                            </div>
                            <br />
                            <div className="contenedor-formulario">
                                <div className="collapse contenedor-formulario__formulario" id="collapseExample">
                                    <div className="card card-body contenedor-formulario__card">
                                        <FormKlicpik adminValoresCotizacion={ adminValoresCotizacion } />
                                    </div>
                                </div>
                            </div>
                        </section>
                    </Col>
                </Row>
            </Container>
        </React.Fragment>
    )

}

export default CotizadorKlicpik;