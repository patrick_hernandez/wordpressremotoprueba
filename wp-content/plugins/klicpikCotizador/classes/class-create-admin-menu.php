<?php

class WP_klicpik_create_admin_menu {

    // Constructor del panel de administracion del plugin
    public function __construct()
    {
        add_action('admin_menu', [$this, 'KLICPIK_plugin_cotizador']);
        ob_start();
    }

    // Creacion de panel de administracion
    public function KLICPIK_plugin_cotizador()
    {
        // Slug que aparecera en la barra lateral de WP
        $capability = 'manage_options';
        $slug = 'klicpik_plugin_cotizador_dashboard';
        // Lo agregamos a la barra
        add_menu_page(
            __('Klicpik cotizador', 'wp-klicpik-plugin'),
            __('Klicpik cotizador', 'wp-klicpik-plugin'),
            $capability,
            $slug,
            [$this, 'KLICPIK_plugin_admin'],
            'dashicons-archive'
        );
    }

    // Funciones para guardar datos en las configuraciones de correo electronico y cotizador
    public function save_item_mail()
    {
        // funcion para controlar BD en PHP 
        global $wpdb;

        // Evaluamos si todos los campos del formulario estan llenos para poder insertar la informacion
        if (
            !empty($_POST)                &&
            $_POST['savemail']      != '' &&
            $_POST['admin']         != '' &&
            $_POST['remitente']     != '' &&
            $_POST['nameremitente'] != '' &&
            $_POST['host']          != '' &&
            $_POST['puerto']        != '' &&
            $_POST['smtps']         != '' &&
            $_POST['password']      != ''
        ) {

            $tabla_form_mail      = $wpdb->prefix . 'cotizacion_correo';
            $correo_administrador = $_POST['admin'];
            $remitente            = $_POST['remitente'];
            $nombre_remitente     =  $_POST['nameremitente'];
            $host                 = $_POST['host'];
            $puerto               = $_POST['puerto'];
            $smtp                 = $_POST['smtps'];
            $password_mail        = $_POST['password'];
            $create_at            = date('Y-m-d H:i:s');
            $update_at            = date('Y-m-d H:i:s');

            // Insercion en la BD
            $wpdb->insert(
                $tabla_form_mail,
                array(
                    'correo_administrador' => $correo_administrador,
                    'remitente'            => $remitente,
                    'nombre_remitente'     => $nombre_remitente,
                    'host'                 => $host,
                    'puerto'               => $puerto,
                    'smtp'                 => $smtp,
                    'password_mail'        => $password_mail,
                    'created_at'           => $create_at,
                    'update_at'            => $update_at
                )
            );

            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Gracias</strong> ¡Los datos han sido registrados con éxito!...
            </div>
            ";
        }

        ob_start();
    }

    public function save_item_quote()
    {
        global $wpdb;

        if (
            !empty($_POST)            &&
            $_POST['savequote'] != '' &&
            $_POST['entrada']   != '' &&
            $_POST['salida']    != '' &&
            $_POST['pallet']    != '' &&
            $_POST['carton']    != ''
        ) {

            $tabla_form_klicpik = $wpdb->prefix . 'cotizacion_klicpik';
            $entrada            = $_POST['entrada'];
            $salida             = $_POST['salida'];
            $pallet             =  $_POST['pallet'];
            $carton             = $_POST['carton'];
            $create_at          = date('Y-m-d H:i:s');
            $update_at          = date('Y-m-d H:i:s');

            $wpdb->insert(
                $tabla_form_klicpik,
                array(
                    'entrada'    => $entrada,
                    'salida'     => $salida,
                    'pallet'     => $pallet,
                    'carton_flow'     => $carton,
                    'created_at' => $create_at,
                    'update_at'  => $update_at
                )
            );
            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Gracias</strong> ¡Los datos han sido registrados con éxito!...
            </div>
            ";
        }

        ob_start();
    }

    public function save_item_range()
    {
        global $wpdb;

        if (
            !empty($_POST)             &&
            $_POST['saverange']  != '' &&
            $_POST['maxskus']    != '' &&
            $_POST['maxpallet']  != '' &&
            $_POST['maxcarton']  != '' &&
            $_POST['maxentrada'] != '' &&
            $_POST['maxsalida']  != ''
        ) {

            $tabla_form_ranges = $wpdb->prefix . 'cotizacion_max_ranges';
            $maxskus           = $_POST['maxskus'];
            $maxpallet         = $_POST['maxpallet'];
            $maxcarton         = $_POST['maxcarton'];
            $maxentrada        = $_POST['maxentrada'];
            $maxsalida         = $_POST['maxsalida'];
            $create_at         = date('Y-m-d H:i:s');
            $update_at         = date('Y-m-d H:i:s');

            $wpdb->insert(
                $tabla_form_ranges,
                array(
                    'num_sku'         => $maxskus,
                    'promedio_pallet' => $maxpallet,
                    'promedio_carton' => $maxcarton,
                    'max_entrada'     => $maxentrada,
                    'max_salida'      => $maxsalida,
                    'created_at'      => $create_at,
                    'update_at'       => $update_at
                )
            );
            echo "
                </br>
                <div class='exito container alert alert-success'>
                    <strong>Gracias</strong> ¡Los datos han sido registrados con éxito!...
                </div>
            ";
        }

        ob_start();
    }

    // Funciones para editar los datos de la configuracion de correo electronico y cotizador

    public function edit_item_mail()
    {
        global $wpdb;
        if (!empty($_POST) && $_POST['updatemail']) {
            $id_mail              = $_POST['mail_id'];
            $correo_administrador = $_POST['admin'];
            $remitente            = $_POST['remitente'];
            $nombre_remitente     = $_POST['nameremitente'];
            $host                 = $_POST['host'];
            $puerto               = $_POST['puerto'];
            $smtp                 = $_POST['smtps'];
            $password_mail        = $_POST['password'];

            global $wpdb;
            $tabla_form_mail = $wpdb->prefix . 'cotizacion_correo';
            $wpdb->update(
                $tabla_form_mail,
                array(
                    'correo_administrador' => $correo_administrador,
                    'remitente' => $remitente,
                    'nombre_remitente' => $nombre_remitente,
                    'host' => $host,
                    'puerto' => $puerto,
                    'smtp' => $smtp,
                    'password_mail' => $password_mail
                ),
                array('id' => $id_mail)
            );

            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Gracias</strong> ¡Los datos han sido actualizados con éxito!...
            </div>
            ";
        }
        ob_start();
    }

    public function edit_item_quote()
    {
        global $wpdb;
        if (!empty($_POST) && $_POST['updatequote']) {
            $id_quote = $_POST['quote_id'];
            $entrada  = $_POST['entrada'];
            $salida   = $_POST['salida'];
            $pallet   = $_POST['pallet'];
            $carton   = $_POST['carton'];

            global $wpdb;
            $tabla_form_mail = $wpdb->prefix . 'cotizacion_klicpik';
            $wpdb->update(
                $tabla_form_mail,
                array(
                    'entrada'     => $entrada,
                    'salida'      => $salida,
                    'pallet'      => $pallet,
                    'carton_flow' => $carton,
                ),
                array('id' => $id_quote)
            );

            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Gracias</strong> ¡Los datos han sido actualizados con éxito!...
            </div>
            ";
        }
        ob_start();
    }

    public function edit_item_ranges()
    {
        global $wpdb;
        if (!empty($_POST) && $_POST['updaterange']) {
            $id_range   = $_POST['range_id'];
            $maxskus    = $_POST['maxskus'];
            $maxpallet  = $_POST['maxpallet'];
            $maxcarton  = $_POST['maxcarton'];
            $maxentrada = $_POST['maxentrada'];
            $maxsalida  = $_POST['maxsalida'];

            global $wpdb;
            $tabla_form_ranges = $wpdb->prefix . 'cotizacion_max_ranges';
            $wpdb->update(
                $tabla_form_ranges,
                array(
                    'num_sku'         => $maxskus,
                    'promedio_pallet' => $maxpallet,
                    'promedio_carton' => $maxcarton,
                    'max_entrada'     => $maxentrada,
                    'max_salida'      => $maxsalida,
                ),
                array('id' => $id_range)
            );

            echo "
                </br>
                <div class='exito container alert alert-success'>
                    <strong>Gracias</strong> ¡Los datos han sido actualizados con éxito!...
                </div>
            ";
        }
        ob_start();
    }

    public function KLICPIK_plugin_admin()
    {
        // Instanciando a funciones para guardar y editar la informacion
        $this->save_item_mail();
        $this->save_item_quote();
        $this->edit_item_mail();
        $this->edit_item_quote();
        $this->save_item_range();
        $this->edit_item_ranges();

        // Variables con urls de assets
        define('DIRECCION_LOGO_KLICPIK',   plugin_dir_url(__DIR__) .  "assets/img/logoAdmin.png");

        // Consulta a la BD para colocar datos en formulario de actualizacion de configuracion de correo electronico
        global $wpdb;
        $tabla_form_mail = $wpdb->prefix . 'cotizacion_correo';
        $datos_mail      = $wpdb->get_results("SELECT * FROM $tabla_form_mail");

        foreach ( $datos_mail as $dato_mail ) {
            $mail_id              = $dato_mail->id;
            $correo_administrador = $dato_mail->correo_administrador;
            $remitente            = $dato_mail->remitente;
            $nombre_remitente     = $dato_mail->nombre_remitente;
            $host                 = $dato_mail->host;
            $puerto               = $dato_mail->puerto;
            $smtp                 = $dato_mail->smtp;
            $password_mail        = $dato_mail->password_mail;
        }

        // Consulta a la BD para colocar datos en formulario de actualizacion de configuracion de cotizador
        global $wpdb;
        $tabla_form_quote = $wpdb->prefix . 'cotizacion_klicpik';
        $datos_quote = $wpdb->get_results("SELECT * FROM $tabla_form_quote");

        foreach ( $datos_quote as $dato_quote ) {
            $quote_id    = $dato_quote->id;
            $entrada     = $dato_quote->entrada;
            $salida      = $dato_quote->salida;
            $pallet      = $dato_quote->pallet;
            $carton_flow = $dato_quote->carton_flow;
        }

        // Consulta a la BD para colocar datos en formulario de actualizacion de configuracion de rangos del cotizador
        global $wpdb;
        $tabla_form_ranges = $wpdb->prefix . 'cotizacion_max_ranges';
        $datos_ranges = $wpdb->get_results("SELECT * FROM $tabla_form_ranges");

        foreach ( $datos_ranges as $dato_range ) {
            $range_id        = $dato_range->id;
            $range_sku       = $dato_range->num_sku;
            $promedio_pallet = $dato_range->promedio_pallet;
            $promedio_carton = $dato_range->promedio_carton;
            $max_entrada     = $dato_range->max_entrada;
            $max_salida      = $dato_range->max_salida;
        }

        // Variables con formularios para registro y edicion de datos de configuracion de correo electronico
        $form_mail_empty = '
            <form action="' . get_the_permalink() . '" method="post" id="form_configuracion_mail">

                <label class="formulario-label" for="">Correo del administrador</label>
                <input class="form-control form-control-sm" type="email" name="admin" value=""> 

                <label class="formulario-label">Remitente</label>
                <input class="form-control form-control-sm" type="email" name="remitente" value=""> 
                
                <label class="formulario-label">Nombre del remitente</label>
                <input class="form-control form-control-sm" type="text" name="nameremitente" value=""> 
                
                <label class="formulario-label">Host</label>
                <input class="form-control form-control-sm" type="text" name="host" value=""> 
                
                <label class="formulario-label">Puerto</label>
                <input class="form-control form-control-sm" type="text" name="puerto" value=""> 
                
                <label class="formulario-label">SMTP Secure</label>
                <input class="form-control form-control-sm" type="text" name="smtps" value=""> 
                
                <label class="formulario-label">Contraseña</label>
                <input class="form-control form-control-sm" type="password" name="password" value=""> 

                </br>
                <input type="submit" name="savemail" value="Guardar" class="btn btn-warning btn-block mt-2">

            </form>
        ';

        $form_mail_update = '
            <form action="' . get_the_permalink() . '" method="post" id="form_configuracion_mail">

                <label class="formulario-label" for="">Correo del administrador</label>
                <input class="form-control form-control-sm" type="email" name="admin" value="'.$correo_administrador.'"> 

                <label class="formulario-label">Remitente</label>
                <input class="form-control form-control-sm" type="email" name="remitente" value="'.$remitente.'"> 
                
                <label class="formulario-label">Nombre del remitente</label>
                <input class="form-control form-control-sm" type="text" name="nameremitente" value="'.$nombre_remitente.'"> 
                
                <label class="formulario-label">Host</label>
                <input class="form-control form-control-sm" type="text" name="host" value="'.$host.'"> 
                
                <label class="formulario-label">Puerto</label>
                <input class="form-control form-control-sm" type="text" name="puerto" value="'.$puerto.'"> 
                
                <label class="formulario-label">SMTP Secure</label>
                <input class="form-control form-control-sm" type="text" name="smtps" value="'.$smtp.'"> 
                
                <label class="formulario-label">Contraseña</label>
                <input class="form-control form-control-sm" type="password" name="password" value="'.$password_mail.'"> 

                <input type="text" name="mail_id" value="'.$mail_id.'" style="display:none"/>

                </br>
                <input type="submit" name="updatemail" value="Actualizar" class="btn btn-warning btn-block mt-2">

            </form>
        ';

        // Variables con formularios para registro y edicion de datos de configuracion de cotizador
        $form_quote_empty = '
            <form action="' . get_the_permalink() . '" method="post" id="form_quote">

                <label for="" class="formulario-label">Entrada</label>
                <input class="form-control form-control-sm" type="text" name="entrada" value=""> 

                <label for="" class="formulario-label">Salida</label>
                <input class="form-control form-control-sm" type="text" name="salida" value=""> 
                
                <label for="" class="formulario-label">Pallet</label>
                <input class="form-control form-control-sm" type="text" name="pallet" value=""> 
                
                <label for="" class="formulario-label">Carton Flow</label>
                <input class="form-control form-control-sm" type="text" name="carton" value=""> 

                </br>
                <input type="submit" name="savequote" value="Guardar" class="btn btn-warning btn-block mt-2"> 

            </form>
        ';

        $form_quote_update = '
            <form action="' . get_the_permalink() . '" method="post" id="form_quote">

                <label for="" class="formulario-label">Entrada</label>
                <input class="form-control form-control-sm" type="text" name="entrada" value="'.$entrada.'"> 

                <label for="" class="formulario-label">Salida</label>
                <input class="form-control form-control-sm" type="text" name="salida" value="'. $salida.'"> 
                
                <label for="" class="formulario-label">Pallet</label>
                <input class="form-control form-control-sm" type="text" name="pallet" value="'.$pallet.'"> 
                
                <label for="" class="formulario-label">Carton Flow</label>
                <input class="form-control form-control-sm" type="text" name="carton" value="'.$carton_flow.'"> 

                <input type="text" name="quote_id" value="'.$quote_id.'" style="display:none"/>

                </br>
                <input type="submit" name="updatequote" value="Guardar" class="btn btn-warning btn-block mt-2"> 

            </form>
        ';

         // Variables con formularios para registro y edicion de datos de configuracion de los ranges del cotizador
        $form_range_empty = '
            <form action="' . get_the_permalink() . '" method="post" id="form_quote">

                <label for="" class="formulario-label">Máximo número de SKUs</label>
                <input class="form-control form-control-sm" type="text" name="maxskus" value=""> 

                <label for="" class="formulario-label">Máximo número de pallets</label>
                <input class="form-control form-control-sm" type="text" name="maxpallet" value=""> 
                
                <label for="" class="formulario-label">Máximo número de carton flow</label>
                <input class="form-control form-control-sm" type="text" name="maxcarton" value=""> 
                
                <label for="" class="formulario-label">Máximo número de entradas</label>
                <input class="form-control form-control-sm" type="text" name="maxentrada" value=""> 

                <label for="" class="formulario-label">Máximo número de salidas</label>
                <input class="form-control form-control-sm" type="text" name="maxsalida" value=""> 

                </br>
                <input type="submit" name="saverange" value="Guardar" class="btn btn-warning btn-block mt-2"> 

            </form>
        ';

        $form_range_update = '
            <form action="' . get_the_permalink() . '" method="post" id="form_quote">

                <label for="" class="formulario-label">Máximo número de SKUs</label>
                <input class="form-control form-control-sm" type="text" name="maxskus" value="'.$range_sku.'"> 

                <label for="" class="formulario-label">Máximo número de pallets</label>
                <input class="form-control form-control-sm" type="text" name="maxpallet" value="'.$promedio_pallet.'"> 
                
                <label for="" class="formulario-label">Máximo número de carton flow</label>
                <input class="form-control form-control-sm" type="text" name="maxcarton" value="'.$promedio_carton.'"> 
                
                <label for="" class="formulario-label">Máximo número de entradas</label>
                <input class="form-control form-control-sm" type="text" name="maxentrada" value="'.$max_entrada.'"> 

                <label for="" class="formulario-label">Máximo número de salidas</label>
                <input class="form-control form-control-sm" type="text" name="maxsalida" value="'.$max_salida.'"> 

                <input type="text" name="range_id" value="'.$range_id.'" style="display:none"/>

                </br>
                <input type="submit" name="updaterange" value="Guardar" class="btn btn-warning btn-block mt-2"> 

            </form>
        ';

        echo '
            <div class="container-fluid admin-plugin__cabecera">
                <img src=" '; echo DIRECCION_LOGO_KLICPIK; echo '" class="cabecera-img" alt="Logo klicpik">
                <span class="cabecera-titulo">Plugin cotizador</span>
                </br>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-4 col-sm-12 col-md-4 col-lg-4 mb-3">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a 
                                class="nav-link link-tab-klicpik active" id="v-pills-home-tab" 
                                data-toggle="pill" 
                                href="#v-pills-home" 
                                role="tab" 
                                aria-controls="v-pills-home" 
                                aria-selected="true"
                            >
                                Configuración de correo electrónico
                            </a>
                            <a 
                                class="nav-link link-tab-klicpik" 
                                id="v-pills-profile-tab" 
                                data-toggle="pill" 
                                href="#v-pills-profile" 
                                role="tab" 
                                aria-controls="v-pills-profile" 
                                aria-selected="false"
                            >
                                Configuración de cotizador
                            </a>
                            <a 
                                class="nav-link link-tab-klicpik" 
                                id="v-pills-messages-tab" 
                                data-toggle="pill" 
                                href="#v-pills-messages" 
                                role="tab" 
                                aria-controls="v-pills-messages" 
                                aria-selected="false"
                            >
                                Configuración de rangos
                            </a>
                        </div>
                    </div>
                    <div class="col-8 col-sm-12 col-md-8 col-lg-8">
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                <div class="admin-plugin__formulario">
                                    <p class="formulario-titulo-tabla">Configuración de correo electrónico</p>
                                    ' . ( !empty( $datos_mail ) ? $form_mail_update : $form_mail_empty ) . '
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                <div class="admin-plugin__formulario">
                                    <p class="formulario-titulo-tabla">Configuración de cotizador</p>
                                    ' . ( !empty( $datos_quote ) ? $form_quote_update : $form_quote_empty ) . '
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                <div class="admin-plugin__formulario">
                                    <p class="formulario-titulo-tabla">Configuración de rangos en cotizador</p>
                                    ' . ( !empty( $datos_ranges ) ? $form_range_update : $form_range_empty ) . '
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ';
    }

}

new WP_klicpik_create_admin_menu();