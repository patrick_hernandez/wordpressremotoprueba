<?php

class WP_klicpik_create_rest_api
{

    // Registra la funcion para la creacion de APIs 
    public function __construct()
    {
        add_action('rest_api_init', [$this, 'create_rest_routes']);
    }

    // Funcion que creara las APIs
    public function create_rest_routes()
    {

        register_rest_route('wprk/klicpik/v1', '/ranges_quote', [
            'methods' => 'GET',
            'callback' => [$this, 'get_ranges_quote'],
            'permission_callback' => [$this, 'get_settings_permission']
        ]);

        register_rest_route('wprk/klicpik/v1', '/values_quote', [
            'methods' => 'GET',
            'callback' => [$this, 'get_values_quote'],
            'permission_callback' => [$this, 'get_settings_permission']
        ]);

        register_rest_route('wprk/klicpik/v1', '/send_mail', [
            'methods' => 'POST',
            'callback' => [$this, 'post_send_mail'],
            'permission_callback' => '__return_true'
        ]);

    }

    // Funcion que otorga los permisos para cada una de las APIs
    public function get_settings_permission()
    {

        return true;

    }

    // Funcion que devuelve los datos de la tabla [cotizacion_max_ranges]
    public function get_ranges_quote()
    {

        global $wpdb;
        $tabla_ranges = $wpdb->prefix . 'cotizacion_max_ranges';
        $datos  = $wpdb->get_results("SELECT * FROM $tabla_ranges");
        return rest_ensure_response($datos);

    }

     // Funcion que devuelve los datos de la tabla [cotizacion_max_ranges]
    public function get_values_quote()
    {

        global $wpdb;
        $tabla_quotes = $wpdb->prefix . 'cotizacion_klicpik';
        $datos  = $wpdb->get_results("SELECT * FROM $tabla_quotes");
        return rest_ensure_response($datos);

    }

    public function post_send_mail( $req )
    {

        // Importacion de plantilla HTML y hoja de estilos
        $mensaje  = (plugin_dir_path(__FILE__) . 'plantilla/estructura.html');
        $estilos  = (plugin_dir_path(__FILE__) . 'plantilla/estilos.css');

        // Importacion de imagenes
        $pikkop_logo = (plugin_dir_path(__FILE__) . 'plantilla/assets/klicpikMail.png');

        // Consulta a la BD
        global $wpdb;
        $tabla_form_mail = $wpdb->prefix . 'cotizacion_correo';
        $datos_mail      = $wpdb->get_results("SELECT * FROM $tabla_form_mail");

        foreach ($datos_mail as $dato_mail) {
            $mail_admin      = $dato_mail->correo_administrador;
            $mail_sender     = $dato_mail->remitente;
            $name_sender     = $dato_mail->nombre_remitente;
            $host            = $dato_mail->host;
            $port            = $dato_mail->puerto;
            $smtp_secure     = $dato_mail->smtp;
            $password_sender = $dato_mail->password_mail;
        }
        // Iniciamos con el envio del email

        // A quien va a enviarse el correo
        $emailTo = $req['email'] ;

        // Datos del formulario pasados mediante props
        // $emailTo               = $mail_admin;
        $Subject               = "Cotizacion klicpik";
        $bodyEmail_email       = $req['email'];
        $bodyEmail_first       = $req['name'];
        $bodyEmail_quoteTotal       = $req['cotizacionAlmacenajeTotal'];
        $bodyEmail_quoteRadioTotal = $req['cotizacionRadiosTotal'];
        $bodyEmail_quoteRadioPallet = $req['cotizacionRadiosPallet'];
        $bodyEmail_quoteRadioCarton = $req['cotizacionRadiosCarton'];

        // Datos de BD a variables de PHPMailer
        $fromemail   = $mail_sender;
        $fromname    = $name_sender;
        $host        = $host;
        $port        = $port;
        $SMTPAuth    = true;
        $_SMTPSecure = $smtp_secure;
        $password    = $password_sender;

        // Instancia de PHPMailer
        $mail = new PHPMailer\PHPMailer\PHPMailer();

        // PHPMailer utlizara SMTP
        $mail->isSMTP();

        // Produccion o debug (colocamos 1 para debbug)
        $mail->SMTPDebug = 0;

        // Desde donde enviaremos el correo electronico
        $mail->Host = $host;
        $mail->Port = $port;
        $mail->SMTPAuth = $SMTPAuth;
        $mail->SMTPSecure = $_SMTPSecure;
        $mail->Username = $fromemail;
        $mail->Password = $password;

        // Asignamos el remitente y el nombre del remitente
        $mail->setFrom($fromemail, $fromname);

        // Asignamos el email a el destinatario
        $mail->addAddress($emailTo);

        // Asunto
        $mail->isHTML(true);
        $mail->Subject = $Subject;

        // Carga de imagenes
        $mail->AddEmbeddedImage($pikkop_logo, 'pikkop_logo', 'pikkop_logo.png');

        // Mensaje inicio -------------------------------------------------------
        // cargar archivo css para cuerpo de mensaje
        $rcss = $estilos; //ruta de archivo css
        $fcss = fopen($rcss, "r"); //abrir archivo css
        $scss = fread($fcss, filesize($rcss)); //leer contenido de css
        fclose($fcss); //cerrar archivo css

        // Cargar archivo html   
        $shtml = file_get_contents($mensaje);

        // reemplazar sección de plantilla html con el css cargado y mensaje creado
        $incss  = str_replace('<style id="estilo"></style>', "<style>$scss</style>", $shtml);
        $cuerpo = str_replace('<span id="email"></span>', $bodyEmail_email, $incss);
        $cuerpo = str_replace('<span id="nombre"></span>', $bodyEmail_first, $cuerpo);
        $cuerpo = str_replace('<span id="cotizaciontotal"></span>', $bodyEmail_quoteTotal, $cuerpo);
        $cuerpo = str_replace('<span id="cotizacionradiototal"></span>', $bodyEmail_quoteRadioTotal, $cuerpo);
        $cuerpo = str_replace('<span id="cotizacionradiopallet"></span>', $bodyEmail_quoteRadioPallet, $cuerpo);
        $cuerpo = str_replace('<span id="cotizacionradiocarton"></span>', $bodyEmail_quoteRadioCarton, $cuerpo);

        // Envio del mensaje
        $mail->Body = utf8_decode($cuerpo);
        // Mensaje final -------------------------------------------------------

        if (!$mail->send()) {
            die();
        }
    }


}

new WP_klicpik_create_rest_api();