<?php

function Tabla_cotizacion_correo( $wpdb, $charset_collate )
{

    $charset_collate = $wpdb->get_charset_collate();

    $tabla_correo = $wpdb->prefix . 'cotizacion_correo';
    $query_correo = "CREATE TABLE IF NOT EXISTS $tabla_correo (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        correo_administrador varchar(60) NOT NULL,
        remitente varchar(60) NOT NULL,
        nombre_remitente varchar(60) NOT NULL,
        host varchar(60) NOT NULL,
        puerto varchar(60) NOT NULL,
        smtp varchar(60) NOT NULL,
        password_mail varchar(60) NOT NULL,
        created_at datetime NOT NULL,
        update_at datetime NOT NULL,
        UNIQUE (id)
    ) $charset_collate;";

    dbDelta( $query_correo );
}

function Tabla_datos_cotizador( $wpdb, $charset_collate )
{

    $charset_collate = $wpdb->get_charset_collate();
    
    $tabla_datos_cotizador = $wpdb->prefix . 'cotizacion_klicpik';
    $query_datos_cotizador = "CREATE TABLE IF NOT EXISTS $tabla_datos_cotizador (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        entrada float NOT NULL,
        salida float NOT NULL,
        pallet float NOT NULL,
        carton_flow float NOT NULL,
        created_at datetime NOT NULL,
        update_at datetime NOT NULL,
        UNIQUE (id)
    ) $charset_collate;";
        
    dbDelta( $query_datos_cotizador );
}

function Tabla_max_ranges( $wpdb, $charset_collate )
{

    $charset_collate = $wpdb->get_charset_collate();

    $tabla_max_ranges = $wpdb->prefix . 'cotizacion_max_ranges';
    $query_datos_cotizador = "CREATE TABLE IF NOT EXISTS $tabla_max_ranges (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        num_sku float NOT NULL,
        promedio_pallet float NOT NULL,
        promedio_carton float NOT NULL,
        max_entrada float NOT NULL,
        max_salida float NOT NULL,
        created_at datetime NOT NULL,
        update_at datetime NOT NULL,
        UNIQUE (id)
    ) $charset_collate;";
        
    dbDelta( $query_datos_cotizador );
}