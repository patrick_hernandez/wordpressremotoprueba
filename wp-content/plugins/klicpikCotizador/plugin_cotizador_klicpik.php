<?php
/**
 * Plugin Name:  Klicpik cotizador
 * Description:  Desarrollado para complementar las funcionalidades del sitio de Klicpik, este plugin permite automatizar la cotización para después poder enviársela al cliente a través de su correo electrónico
 * Version:      1.0.0
 * Author:       TEKSI
 * Author URI:   https://www.teksi.mx
 * PHP Version:  5.6
 *
 * @category Shipments
 * @package  TEKSI
 * @author   TEKSI
 * @license  GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @link     https://www.teksi.mx
*/

/** Manejo de errores --------------------------------------------------------------------------------------
 * Al tener dos form method="POST" dentro de un mismo archivo nos produce
 * advertencias del los inidices de los arrays, pero no ocasionan
 * el quiebre del sistena, por esa razon se coloca el siguiente if
*/
if ($_SERVER['REMOTE_ADDR'] == "00.00.00.00") {
    ini_set('display_errors', 'On');
} else {
    ini_set('display_errors', 'Off');
}
// Fin Manejo de errore --------------------------------------------------------------------------------------

// Dependencias Inicio ---------------------------------------------------------------------------------------

// Registrar Bootstrap
wp_register_style('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css', '', '5.0', 'all');
// Agregar Bootstrap
wp_enqueue_style('estilos', get_stylesheet_uri(), array('bootstrap'), '1.0', 'all');
// Scripts de necesarios para Bootstrap
// Inicializa
wp_register_script('popper', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js', '', '1.16.1', true);
// Agrega
wp_enqueue_script('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js', array('jquery', 'popper'), '5.0', true);
// Cargar la hoja de estilos (archivo .css) para poder personalizar el formulario
wp_enqueue_style('css_plugin', plugins_url('style.css', __FILE__));

// Dependencias Final -----------------------------------------------------------------------------------------

// Cuando el plugin se active se crea la tabla del mismo si no existe
register_activation_hook(__FILE__, 'cotizacion_klicpik_plugin');

// Includes de libreria PHPMailer
// plugin_dir_path(__FILE__) nos da la direccion hasta la carpeta raiz de este archivo
// y concatenamos el resto necesario para tener acceso a los archivos que requerimos
// if ($entornoDesarrollo == true) {
    include_once(plugin_dir_path(__FILE__) . 'Mailer/src/PHPMailer.php');
    include_once(plugin_dir_path(__FILE__) . 'Mailer/src/SMTP.php');
    include_once(plugin_dir_path(__FILE__) . 'Mailer/src/Exception.php');
// }

// Funcion que nos permite en colar los archivos JS
function enqueue_scripts( $hook )
{

    // Validacion para saber si estamos dentro del panel de administracion del plugin
    if ( $hook != 'toplevel_page_klicpik_plugin_cotizador_dashboard' ) {
        return;
    }

    // encolando js
    wp_enqueue_script(
        'plugin-script',
        plugin_dir_url(__DIR__) . 'klicpikCotizador/js/custom.js',
        ['jquery'],
        '1.0.0',
        true
    );

}
add_action( 'admin_enqueue_scripts', 'enqueue_scripts' );


/**
 * Define Plugins Contants
 */
define('WPRK_PATH', trailingslashit(plugin_dir_path(__FILE__)));
define('WPRK_URL', trailingslashit(plugins_url('/', __FILE__)));

/**
 * Realiza las acciones necesarias para configurar el plugin cuando se activa
 * Creacion de tablas en Base de datos
 */
function cotizacion_klicpik_plugin()
{
    global $wpdb;
    include_once ABSPATH . 'wp-admin/includes/upgrade.php';
    include_once 'classes/class-create-tables-db.php';

    $charset_collate = $wpdb->get_charset_collate();

    // Crear las tablas para el plugin
    Tabla_cotizacion_correo( $wpdb, $charset_collate );
    Tabla_datos_cotizador( $wpdb, $charset_collate );
    Tabla_max_ranges( $wpdb, $charset_collate );
}

// Conexion con React para la creacion de el frontend en con un shortcode
add_shortcode('klicpik_cotizador_plugin', 'Klicpik_Cotizador_Plugin');
function Klicpik_Cotizador_Plugin()
{
    wp_enqueue_style('script_klicpik', plugins_url('dist/bundle.js', __FILE__));
    wp_enqueue_script('wp-klicpik-plugin', WPRK_URL . 'dist/bundle.js', ['jquery', 'wp-element'], wp_rand(), true);
    wp_localize_script('wp-klicpik-plugin', 'appLocalizer', [
        'apiUrl' => home_url('/wp-json'),
        'nonce' => wp_create_nonce('wp_rest'),
    ]);

    ?>

        <div class="wrap">
            <div id="wprk-admin-app"></div>
        </div>

    <?php
        return ob_get_clean();
}

require_once WPRK_PATH . 'classes/class-create-admin-menu.php';
require_once WPRK_PATH . 'classes/class-create-tables-db.php';
require_once WPRK_PATH . 'classes/class-rest-api.php';