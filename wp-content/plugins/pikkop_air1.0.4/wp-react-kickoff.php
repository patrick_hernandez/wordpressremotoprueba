<?php

/**
 * Plugin Name: WP React Pikkop.
 * Author: TEKSI
 * Author URI: https://www.teksi.mx/
 * Version: 1.0.4
 * Description: WordPress React Pikkop.
 * Text-Domain: wp-react-Pikkop
 */



// Hook de WP que se activa cuando se activa el plugin
// __FILE__ devuelde la ruta actual
register_activation_hook(__FILE__, "teksi_init");

function teksi_init()
{
    global $wpdb;

    include_once ABSPATH . 'wp-admin/includes/upgrade.php';
    include_once 'classes/class-create-tables-db.php';

    $charset_collate = $wpdb->get_charset_collate();

    //Create table for plugin
    Sku_handling_charge($wpdb, $charset_collate);
    Discounts_volume($wpdb, $charset_collate);
    Packing_shipping_weight($wpdb, $charset_collate);
    Handling_fees($wpdb, $charset_collate);
    price($wpdb, $charset_collate);
    Data_aditional($wpdb, $charset_collate);
    Send_mail($wpdb, $charset_collate);
    Url_redirect($wpdb, $charset_collate);
    Data_success_stories($wpdb, $charset_collate);
}

if (!defined('ABSPATH')) : exit();
endif; // No direct access allowed.

/**
 * Define Plugins Contants
 */
define('WPRK_PATH', trailingslashit(plugin_dir_path(__FILE__)));
define('WPRK_URL', trailingslashit(plugins_url('/', __FILE__)));

/**
 * Loading Necessary Scripts
 */
add_action('admin_enqueue_scripts', 'load_scripts');
function load_scripts()
{
    wp_enqueue_script('wp-react-pikkop', WPRK_URL . 'dist/bundle.js', ['jquery', 'wp-element'], wp_rand(), true);
    wp_localize_script('wp-react-pikkop', 'appLocalizer', [
        'apiUrl' => home_url('/wp-json'),
        'nonce' => wp_create_nonce('wp_rest'),
    ]);
}


/**
 * Definición de shortCode que se invocara al formulario
 */
add_shortcode('teksi_plugin_react', 'menu_page_template');
function menu_page_template()
{

    wp_enqueue_style('css_aspirante', plugins_url('dist/bundle.js', __FILE__));
    wp_enqueue_script('wp-react-pikkop', WPRK_URL . 'dist/bundle.js', ['jquery', 'wp-element'], wp_rand(), true);
    wp_localize_script('wp-react-pikkop', 'appLocalizer', [
        'apiUrl' => home_url('/wp-json'),
        'nonce' => wp_create_nonce('wp_rest'),
    ]);
?>
    <div class="wrap">
        <div id="wprk-admin-app"></div>
    </div>
<?php
    return ob_get_clean();
}


/**
 * files for plugin configuration
 */
require_once WPRK_PATH . 'classes/class-create-admin-menu.php';
require_once WPRK_PATH . 'classes/class-create-settings-routes.php';
require_once WPRK_PATH . 'classes/class-create-tables-db.php';
