<?php

/** Manejo de errores ------------------------------------------------------
 * Al tener dos form method="POST" dentro de un mismo archivo nos produce
 * advertencias del los inidices de los arrays, pero no ocasionan
 * el quiebre del sistena, por esa razon se coloca el siguiente if
 */
if ($_SERVER['REMOTE_ADDR'] == "00.00.00.00") {
    ini_set('display_errors', 'On');
} else {
    ini_set('display_errors', 'Off');
}
// Fin del iff de manejo de errores -----------------------------------------

// Dependencias Inicio ------------------------------------------------------

// (nombre), (url), (dependencias), (version), (medi)
// Registrar Bootstrap
wp_register_style('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css', '', '5.0', 'all');
// Agregar Bootstrap
wp_enqueue_style('estilos', get_stylesheet_uri(), array('bootstrap'), '1.0', 'all');
// Scripts de necesarios para Bootstrap
// Inicializa
wp_register_script('popper', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js', '', '1.16.1', true);
// Agrega
wp_enqueue_script('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js', array('jquery', 'popper'), '5.0', true);
// Dependencias Final --------------------------------------------------------

// variable para validar si estamos en desarrollo o produccion
$entornoDesarrollo = false;

// Includes de libreria PHPMailer
// plugin_dir_path(__FILE__) nos da la direccion hasta la carpeta raiz de este archivo
// y concatenamos el resto necesario para tener acceso a los archivos que requerimos
// if ($entornoDesarrollo == true) {
    include_once(plugin_dir_path(__FILE__) . 'Mailer/src/PHPMailer.php');
    include_once(plugin_dir_path(__FILE__) . 'Mailer/src/SMTP.php');
    include_once(plugin_dir_path(__FILE__) . 'Mailer/src/Exception.php');
// }

// Dashicons de WP -------------------------------------------------------------
add_action('wp_enqueue_scripts', 'dcms_load_dashicons_front_end');

function dcms_load_dashicons_front_end()
{
    wp_enqueue_style('dashicons');
}
// Dashicons de WP -------------------------------------------------------------

// This file will create admin menu page
class WPRK_Create_Admin_Page
{

    public $pestania = "home";
    public $edit_state_api = false;
    public $edit_state_Packing_shipping_weight = false;
    public $edit_state_Discounts_volume = false;
    public $edit_state = false;

    // Constructor del panel de administracion del plugin
    public function __construct()
    {
        add_action('admin_menu', [$this, 'create_admin_menu']);
        wp_enqueue_style('css_aspirante', plugins_url('styles/bootstrap/css/bootstrap.min.css', __FILE__));
        ob_start();
    }

    // Creacion de panel de administracion
    public function create_admin_menu()
    {
        // Slug que aparecera en la barra lateral de WP
        $capability = 'manage_options';
        $slug = 'wprk-settings';
        // Lo agregamos a la barra
        add_menu_page(
            __('WP React Pikkop', 'wp-react-pikkop'),
            __('WP React Pikkop', 'wp-react-pikkop'),
            $capability,
            $slug,
            [$this, 'menu_page_template'],
            'dashicons-tickets'
        );
    }

    // Funciones para insercion en BD inicio -----------------------------------------------------------

    // Funcion para agregar un SKU_handling_charge
    public function save_SKU_handling_charge()
    {
        // global $wpdb nos permite manipular las funciones SQL dentro de WP
        global $wpdb;
        // Preguntamos si $_POST es distinto de vario al igual que los inputs
        // Si la condicion se cumple procedemos a realizar la insercion
        if (!empty($_POST) && $_POST['insert_SKU_handling_charge']) {

            $this->pestania = "home";
            // Asignamos el nombre de la tabla a a la variable $tabla_form
            // $wpdb->prefix nos devuelve el prefijo de las tablas de WP y concatenamos el nombre de la tabla
            $tabla_form = $wpdb->prefix . 'SKU_handling_charge';
            // Asignamos los valores recuperados de $_POST y los almacenamos en las variables siguientes

            $min = $_POST['min'];
            $max = $_POST['max'];
            $percentaje = $_POST['percentaje'];
            $create_at = date('Y-m-d H:i:s');
            $update_at = date('Y-m-d H:i:s');

            // Insercion en la BD
            $wpdb->insert(
                $tabla_form,
                array(
                    'min' => $min,
                    'max' => $max,
                    'percentaje' => $percentaje,
                    'create_at' => $create_at,
                    'update_at' => $update_at
                )
            );
            echo "
            </br>
            <div class='container alert alert-success'>
                <strong>Registro exitoso</strong>
            </div>";
        }
        /**
         * La función ob_start() sirve para indicarle a PHP que se ha de iniciar el
         * buffering de la salida, es decir, que debe empezar a guardar la salida
         * en un bufer interno, en vez de enviarla al cliente. De modo que,
         * aunque se escriba código HTML con echo o directamente fuera del
         * código PHP, no se enviará al navegador hasta que se ordene
         * explícitamente. O eventualmente, hasta que se acabe el
         * procesamiento de todo el archivo PHP.
         * (Refrescamos la pagina sin que se modifique la url)
         */
        ob_start();
    }

    // Funcion para update_SKU_handling_charge
    public function update_SKU_handling_charge()
    {
        global $wpdb;

        // Realizamos el if para ver que los inputs tengan contenido y asi poder insertar
        if (!empty($_POST) && $_POST['update_SKU_handling_charge']) {

            $this->pestania = "home";

            $id = $_POST['id_sku_hc'];

            // Asignamos los valores obtenidos mediante $_POST a variables
            $min = $_POST['min'];
            $max = $_POST['max'];
            $percentaje = $_POST['percentaje'];
            $update_at = date('Y-m-d H:i:s');

            // Edicion de los datos
            $tabla_form = $wpdb->prefix . 'SKU_handling_charge';
            $wpdb->update($tabla_form, array(
                'min' => $min,
                'max' => $max,
                'percentaje' => $percentaje,
                'update_at' => $update_at
            ), array('id' => $id));

            echo "
            </br>
            <div class='exito container alert alert-success'>
                Actualizacion exitosa
            </div>";
        }
        ob_start();
    }

    // Funcion para eliminar un item en submenu Weight Handling Fees
    public function delete_SKU_handling_charge()
    {
        global $wpdb;

        if (!empty($_POST) && $_POST['borrar_SKU_handling_charge']) {

            $idd = $_POST['busqueda_SKU_handling_charge'];

            $this->pestania = "home";

            global $wpdb;
            $tabla_form = $wpdb->prefix . 'SKU_handling_charge';

            $wpdb->delete($tabla_form, array('id' => $idd));


            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Registro eliminado exitosamente</strong>
            </div>";
        }

        ob_start();
    }

    // Funcion para agregar un item en submenu Price E-commerce
    public function save_Discounts_volume()
    {
        // global $wpdb nos permite manipular las funciones SQL dentro de WP
        global $wpdb;
        // Realizamos el if para ver que los inputs tengan contenido y asi poder insertar
        if (!empty($_POST) && $_POST['insert_Discounts_volume']) {

            $this->pestania = "menu1";
            // Asignamos los valores obtenidos mediante $_POST a variables
            $tabla_form_order = $wpdb->prefix . 'discounts_volume';
            $min = $_POST['min'];
            $max = $_POST['max'];
            $percentaje = $_POST['percentaje'];
            $create_at = date('Y-m-d H:i:s');
            $update_at = date('Y-m-d H:i:s');

            // Insercion en la BD
            $wpdb->insert(
                $tabla_form_order,
                array(
                    'min' => $min,
                    'max' => $max,
                    'percentaje' => $percentaje,
                    'create_at' => $create_at,
                    'update_at' => $update_at
                )
            );

            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Registro exitoso</strong>
            </div>";
        }

        ob_start();
    }

    // Funcion para agregar un item en submenu data_aditional
    public function saveItem_api()
    {
        // global $wpdb nos permite manipular las funciones SQL dentro de WP
        global $wpdb;
        // Realizamos el if para ver que los inputs tengan contenido y asi poder insertar
        if (!empty($_POST) && $_POST['insert_api']) {
            $this->pestania = "menu3";
            // Asignamos los valores obtenidos mediante $_POST a variables
            $tabla_form_api = $wpdb->prefix . 'data_aditional';
            $datakey = $_POST['key_api'];
            $p_almacenaje = $_POST['p_almacenaje'];
            $p_maniobra = $_POST['p_maniobra'];
            $p_envios = $_POST['p_envios'];
            $url_video = $_POST['url_video'];
            $create_at = date('Y-m-d H:i:s');
            $update_at = date('Y-m-d H:i:s');

            // Insercion en la BD
            $wpdb->insert(
                $tabla_form_api,
                array(
                    'datta' => $datakey,
                    'p_almacenaje' => $p_almacenaje,
                    'p_maniobra' => $p_maniobra,
                    'p_envios' => $p_envios,
                    'url_video' => $url_video,
                    'create_at' => $create_at,
                    'update_at' => $update_at
                )
            );

            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Registro exitoso</strong>   
            </div>";
        }

        ob_start();
    }

    // Funcion para agregar un packing_shipping_weight
    public function save_Packing_shipping_weight()
    {
        global $wpdb;
        // Realizamos el if para ver que los inputs tengan contenido y asi poder insertar
        if (!empty($_POST) && $_POST['insert_Packing_shipping_weight']) {
            $this->pestania = "menu2";

            $tabla_form_sub = $wpdb->prefix . 'packing_shipping_weight';
            $kgs = (float)$_POST['kgs'];
            $embalaje = (float)$_POST['embalaje'];
            $envio_mismo_dia = (float)$_POST['envio_mismo_dia'];
            $envio_estandar = (float)$_POST['envio_estandar'];
            $envio_dia_siguiente = (float)$_POST['envio_dia_siguiente'];
            $create_at = date('Y-m-d H:i:s');
            $update_at = date('Y-m-d H:i:s');

            // Insercion en la BD
            $wpdb->insert(
                $tabla_form_sub,
                array(
                    'kgs' => $kgs,
                    'embalaje' => $embalaje,
                    'envio_mismo_dia' => $envio_mismo_dia,
                    'envio_estandar' => $envio_estandar,
                    'envio_dia_siguiente' => $envio_dia_siguiente,
                    'create_at' => $create_at,
                    'update_at' => $update_at
                )
            );

            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Registro exitoso</strong>
            </div>";
        }

        ob_start();
    }

    // Funcion para editar un item en submenu Price Subscription
    public function update_packing_shipping_weight()
    {
        global $wpdb;
        if (!empty($_POST) && $_POST['update_Packing_shipping_weight']) {

            $this->pestania = "menu2";


            $id_packing_shipping_weight = $_POST['id_Packing_shipping_weight'];

            $kgs = (float)$_POST['kgs'];
            $embalaje = (float)$_POST['embalaje'];
            $envio_mismo_dia = (float)$_POST['envio_mismo_dia'];
            $envio_estandar = (float)$_POST['envio_estandar'];
            $envio_dia_siguiente = (float)$_POST['envio_dia_siguiente'];
            $update_at = date('Y-m-d H:i:s');
            //$idd = $_POST['id_usar'];

            global $wpdb;
            $tabla_form_sub = $wpdb->prefix . 'packing_shipping_weight';
            $wpdb->update(
                $tabla_form_sub,
                array(
                    'kgs' => $kgs,
                    'embalaje' => $embalaje,
                    'envio_mismo_dia' => $envio_mismo_dia,
                    'envio_estandar' => $envio_estandar,
                    'envio_dia_siguiente' => $envio_dia_siguiente,
                    'update_at' => $update_at
                ),
                array('id' => $id_packing_shipping_weight)
            );

            echo "
             </br>
             <div class='exito container alert alert-success'>
                 <strong>Actualización exitosa.... </strong>
             </div>";
        }
        ob_start();
    }

    // Funcion para eliminar un item en submenu Price Subscription
    public function delete_Packing_shipping_weight()
    {
        global $wpdb;

        if (!empty($_POST) && $_POST['borrar_Packing_shipping_weight']) {
            $id_packing_shipping_weight = $_POST['busqueda_Packing_shipping_weight'];

            $this->pestania = "menu2";

            global $wpdb;
            $tabla_form_sub_delete = $wpdb->prefix . 'packing_shipping_weight';
            $datos = $wpdb->get_results("SELECT * FROM $tabla_form_sub_delete");

            $wpdb->delete($tabla_form_sub_delete, array('id' => $id_packing_shipping_weight));

            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Registro eliminado exitosamente</strong>
            </div>";
        }

        ob_start();
    }



    // Funcion para editar un item en submenu Price E-commerce
    public function update_Discounts_volume()
    {
        global $wpdb;

        if (!empty($_POST) && $_POST['update_Discounts_volume']) {

            $this->pestania = "menu1";
            $id_DV = $_POST['id_Discounts_volume'];

            $min = $_POST['min'];
            $max = $_POST['max'];
            $percentaje = $_POST['percentaje'];
            $update_at = date('Y-m-d H:i:s');

            global $wpdb;
            $tabla_form_order = $wpdb->prefix . 'discounts_volume';
            $wpdb->update(
                $tabla_form_order,
                array(
                    'min' => $min,
                    'max' => $max,
                    'percentaje' => $percentaje,
                    'update_at' => $update_at
                ),
                array('id' => $id_DV)
            );

            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Actualización exitosa .....</strong>
            </div>";
        }
        ob_start();
    }

    // Funcion para eliminar un item en submenu Price E-commerce
    public function delete_Discounts_volume()
    {
        global $wpdb;

        if (!empty($_POST) && $_POST['borrar_Discounts_volume']) {
            $id_DV = $_POST['busqueda_Discounts_volume'];

            $this->pestania = "menu1";

            global $wpdb;
            $tabla_form_order_delete = $wpdb->prefix . 'discounts_volume';
            $wpdb->delete($tabla_form_order_delete, array('id' => $id_DV));

            echo "
             </br>
             <div class='exito container alert alert-success'>
                <strong>Registro eliminado exitosamente</strong>
             </div>";
        }

        ob_start();
    }

    // Funcion para editar un item en submenu data_aditional
    public function editItem_api()
    {
        global $wpdb;
        if (!empty($_POST) && $_POST['actualizar_api']) {

            $this->pestania = "menu3";
            $idd_api = $_POST['valorid_key'];

            $keyapi = $_POST['key_api'];
            $p_almacenaje = $_POST['p_almacenaje'];
            $p_maniobra = $_POST['p_maniobra'];
            $p_envios = $_POST['p_envios'];
            $url_video = $_POST['url_video'];
            $update_at = date('Y-m-d H:i:s');

            global $wpdb;
            $tabla_form_api = $wpdb->prefix . 'data_aditional';
            $wpdb->update(
                $tabla_form_api,
                array(
                    'datta' => $keyapi,
                    'url_video' => $url_video,
                    'p_almacenaje' => $p_almacenaje,
                    'p_maniobra' => $p_maniobra,
                    'p_envios' => $p_envios,
                    'update_at' => $update_at
                ),
                array('id' => $idd_api)
            );

            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Actualización exitosa</strong>
            </div>";
        }
        ob_start();
    }

    // Funcion para agregar un data_success_stories
    public function save_data_success_stories()
    {
        global $wpdb;
        // Realizamos el if para ver que los inputs tengan contenido y asi poder insertar
        if (!empty($_POST) && $_POST['insert_data_success_stories']) {
            $this->pestania = "menu5";

            //Seccion que guarda imagen %%%%%%%%
            function handle_my_file_upload()
            {
                // will return the attachment id of the image in the media library
                $attachment_id = media_handle_upload('my_file_field', 0);

                echo 'attachment_id ' . is_wp_error($attachment_id);

                // test if upload succeeded
                if (is_wp_error($attachment_id)) {
                    http_response_code(400);
                    echo 'Failed to upload file.';
                } else {
                    http_response_code(200);
                }

                // done!
                return $attachment_id;
                die();
            }

            // allow uploads from users that are logged in
            add_action('wp_ajax_my_file_upload', 'handle_my_file_upload');

            // allow uploads from guests
            add_action('wp_ajax_nopriv_my_file_upload', 'handle_my_file_upload');

            //Seccion que guarda imagen %%%%%%%%

            $tabla_form_sub = $wpdb->prefix . 'data_success_stories';
            $person_name = $_POST['person_name'];
            $company_name = $_POST['company_name'];
            $description = $_POST['description'];
            $imagen_prev = handle_my_file_upload();
            $imagen = wp_get_attachment_url($imagen_prev);
            $create_at = date('Y-m-d H:i:s');
            $update_at = date('Y-m-d H:i:s');

            // Insercion en la BD
            $wpdb->insert(
                $tabla_form_sub,
                array(
                    'person_name' => $person_name,
                    'company_name' => $company_name,
                    'description' => $description,
                    'image' => $imagen,
                    'create_at' => $create_at,
                    'update_at' => $update_at
                )
            );

            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Registro exitoso</strong>
            </div>";
        }

        ob_start();
    }

    // Funcion para editar un item data_success_stories
    public function update_data_success_stories()
    {
        global $wpdb;
        if (!empty($_POST) && $_POST['update_data_success_stories']) {

            $this->pestania = "menu5";

            //Seccion que guarda imagen %%%%%%%%
            function handle_my_file_upload()
            {
                // will return the attachment id of the image in the media library
                $attachment_id = media_handle_upload('my_file_field', 0);

                echo 'attachment_id ' . is_wp_error($attachment_id);

                // test if upload succeeded
                if (is_wp_error($attachment_id)) {
                    http_response_code(400);
                    echo 'Failed to upload file.';
                } else {
                    http_response_code(200);
                }

                // done!
                return $attachment_id;
                die();
            }

            // allow uploads from users that are logged in
            add_action('wp_ajax_my_file_upload', 'handle_my_file_upload');

            // allow uploads from guests
            add_action('wp_ajax_nopriv_my_file_upload', 'handle_my_file_upload');

            //Seccion que guarda imagen %%%%%%%%

            $id_data_success_stories = $_POST['id_data_success_stories'];

            $person_name = $_POST['person_name'];
            $company_name = $_POST['company_name'];
            $description = $_POST['description'];
            $imagen_prev = handle_my_file_upload();
            $imagen = wp_get_attachment_url($imagen_prev);
            $update_at = date('Y-m-d H:i:s');

            global $wpdb;
            $tabla_form_sub = $wpdb->prefix . 'data_success_stories';
            $wpdb->update(
                $tabla_form_sub,
                array(
                    'person_name' => $person_name,
                    'company_name' => $company_name,
                    'description' => $description,
                    'image' => $imagen,
                    'update_at' => $update_at
                ),
                array('id' => $id_data_success_stories)
            );

            echo "
             </br>
             <div class='exito container alert alert-success'>
                 <strong>Actualización exitosa.... </strong>
             </div>";
        }
        ob_start();
    }

    // Funcion para eliminar un item data_success_stories
    public function delete_data_success_stories()
    {
        global $wpdb;

        if (!empty($_POST) && $_POST['borrar_data_success_stories']) {
            $id_data_success_stories = $_POST['busqueda_data_success_stories'];

            $this->pestania = "menu5";

            global $wpdb;
            $tabla_form_sub_delete = $wpdb->prefix . 'data_success_stories';
            $datos = $wpdb->get_results("SELECT * FROM $tabla_form_sub_delete");

            $wpdb->delete($tabla_form_sub_delete, array('id' => $id_data_success_stories));

            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Registro eliminado exitosamente</strong>
            </div>";
        }

        ob_start();
    }

    // Funcion para eliminar un item en submenu data_aditional
    public function deleteItem_api()
    {
        global $wpdb;

        if (!empty($_POST) && $_POST['delete_api']) {
            $idd_api_delete = $_POST['busqueda_api'];

            $this->pestania = "menu3";

            global $wpdb;
            $tabla_form_api_delete = $wpdb->prefix . 'data_aditional';
            $datos = $wpdb->get_results("SELECT * FROM $tabla_form_api_delete");

            $wpdb->delete($tabla_form_api_delete, array('id' => $idd_api_delete));

            echo ($this->pestania);
            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Success...</strong> Data deleted!!...
            </div>";
        }

        ob_start();
    }

    // Funciones para la eliminacion en BD final --------------------------------------------------------

    // Funciones para en envio de emails inicio ----------------------------------------------------------

    // Funcion para el envio de emails
    public function send_mail()
    {

        // variable para validar si estamos en desarrollo o produccion
        $entornoDesarrollo = true;

        $this->pestania = "menu4";

        if ($entornoDesarrollo == false) {



            // Llamamos a plantilla HTML y los estilos para la misma
            // ambos se encuentran en la carpeta llamada plantilla
            $mensaje  = (plugin_dir_path(__FILE__) . 'plantilla/mensaje.html');
            $estilos = (plugin_dir_path(__FILE__) . 'plantilla/estilos.css');

            // Iniciamos con la consulta a la BD para obtener los datos necesarios
            global $wpdb;
            $tabla_form_mail = $wpdb->prefix . 'send_mail';
            $datos_mail = $wpdb->get_results("SELECT * FROM $tabla_form_mail");

            foreach ($datos_mail as $dato_mail) {
                $mail_sender = $dato_mail->mail_sender;
                $name_sender = $dato_mail->name_sender;
                $host = $dato_mail->host;
                $port = $dato_mail->port;
                $smtp_secure = $dato_mail->smtp_secure;
                $password_sender = $dato_mail->password_sender;
            }

            // Iniciamos con el envio del email con PHPMailer

            // Obtenemos los datos del formulario que son (destinatario, asunto y mensaje)
            $emailTo = $_POST['destinatario'];
            $Subject = $_POST['asunto'];
            $bodyEmail = $_POST['mensaje'];

            // Consultamos si los inputs tienen el contenido necesario para la funcion
            if (!empty($_POST) && $_POST['send'] && $_POST['mensaje'] && $_POST['asunto'] && $_POST['destinatario']) {

                // Asignamos los datos de la BD a las variables necesarias para el envio
                $fromemail = $mail_sender;
                $fromname = $name_sender;
                $host = $host;
                $port = $port;
                $SMTPAuth = true;
                $_SMTPSecure = $smtp_secure;
                $password = $password_sender;

                // Instanciamos a PHPMailer
                $mail = new PHPMailer\PHPMailer\PHPMailer();

                // Le decimos a  PHPMailer que utilize SMTP
                $mail->isSMTP();

                // Para saber si es para produccion o debug (colocamos 1 para debbug)
                $mail->SMTPDebug = 0;

                // Desde donde enviaremos el correo electronico
                $mail->Host = $host;
                $mail->Port = $port;
                $mail->SMTPAuth = $SMTPAuth;
                $mail->SMTPSecure = $_SMTPSecure;
                $mail->Username = $fromemail;
                $mail->Password = $password;

                // Asignamos el remitente y el nombre del remitente
                $mail->setFrom($fromemail, $fromname);

                // Asignamos el email a el destinatario
                $mail->addAddress($emailTo);

                // Asunto
                $mail->isHTML(true);
                $mail->Subject = $Subject;

                // Mensaje inicio -------------------------------------------------------
                // cargar archivo css para cuerpo de mensaje
                $rcss = $estilos; //ruta de archivo css
                $fcss = fopen($rcss, "r"); //abrir archivo css
                $scss = fread($fcss, filesize($rcss)); //leer contenido de css
                fclose($fcss); //cerrar archivo css

                // Cargar archivo html   
                $shtml = file_get_contents($mensaje);

                // reemplazar sección de plantilla html con el css cargado y mensaje creado
                $incss  = str_replace('<style id="estilo"></style>', "<style>$scss</style>", $shtml);
                $cuerpo = str_replace('<p id="mensaje"></p>', $bodyEmail, $incss);

                // Envio del mensaje
                $mail->Body = $cuerpo;
                // Mensaje final -------------------------------------------------------

                if (!$mail->send()) {
                    echo "
                        </br>
                        <div class='exito alert alert-danger'>
                            <strong>Success...</strong> Mail do not send!!...
                        </div>";
                    die();
                } else {
                    echo "
                    </br>
                    <div class='exito container alert alert-success'>
                        <strong>Success...</strong> Mail send!!...
                    </div>";

                    ob_start();
                }
            } else {
                // Varios destinatarios
                $para  = $emailTo;

                // título
                $título = 'Cotización de servicios Pikkop Air';

                // mensaje
                $mensaje = (plugin_dir_path(__FILE__) . 'plantilla/mensaje.html');

                // Para enviar un correo HTML, debe establecerse la cabecera Content-type
                $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
                $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                // Cabeceras adicionales
                $cabeceras .= 'To: ' . $emailTo  . "\r\n";
                $cabeceras .= ' From: ' . $mail_sender .  ' <cumples@example.com>' . "\r\n";

                // Enviarlo
                mail($para, $título, $mensaje, $cabeceras);
            }
        }

        ob_start();
    }

    // Funcion para insertar parametros requeridos para el envio de un email
    public function saveItem_mail()
    {
        global $wpdb;

        if (
            !empty($_POST) &&
            $_POST['savemail'] != '' &&
            $_POST['admin'] != '' &&
            $_POST['remitente'] != '' &&
            $_POST['nameremitente'] != '' &&
            $_POST['host'] != '' &&
            $_POST['puerto'] != '' &&
            $_POST['smtps'] != '' &&
            $_POST['password'] != ''
        ) {

            $this->pestania = "menu4";

            //$this->pestania = "menu4";
            $hashnopass = $_POST['password'];
            $hashpass =  password_hash($_POST['password'], PASSWORD_DEFAULT);

            $tabla_form_mail = $wpdb->prefix . 'send_mail';
            $admin_mail = $_POST['admin'];
            $mail_sender = $_POST['remitente'];
            $name_sender =  $_POST['admin']; // $_POST['remitente'];
            $host = $_POST['host'];
            $port = $_POST['puerto'];
            $smtp_secure = $_POST['smtps'];
            $password = $_POST['password'];
            $create_at = date('Y-m-d H:i:s');
            $update_at = date('Y-m-d H:i:s');

            // Insercion en la BD
            $wpdb->insert(
                $tabla_form_mail,
                array(
                    'mail_admin' => $admin_mail,
                    'mail_sender' => $mail_sender,
                    'name_sender' => $name_sender,
                    'host' => $host,
                    'port' => $port,
                    'smtp_secure' => $smtp_secure,
                    'password_sender' => $password,
                    'create_at' => $create_at,
                    'update_at' => $update_at
                )
            );
            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Thanks...</strong> Your data has been registered !! Thanks...
            </div>";
        }

        ob_start();
    }

    // Funcion para editar los parametros requeridos para el envio de un email
    public function editItem_mail()
    {
        global $wpdb;
        if (!empty($_POST) && $_POST['updatemail']) {
            $id_mail = $_POST['mail_id'];
            $admin_mail = $_POST['admin'];
            $remitente =  $_POST['admin']; // $_POST['remitente'];
            $remitente_nombre = $_POST['nameremitente'];
            $host = $_POST['host'];
            $puerto = $_POST['puerto'];
            $smtps = $_POST['smtps'];
            $password_sender = $_POST['password'];

            $this->pestania = "menu4";

            global $wpdb;
            $tabla_form_mail = $wpdb->prefix . 'send_mail';
            $wpdb->update(
                $tabla_form_mail,
                array(
                    'mail_admin' => $admin_mail,
                    'mail_sender' => $remitente,
                    'name_sender' => $remitente_nombre,
                    'host' => $host,
                    'port' => $puerto,
                    'smtp_secure' => $smtps,
                    'password_sender' => $password_sender
                ),
                array('id' => $id_mail)
            );

            echo "
            </br>
            <div class='exito container alert alert-success'>
                <strong>Success...</strong> Your data already update
            </div>";
        }
        ob_start();
    }

    // Funciones para en envio de emails final ----------------------------------------------------------

    // Funciones para almacenamiento de urls inicio ----------------------------------------------------------

    // Funcion para insertar parametros requeridos para el envio de un email
    public function saveItem_url()
    {
        global $wpdb;

        if (
            !empty($_POST) &&
            $_POST['saveurl'] &&
            $_POST['contactar_asesor'] != '' &&
            $_POST['agendar_llamada'] != '' &&
            $_POST['imprimir_cotizacion'] != '' &&
            $_POST['volver_cotizar'] != '' &&
            $_POST['linkedin'] != '' &&
            $_POST['twitter'] != '' &&
            $_POST['facebook'] != '' &&
            $_POST['instagram'] != '' &&
            $_POST['youtube'] != '' &&
            $_POST['tiktok'] != ''
        ) {

            $this->pestania = "menu5";

            //$this->pestania = "menu5";
            $tabla_form_url = $wpdb->prefix . 'url_redirect';
            $url_contactar_asesor = $_POST['contactar_asesor'];
            $url_agendar_llamada = $_POST['agendar_llamada'];
            $url_imprimir_cotizacion = $_POST['imprimir_cotizacion'];
            $url_volver_cotizar = $_POST['volver_cotizar'];
            $url_linkedin = $_POST['linkedin'];
            $url_twitter = $_POST['twitter'];
            $url_facebook = $_POST['facebook'];
            $url_instagram = $_POST['instagram'];
            $url_youtube = $_POST['youtube'];
            $url_tiktok = $_POST['tiktok'];
            $create_at = date('Y-m-d H:i:s');
            $update_at = date('Y-m-d H:i:s');

            // Insercion en la BD
            $wpdb->insert(
                $tabla_form_url,
                array(
                    'contactar_asesor' => $url_contactar_asesor,
                    'agendar_llamada' => $url_agendar_llamada,
                    'imprimir_cotizacion' => $url_imprimir_cotizacion,
                    'volver_cotizar' => $url_volver_cotizar,
                    'url_linkedin' => $url_linkedin,
                    'url_twitter' => $url_twitter,
                    'url_facebook' => $url_facebook,
                    'url_instagram' => $url_instagram,
                    'url_youtube' => $url_youtube,
                    'url_tiktok' => $url_tiktok,
                    'create_at' => $create_at,
                    'update_at' => $update_at
                )
            );
            echo "
            </br>
            <div class='exito alert alert-success'>
                <strong>Thanks...</strong> Your data has been registered !! Thanks...
            </div>";
        }

        ob_start();
    }

    // Funcion para editar los parametros requeridos para el envio de un email
    public function editItem_url()
    {
        global $wpdb;
        if (!empty($_POST) && $_POST['updateurl']) {

            $this->pestania = "menu5";

            $id_url = $_POST['url_id'];
            $url_contactar_asesor = $_POST['contactar_asesor'];
            $url_agendar_llamada = $_POST['agendar_llamada'];
            $url_imprimir_cotizacion = $_POST['imprimir_cotizacion'];
            $url_volver_cotizar = $_POST['volver_cotizar'];
            $url_linkedin = $_POST['linkedin'];
            $url_twitter = $_POST['twitter'];
            $url_facebook = $_POST['facebook'];
            $url_instagram = $_POST['instagram'];
            $url_youtube = $_POST['youtube'];
            $url_tiktok = $_POST['tiktok'];

            global $wpdb;
            $tabla_form_url = $wpdb->prefix . 'url_redirect';
            $wpdb->update(
                $tabla_form_url,
                array(
                    'contactar_asesor' => $url_contactar_asesor,
                    'agendar_llamada' => $url_agendar_llamada,
                    'imprimir_cotizacion' => $url_imprimir_cotizacion,
                    'volver_cotizar' => $url_volver_cotizar,
                    'url_linkedin' => $url_linkedin,
                    'url_twitter' => $url_twitter,
                    'url_facebook' => $url_facebook,
                    'url_instagram' => $url_instagram,
                    'url_youtube' => $url_youtube,
                    'url_tiktok' => $url_tiktok
                ),
                array('id' => $id_url)
            );

            echo "
            </br>
            <div class='exito alert alert-success'>
                <strong>Success...</strong> Your data already update
            </div>";
        }
        ob_start();
    }

    // Funciones para almacenamiento de urls final ----------------------------------------------------------

    // Funciones para complemento de edicion inicio ----------------------------------------------------------

    public function preEditItem_api()
    {
        if ($_POST['update_api']) {
            $this->edit_state_api = true;
            $this->pestania = "menu3";
        }
    }

    public function preEditItem_sub()
    {
        if ($_POST['editar_Packing_shipping_weight']) {
            $this->edit_state_Packing_shipping_weight = true;
            $this->pestania = "menu2";
        }
    }

    public function preEditItem_order()
    {
        if ($_POST['editar_Discounts_volume']) {
            $this->edit_state_Discounts_volume = true;
            $this->pestania = "menu1";
        }
    }

    public function preEditItem()
    {
        if ($_POST['editar_SKU_handling_charge']) {
            $this->edit_state = true;
            $this->pestania = "home";
        }
    }

    public function preEditItemCasoExito()
    {
        if ($_POST['editar_data_casos_exito']) {
            $this->edit_state = true;
            $this->pestania = "menu5";
        }
    }

    public function getContent_1()
    {

        // Consulta a la BD para mostrar datos en la tabla principal-----------------------------
        global $wpdb;
        $tabla_form = $wpdb->prefix . 'handling_fees';
        $datos = $wpdb->get_results("SELECT * FROM $tabla_form");

        // Consulta para obtener datos por medio del ID enviado con el boton editar INICIO-------------
        if (!empty($_POST) && $_POST['editar']) {
            $idd = $_POST['busqueda'];
        }
        $post = $wpdb->get_row("SELECT * FROM $tabla_form WHERE ID = $idd");
        $order = $post->weight_per_order;
        $fees = $post->weight_based_fees;
        // Consulta para obtener datos por medio del ID enviado con el boton editar FINAL-------------

        $contenido = '
            <div class="container-fluid p-4">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="card m-0 p-0">
                            <h5 class="card-header" style="display:flex; align-items:center;">
                                <span class="dashicons dashicons-insert" style="margin-right:0.2rem"></span>
                                New register
                            </h5>
                            <div class="card-body">
                                <form  action="' . get_the_permalink() . '" method="POST">
                                    <label for="">Weight Per order:</label> ';
        if ($this->edit_state == false) {
            $contenido .= '         <input class="form-control form-control-sm" type="text" name="weight_per_order" required>';
        } else {
            $contenido .= '         <input class="form-control form-control-sm" type="text" name="weight_per_order" value="' . $order . '" required>';
        }
        $contenido .= '             <label class="mt-3" for="">Weight based fees_</label>';
        if ($this->edit_state == false) {
            $contenido .= '         <input class="form-control form-control-sm" type="text" name="weight_based_fees" required>';
        } else {
            $contenido .= '         <input class="form-control form-control-sm" type="text" name="weight_based_fees" value="' . $fees . '" required>';
        }
        $contenido .= '             <!--Input oculto para obtener el id del item seleccionado para editar-->
                                    <input type="text" name="valorid" value="' . $idd . '" style="display:none"/>';
        if ($this->edit_state == false) {
            $contenido .= '         <input type="submit" name="insertar" value="Save" class="btn btn-primary btn-block mt-2">';
        } else {
            $contenido .= '         <input type="submit" name="actualizar" value="Update" class="btn btn-primary btn-block mt-2">';
        }
        $contenido .= '         </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                            <table class="table table-striped table-hover">
                                <thead class="thead-light ">
                                    <tr>
                                        <th>Weight Per order</th>
                                        <th>Weight based fees</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>';
        foreach ($datos as $dato) {
            $contenido .= '     <form action="" method="POST">
                                            <tr>
                                                <td class="text-center">' . $dato->weight_per_order . '</td>
                                                <td class="text-center"> $ ' . $dato->weight_based_fees . '</td>
                                                <td class="text-center"> 
                                                    <input type="submit" name="editar" value="Edit" class="btn btn-info">
                                                    <input type="submit" name="borrar" value="Delete" class="btn btn-danger">
                                                    <input type="text" name="busqueda" value="' .  $dato->id . '" style="display:none"/>
                                                </td>
                                            </tr>
                                </form>';
        }
        $contenido .= '        </tbody>
                            </table>
                         
                    </div> 
                </div> 
            </div>';

        return $contenido;
    }

    public function getContent_2()
    {
        global $wpdb;
        $tabla_form_order = $wpdb->prefix . 'price';
        $datos_order = $wpdb->get_results("SELECT * FROM $tabla_form_order");

        // Consulta para obtener datos por medio del ID enviado con el boton editar INICIO-------------
        if (!empty($_POST) && $_POST['edit_order']) {
            $id_order = $_POST['busqueda_order'];
        }
        $post_order = $wpdb->get_row("SELECT * FROM $tabla_form_order WHERE ID = $id_order");
        $type = 'EC_FF';
        $min = $post_order->minn;
        $max = $post_order->maxx;
        $picking_fees = $post_order->picking_fees;
        $additional_items = $post_order->additional_items;
        $promotional_inserts = $post_order->promotional_inserts;
        $return_processing = $post_order->return_processing;
        $return_processing_ai = $post_order->return_processing_ai;
        $d_same_day = $post_order->d_same_day;
        $d_ground = $post_order->d_ground;
        $d_next_day = $post_order->d_next_day;
        $r_ground = $post_order->r_ground;
        $r_next_day = $post_order->r_next_day;
        // Consulta para obtener datos por medio del ID enviado con el boton editar FINAL-------------

        $content = '
                    <div class="container-fluid p-4">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="card m-0 p-0">
                                    <h5 class="card-header" style="display:flex; align-items:center;">
                                        <span class="dashicons dashicons-insert" style="margin-right:0.2rem"></span>
                                        New register
                                    </h5>
                                    <div class="card-body">
                                        <form  action="' . get_the_permalink() . '" method="POST">
                                            <div class="row">';
        $content .= '
                                                <div class="col-6">
                                                    <label for="">Min:</label>';
        if ($this->edit_state_Discounts_volume == false) {
            $content .= '<input class="form-control form-control-sm" type="text" name="min" required>';
        } else {
            $content .= '<input class="form-control form-control-sm" type="text" name="min" value="' . $min . '" required>';
        }
        $content .= '
                                                </div>';

        $content .= '
                                                <div class="col-6">
                                                    <label class="" for="">Max:</label>';
        if ($this->edit_state_Discounts_volume == false) {
            $content .= '<input class="form-control form-control-sm" type="text" name="max" required>';
        } else {
            $content .= '<input class="form-control form-control-sm" type="text" name="max" value="' . $max . '" required>';
        }
        $content .= '
                                                </div>';

        $content .= '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">Picking fees:</label>';
        if ($this->edit_state_Discounts_volume == false) {
            $content .= '<input class="form-control form-control-sm" type="text" name="picking_fees" required>';
        } else {
            $content .= '<input class="form-control form-control-sm" type="text" name="picking_fees" value="' . $picking_fees . '" required>';
        }
        $content .= '
                                                </div>';

        $content .= '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">Additional items:</label>';
        if ($this->edit_state_Discounts_volume == false) {
            $content .= '<input class="form-control form-control-sm" type="text" name="additional_items" required>';
        } else {
            $content .= '<input class="form-control form-control-sm" type="text" name="additional_items" value="' . $additional_items . '" required>';
        }
        $content .= '
                                                </div>';

        $content .= '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">Promotional inserts:</label>';
        if ($this->edit_state_Discounts_volume == false) {
            $content .= '<input class="form-control form-control-sm" type="text" name="promotional_inserts" required>';
        } else {
            $content .= '<input class="form-control form-control-sm" type="text" name="promotional_inserts" value="' . $promotional_inserts . '" required>';
        }
        $content .= '
                                                </div>';

        $content .= '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">Return processing:</label>';
        if ($this->edit_state_Discounts_volume == false) {
            $content .= '<input class="form-control form-control-sm" type="text" name="return_processing" required>';
        } else {
            $content .= '<input class="form-control form-control-sm" type="text" name="return_processing" value="' . $return_processing . '" required>';
        }
        $content .= '
                                                </div>';

        $content .= '
                                                <div class="col-12">
                                                    <label class="mt-3" for="">Return processing AI:</label>';
        if ($this->edit_state_Discounts_volume == false) {
            $content .= '<input class="form-control form-control-sm" type="text" name="return_processing_ai" required>';
        } else {
            $content .= '<input class="form-control form-control-sm" type="text" name="return_processing_ai" value="' . $return_processing_ai . '" required>';
        }
        $content .= '
                                                </div>';

        $content .= '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">D same day:</label>';
        if ($this->edit_state_Discounts_volume == false) {
            $content .= '<input class="form-control form-control-sm" type="text" name="d_same_day" required>';
        } else {
            $content .= '<input class="form-control form-control-sm" type="text" name="d_same_day" value="' . $d_same_day . '" required>';
        }
        $content .= '
                                                </div>';

        $content .= '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">D Ground:</label>';
        if ($this->edit_state_Discounts_volume == false) {
            $content .= '<input class="form-control form-control-sm" type="text" name="d_ground" required>';
        } else {
            $content .= '<input class="form-control form-control-sm" type="text" name="d_ground" value="' . $d_ground . '" required>';
        }
        $content .= '
                                                </div>';

        $content .= '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">D next day:</label>';
        if ($this->edit_state_Discounts_volume == false) {
            $content .= '<input class="form-control form-control-sm" type="text" name="d_next_day" required>';
        } else {
            $content .= '<input class="form-control form-control-sm" type="text" name="d_next_day" value="' . $d_next_day . '" required>';
        }
        $content .= '
                                                </div>';

        $content .= '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">R ground:</label>';
        if ($this->edit_state_Discounts_volume == false) {
            $content .= '<input class="form-control form-control-sm" type="text" name="r_ground" required>';
        } else {
            $content .= '<input class="form-control form-control-sm" type="text" name="r_ground" value="' . $r_ground . '" required>';
        }
        $content .= '
                                                </div>';

        $content .= '
                                                <div class="col-12">
                                                    <label class="mt-3" for="">R next day:</label>';
        if ($this->edit_state_Discounts_volume == false) {
            $content .= '<input class="form-control form-control-sm" type="text" name="r_next_day" required>';
        } else {
            $content .= '<input class="form-control form-control-sm" type="text" name="r_next_day" value="' . $r_next_day . '" required>';
        }
        $content .= '
                                                </div>';
        $content .= '
                                            </div> <!--Row-->';
        // Inputs ocultos para obtener el id y el type para la funcion de edicion
        $content .= '
                                            <input type="text" name="valorid_order" value="' . $id_order . '" style="display:none" />';

        $content .= '
                                            <input type="text" name="type" value="' . $type . '" style="display:none" />';

        if ($this->edit_state_Discounts_volume == false) {
            $content .= '<input type="submit" name="insert_order" value="Save" class="mt-3 btn btn-primary btn-block mt-2">';
        } else {
            $content .= '<input type="submit" name="update_order" value="Update" class="mt-3 btn btn-primary btn-block mt-2">';
        }
        $content .= '
                                        </form>
                                    </div> <!--Card Body-->
                                </div> <!--Card-->
                            </div> <!--Col-->
                            <div class="col-sm-9">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>Min</th>
                                                <th>Max</th>
                                                <th>Picking fees</th>
                                                <th>Additional items</th>
                                                <th>Promotional inserts</th>
                                                <th>Return processing</th>
                                                <th>Return processing AI</th>
                                                <th>D same day</th>
                                                <th>D Ground</th>
                                                <th>D nex day</th>
                                                <th>R ground</th>
                                                <th>Rnext day</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
        foreach ($datos_order as $dato_order) {
            $id_dato = $dato_order->id;
            $type = $dato_order->typed;

            if ($type == 'EC_FF') {
                $min = $dato_order->minn;
                $max = $dato_order->maxx;
                $picking_fees = $dato_order->picking_fees;
                $additional_items = $dato_order->additional_items;
                $promotional_inserts = $dato_order->promotional_inserts;
                $return_processing = $dato_order->return_processing;
                $return_processing_ai = $dato_order->return_processing_ai;
                $d_same_day = $dato_order->d_same_day;
                $d_ground = $dato_order->d_ground;
                $d_next_day = $dato_order->d_next_day;
                $r_ground = $dato_order->r_ground;
                $r_next_day = $dato_order->r_next_day;
                $content .= '
                                                    <form action="" method="POST">
                                                        <tr>
                                                            <!--<td>' . $type . '</td>-->
                                                            <td class="text-right">' . $min . '</td>
                                                            <td class="text-right">' . $max . '</td>
                                                            <td class="text-right">' . $picking_fees . '</td>
                                                            <td class="text-right">' . $additional_items . '</td>
                                                            <td class="text-right">' . $promotional_inserts . '</td>
                                                            <td class="text-right">' . $return_processing . '</td>
                                                            <td class="text-right">' . $return_processing_ai . '</td>
                                                            <td class="text-right">' . $d_same_day . '</td>
                                                            <td class="text-right">' . $d_ground . '</td>
                                                            <td class="text-right">' . $d_next_day . '</td>
                                                            <td class="text-right">' . $r_ground . '</td>
                                                            <td class="text-right">' . $r_next_day . '</td>
                                                            <td class="text-center">';
                $content .= '
                                                                <input type="submit" name="edit_order" value="Edit" class="btn btn-info">
                                                                <input type="submit" name="borrar_order" value="Delete" class="btn btn-danger">
                                                                <input type="text" name="busqueda_order" value="' . $id_dato . '" style="display:none"/>
                                                            </td>
                                                        </tr>
                                                    </form>';
            }
        }
        $content .= '
                                        </tbody>
                                    </table>
                                </div> <!--Table responsive-->
                            </div> <!--Col-->
                        </div> <!--Row-->
                    </div> <!--Container-fluid-->';
        return $content;
    }

    public function SKU_handling_charge_content()
    {

        global $wpdb;
        $tabla_form = $wpdb->prefix . 'SKU_handling_charge';
        $datos = $wpdb->get_results("SELECT * FROM $tabla_form");

        $table_content = '';

        if (!empty($_POST) && $_POST['editar_SKU_handling_charge']) {
            $id_sku_hc = $_POST['busqueda_SKU_handling_charge'];
        }

        $item = $wpdb->get_row("SELECT * FROM $tabla_form WHERE ID = $id_sku_hc");

        foreach ($datos as $key => $dato) {
            $table_content .= '
                <form action="" method="POST">
                    <tr>
                        <th class="text-center" scope="row">' . ($key + 1) . '</th>
                        <td class="text-center">' . $dato->min . '</td>
                        <td class="text-center">' . $dato->max . '</td>
                        <td class="text-center">' . $dato->percentaje . ' %</td>
                        <td class="text-center">
                            <input type="text" name="busqueda_SKU_handling_charge" value="' .  $dato->id . '" style="display:none"/>
                            <input type="submit" name="editar_SKU_handling_charge" value="Editar" class="btn btn btn-success">
                            <input type="submit" name="borrar_SKU_handling_charge" value="Borrar" class="btn btn-danger">
                        </td>
                    </tr>
                </form>
            ';
        }

        $form_content = $this->edit_state ? '
            <form action="' . get_the_permalink() . '" method="POST">
                    <div class="mb-3">
                        <label class="form-label">Desde</label>
                        <input class="form-control form-control-sm" type="number" step="0.01" min="1" value=' . $item->min . ' name="min" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Hasta</label>
                        <input class="form-control form-control-sm" type="number" step="0.01" min="1" value=' . $item->max . ' name="max" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Porcentaje</label>
                        <input class="form-control form-control-sm" type="number" step="0.01" min="0" value=' . $item->percentaje . ' name="percentaje" required>
                    </div>
                    <input type="text" name="id_sku_hc" value="' . $id_sku_hc . '" style="display:none" />                 
                    <button type="submit" name="update_SKU_handling_charge" value="update_SKU_handling_charge" class="btn btn-block btn-primary">Guardar</button>
            </form>' : '
            <form action="' . get_the_permalink() . '" method="POST">
                <div class="mb-3">
                    <label class="form-label">Desde</label>
                    <input class="form-control form-control-sm" type="number" step="0.01" min="1" name="min" required>
                </div>
                <div class="mb-3">
                    <label class="form-label">Hasta</label>
                    <input class="form-control form-control-sm" type="number" step="0.01" min="1" name="max" required>
                </div>
                <div class="mb-3">
                    <label class="form-label">Porcentaje</label>
                    <input class="form-control form-control-sm" type="number" step="0.01" min="0" name="percentaje" required>
                </div>                 
                <button type="submit" name="insert_SKU_handling_charge" value="insert_SKU_handling_charge" class="btn btn-block btn-primary">Guardar</button>
        </form>
        ';

        $content = '
            <div class="row">
                <div class="col-sm-3">
                    <div class="card m-0 p-0">
                        <div class="card-header" style="display:flex; align-items:center;">
                            Registro de datos
                        </div>
                        <div class="card-body">
                            ' . $form_content . '
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center" scope="col">#</th>
                                <th class="text-center" scope="col">desde</th>
                                <th class="text-center" scope="col">Hasta</th>
                                <th class="text-center" scope="col">Porcetaje</th>
                                <th class="text-center" scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            ' . $table_content . '
                        </tbody>
                    </table>
                </div>
            </div>
        ';

        return $content;
    }

    public function Discounts_volume_content()
    {
        global $wpdb;
        $tabla_form = $wpdb->prefix . 'discounts_volume';
        $datos = $wpdb->get_results("SELECT * FROM $tabla_form");

        if (!empty($_POST) && $_POST['editar_Discounts_volume']) {
            $id_Discounts_volume = $_POST['busqueda_Discounts_volume'];
        }

        $item = $wpdb->get_row("SELECT * FROM $tabla_form WHERE ID = $id_Discounts_volume");

        $table_content = '';
        $form_content = '';

        foreach ($datos as $key => $dato) {
            $table_content .= '
                <form action="" method="POST">
                    <tr>
                        <th class="text-center" scope="row">' . ($key + 1) . '</th>
                        <td class="text-center">' . $dato->min . '</td>
                        <td class="text-center">' . $dato->max . '</td>
                        <td class="text-center">' . $dato->percentaje . ' %</td>
                        <td class="text-center">
                            <input type="text" name="busqueda_Discounts_volume" value="' .  $dato->id . '" style="display:none"/>
                            <input type="submit" name="editar_Discounts_volume" value="Editar" class="btn btn btn-success">
                            <input type="submit" name="borrar_Discounts_volume" value="Borrar" class="btn btn-danger">
                        </td>
                    </tr>
                </form>
            ';
        }

        $form_content = $this->edit_state_Discounts_volume ? '
            <form action="' . get_the_permalink() . '" method="POST">
                    <div class="mb-3">
                        <label class="form-label">Desde</label>
                        <input class="form-control form-control-sm" type="number" step="0.01" min="0" value=' . $item->min . ' name="min" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Hasta</label>
                        <input class="form-control form-control-sm" type="number" step="0.01" min="1" value=' . $item->max . ' name="max" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Porcentaje</label>
                        <input class="form-control form-control-sm" type="number" step="0.01" min="0" value=' . $item->percentaje . ' name="percentaje" required>
                    </div>
                    <input type="text" name="id_Discounts_volume" value="' . $id_Discounts_volume . '" style="display:none" />                 
                    <button type="submit" name="update_Discounts_volume" value="update_Discounts_volume" class="btn btn-block btn-primary">Guardar</button>
            </form>' : '
            <form action="' . get_the_permalink() . '" method="POST">
                <div class="mb-3">
                    <label class="form-label">Desde</label>
                    <input class="form-control form-control-sm" type="number" step="0.01" min="0" name="min" required>
                </div>
                <div class="mb-3">
                    <label class="form-label">Hasta</label>
                    <input class="form-control form-control-sm" type="number" step="0.01" min="1" name="max" required>
                </div>
                <div class="mb-3">
                    <label class="form-label">Porcentaje</label>
                    <input class="form-control form-control-sm" type="number" step="0.01" min="0" name="percentaje" required>
                </div>                 
                <button type="submit" name="insert_Discounts_volume" value="insert_Discounts_volume" class="btn btn-block btn-primary">Guardar</button>
        </form>
        ';

        $content = '
        <div class="row">
            <div class="col-sm-3">
                <div class="card m-0 p-0">
                    <div class="card-header" style="display:flex; align-items:center;">
                        Registro de datos
                    </div>
                    <div class="card-body">
                        ' . $form_content . '
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" scope="col">#</th>
                            <th class="text-center" scope="col">desde</th>
                            <th class="text-center" scope="col">Hasta</th>
                            <th class="text-center" scope="col">Porcetaje</th>
                            <th class="text-center" scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        ' . $table_content . '
                    </tbody>
                </table>
            </div>
        </div>
        ';

        return $content;
    }

    public function Packing_shipping_weight_content()
    {

        global $wpdb;
        $tabla_form = $wpdb->prefix . 'packing_shipping_weight';
        $datos = $wpdb->get_results("SELECT * FROM $tabla_form");

        if (!empty($_POST) && $_POST['editar_Packing_shipping_weight']) {
            $id_Packing_shipping_weight = $_POST['busqueda_Packing_shipping_weight'];
        }

        $item = $wpdb->get_row("SELECT * FROM $tabla_form WHERE ID = $id_Packing_shipping_weight");

        $table_content = '';
        $form_content = '';

        foreach ($datos as $key => $dato) {
            $table_content .= '
                <form action="" method="POST">
                    <tr>
                        <th class="text-center" scope="row">' . ($key + 1) . '</th>
                        <td class="text-center">' . $dato->kgs . ' kg.</td>
                        <td class="text-center">' . $dato->embalaje . '</td>
                        <td class="text-center">$ ' . number_format($dato->envio_mismo_dia, 2, '.', ' ') . ' </td>
                        <td class="text-center">$ ' . number_format($dato->envio_estandar, 2, '.', ' ') . ' </td>
                        <td class="text-center">$ ' . number_format($dato->envio_dia_siguiente, 2, '.', ' ') . ' </td>
                        <td class="text-center">
                            <input type="text" name="busqueda_Packing_shipping_weight" value="' .  $dato->id . '" style="display:none"/>
                            <input type="submit" name="editar_Packing_shipping_weight" value="Editar" class="btn btn btn-success">
                            <input type="submit" name="borrar_Packing_shipping_weight" value="Borrar" class="btn btn-danger">
                        </td>
                    </tr>
                </form>
            ';
        }

        $form_content = $this->edit_state_Packing_shipping_weight ? '
            <form action="' . get_the_permalink() . '" method="POST">
                    <div class="mb-3">
                        <label class="form-label">Kg</label>
                        <input class="form-control form-control-sm" type="number" step="0.01" min="0" value="' . $item->kgs . '" name="kgs" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Embalaje</label>
                        <input class="form-control form-control-sm" type="number" step="0.01" min="1" value="' . $item->embalaje . '" name="embalaje" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Precio envio el mismo día</label>
                        <input class="form-control form-control-sm" type="number" step="0.01" min="0" value="' . $item->envio_mismo_dia . '" name="envio_mismo_dia" required>
                    </div>  
                    <div class="mb-3">
                        <label class="form-label">Precio envio Estandar</label>
                        <input class="form-control form-control-sm" type="number" step="0.01" min="0" value="' . $item->envio_estandar . '" name="envio_estandar" required>
                    </div>  
                    <div class="mb-3">
                        <label class="form-label">Precio envio al día siguiente</label>
                        <input class="form-control form-control-sm" type="number" step="0.01" min="0" value="' . $item->envio_dia_siguiente . '" name="envio_dia_siguiente" required>
                    </div> 
                    <input type="text" name="id_Packing_shipping_weight" value="' . $id_Packing_shipping_weight . '" style="display:none" />                 
                    <button type="submit" name="update_Packing_shipping_weight" value="update_Packing_shipping_weight" class="btn btn-block btn-primary">Guardar</button>
            </form>' : '
            <form action="' . get_the_permalink() . '" method="POST">
                <div class="mb-3">
                    <label class="form-label">Kg</label>
                    <input class="form-control form-control-sm" type="number" step="0.01" min="0" name="kgs" required>
                </div>
                <div class="mb-3">
                    <label class="form-label">Embalaje</label>
                    <input class="form-control form-control-sm" type="number" step="0.01" min="1" name="embalaje" required>
                </div>
                <div class="mb-3">
                    <label class="form-label">Precio envio el mismo día</label>
                    <input class="form-control form-control-sm" type="number" step="0.01" min="0" name="envio_mismo_dia" required>
                </div>  
                <div class="mb-3">
                    <label class="form-label">Precio envio Estandar</label>
                    <input class="form-control form-control-sm" type="number" step="0.01" min="0" name="envio_estandar" required>
                </div>  
                <div class="mb-3">
                    <label class="form-label">Precio envio al día siguiente</label>
                    <input class="form-control form-control-sm" type="number" step="0.01" min="0" name="envio_dia_siguiente" required>
                </div>                 
                <button type="submit" name="insert_Packing_shipping_weight" value="insert_Packing_shipping_weight" class="btn btn-block btn-primary">Guardar</button>
        </form>
        ';


        $content = '
        <div class="row">
            <div class="col-sm-3">
                <div class="card m-0 p-0">
                    <div class="card-header" style="display:flex; align-items:center;">
                        Registro de datos
                    </div>
                    <div class="card-body">
                        ' . $form_content . '
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" scope="col">#</th>
                            <th class="text-center" scope="col">Kps</th>
                            <th class="text-center" scope="col">Embalage</th>
                            <th class="text-center" scope="col">Envio mismo día</th>
                            <th class="text-center" scope="col">Envio estandar</th>
                            <th class="text-center" scope="col">Envio al día siguiente</th>
                            <th class="text-center" scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        ' . $table_content . '
                    </tbody>
                </table>
            </div>
        </div>
        ';

        return $content;
    }

    public function data_aditional_plugin()
    {

        global $wpdb;
        $tabla_form_api = $wpdb->prefix . 'data_aditional';
        $datos_key = $wpdb->get_results("SELECT * FROM $tabla_form_api");

        $form_content = '
            <div class="mb-3">
                <label for="">Api key hubspot:</label>
                <input class="form-control form-control-sm" type="text" name="key_api" required>
            </div>
            <div class="mb-3">
                <label for="">Url video</label>
                <input class="form-control form-control-sm" type="text" name="url_video" required>
            </div>
            <div class="mb-3">
                <label for="">Precio de almacenaje</label>
                <input class="form-control form-control-sm" type="number" step="0.01" name="p_almacenaje" required>
            </div>
            <div class="mb-3">
                <label for="">Precio por maniobra de pedido</label>
                <input class="form-control form-control-sm" type="number" step="0.01" name="p_maniobra" required>
            </div>
            <div class="mb-3">
                <label for="">Precio de envíos nacionales desde</label>
                <input class="form-control form-control-sm" type="number" step="0.01" name="p_envios" required>
            </div>
            <input type="submit" name="insert_api" value="Guardar" class="btn btn-primary btn-block mt-2">
        ';

        $form_content_update = '
            <div class="mb-3">
                <label for="">Api key hubspot:</label>
                <input class="form-control form-control-sm" type="text" name="key_api" value="' . $datos_key[0]->datta . '" required>
            </div>
            <div class="mb-3">
                <label for="">Url video</label>
                <input class="form-control form-control-sm" type="text" name="url_video"  value="' . $datos_key[0]->url_video . '" required>
            </div>
            <div class="mb-3">
                <label for="">Precio de almacenaje</label>
                <input class="form-control form-control-sm" type="number" step="0.01" name="p_almacenaje"  value="' . $datos_key[0]->p_almacenaje . '" required>
            </div>
            <div class="mb-3">
                <label for="">Precio por maniobra de pedido</label>
                <input class="form-control form-control-sm" type="number" step="0.01" name="p_maniobra"  value="' . $datos_key[0]->p_maniobra . '" required>
            </div>
            <div class="mb-3">
                <label for="">Precio de envíos nacionales desde</label>
                <input class="form-control form-control-sm" type="number" step="0.01" name="p_envios"  value="' . $datos_key[0]->p_envios . '" required>
            </div>
            <input type="text" name="valorid_key" value="' . $datos_key[0]->id . '" style="display:none"/>
            <input type="submit" name="actualizar_api" value="Actualizar" class="btn btn-primary btn-block mt-2">
        ';

        $content = '
        <div class="row">
            <div class="col-sm-3">
                <div class="card m-0 p-0">
                    <div class="card-header" style="display:flex; align-items:center;">
                        Registro de datos
                    </div>
                    <div class="card-body">
                        <form  action="' . get_the_permalink() . '" method="POST">
                            ' . (!empty($datos_key) ? $form_content_update : $form_content) . '
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
            </div>
        </div>
        ';

        return $content;
    }

    public function data_configuration_mail()
    {

        // Consulta a la BD para mostrar datos en la tabla principal-----------------------------
        global $wpdb;
        $tabla_form_mail = $wpdb->prefix . 'send_mail';
        $datos_mail = $wpdb->get_results("SELECT * FROM $tabla_form_mail");

        foreach ($datos_mail as $dato_mail) {
            $mail_id = $dato_mail->id;
            $mail_admin = $dato_mail->mail_admin;
            $mail_sender = $dato_mail->mail_sender;
            $name_sender = $dato_mail->name_sender;
            $host = $dato_mail->host;
            $port = $dato_mail->port;
            $smtp_secure = $dato_mail->smtp_secure;
            $password_sender = $dato_mail->password_sender;
        }

        $form_content_test = '
            <div class="card m-0 p-0">
                <h5 class="card-header" style="display:flex; align-items:center;">
                    <span class="dashicons dashicons-insert" style="margin-right:0.2rem"></span>
                    Pruebas de envio de correo
                </h5>
                <div class="card-body">
                    <label for="">Destinatario</label>
                        <input class="form-control" type="text" name="destinatario">

                    <label for="">Asunto</label>
                        <input class="form-control" type="text" name="asunto">

                    <label class="mt-3" for="">Mensaje</label>
                        <textarea class="form-control" rows="5" type="text" name="mensaje"></textarea>

                    <input type="submit" name="send" value="Enviar" class="btn btn-primary btn-block mt-2">
                </div>
            </div>
        ';

        $form_content_configuration = '
            <label for="">Admin mail</label>
                <input class="form-control" type="email" name="admin" value=""> 
            <label for="">Sender</label>
                <input class="form-control" type="email" name="remitente" value=""> 
            <label for="">Sender\'s name</label>
                <input class="form-control" type="text" name="nameremitente" value=""> 
            <label for="">Host</label>
                <input class="form-control" type="text" name="host" value=""> 
            <label for="">Port</label>
                <input class="form-control" type="text" name="puerto" value=""> 
            <label for="">SMTP Secure</label>
                <input class="form-control" type="text" name="smtps" value=""> 
            <label for="">Password</label>
                <input class="form-control" type="password" name="password" value=""> 
            <input type="submit" name="savemail" value="Save" class="btn btn-primary btn-block mt-2"> 
        ';

        $form_content_configuration_update = '
            <label for="">Correo de administración</label>
                <input class="form-control" type="email" name="admin" value="' . $mail_admin . '">
            <label for="" class="mt-3">Nombre del remitente</label>
                <input class="form-control" type="text" name="nameremitente" value="' . $name_sender . '">
            <label for="" class="mt-3">Host</label>
                <input class="form-control" type="text" name="host" value="' . $host . '">
            <label for="" class="mt-3">Puerto</label>
                <input class="form-control" type="text" name="puerto" value="' . $port . '">
            <label for="" class="mt-3">SMTP Secure</label>
                <input class="form-control" type="text" name="smtps" value="' . $smtp_secure . '">
            <label for="" class="mt-3">Password</label>
                <input class="form-control" type="password" name="password" value="' . $password_sender . '">
            <input type="text" name="mail_id" value="' . $mail_id . '" style="display:none"/>
            <input type="submit" name="updatemail" value="Update" class="btn btn-primary btn-block mt-2">
        ';

        $content = '
            <div class="row">
                <div class="col-sm-12"> 
                    <form  action="' . get_the_permalink() . '" method="POST">
                        <div class="row">
                            <div class="col-sm-6">
                                ' . $form_content_test . '
                            </div> 
                            <div class="col-sm-6"> 
                                <div class="card m-0 p-0">
                                    <h5 class="card-header" style="display:flex; align-items:center;">
                                        <span class="dashicons dashicons-admin-generic" style="margin-right:0.2rem"></span>
                                        Configuración de envío de correo electrónico
                                    </h5>
                                    <div class="card-body">
                                        ' . (!empty($datos_mail) ? $form_content_configuration_update : $form_content_configuration) . '
                                    </div>  
                                </div>  
                            </div>  
                        </div>  
                    </form>
                </div>  
            </div>';

        return $content;
    }

    public function data_casos_exito()
    {

        global $wpdb;
        $tabla_form = $wpdb->prefix . 'data_success_stories';
        $datos = $wpdb->get_results("SELECT * FROM $tabla_form");

        $table_content = '';

        if (!empty($_POST) && $_POST['editar_data_casos_exito']) {
            $id_data_success_stories = $_POST['busqueda_data_success_stories'];
        }

        $item = $wpdb->get_row("SELECT * FROM $tabla_form WHERE ID = $id_data_success_stories");

        foreach ($datos as $key => $dato) {
            $table_content .= '
                <form action="" method="POST">
                    <tr>
                        <th class="text-center" scope="row">' . ($key + 1) . '</th>
                        <td class="text-center">' . $dato->person_name . '</td>
                        <td class="text-center">' . $dato->company_name . '</td>
                        <td class="text-center">' . $dato->description . '</td>
                        <td class="text-center">
                            <input type="text" name="busqueda_data_success_stories" value="' .  $dato->id . '" style="display:none"/>
                            <input type="submit" name="editar_data_casos_exito" value="Editar" class="btn btn btn-success">
                            <input type="submit" name="borrar_data_success_stories" value="Borrar" class="btn btn-danger">
                        </td>
                    </tr>
                </form>
            ';
        }

        $form_content = $this->edit_state ? '
            <form action="' . get_the_permalink() . '" method="POST" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label class="form-label">Nombre</label>
                        <input class="form-control" type="text" value=' . $item->person_name . ' name="person_name" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Nombre de la empresa</label>
                        <input class="form-control" type="text" value=' . $item->company_name . ' name="company_name" required>
                    </div>
                    <div>
                        <input type="hidden" name="action" value="my_file_upload"/>
                        <label for="image">Seleccionar imagen</label>
                        <input type="file" required id="image" name="my_file_field" accept="image/*"/>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Descripción</label>
                        <textarea class="form-control" rows="5" type="text" name="description">' . $item->description . '</textarea>
                    </div>
                    <input type="text" name="id_data_success_stories" value="' . $id_data_success_stories . '" style="display:none" />                 
                    <button type="submit" name="update_data_success_stories" value="update_data_success_stories" class="btn btn-block btn-primary">Guardar</button>
            </form>' : '
            <form action="' . get_the_permalink() . '" method="POST" enctype="multipart/form-data">
                <div class="mb-3">
                    <label class="form-label">Nombre</label>
                    <input class="form-control" type="text" name="person_name" required>
                </div>
                <div class="mb-3">
                    <label class="form-label">Nombre de la empresa</label>
                    <input class="form-control" type="text" name="company_name" required>
                </div>
                <div>
                    <input type="hidden" name="action" value="my_file_upload"/>
                    <label for="image">Seleccionar imagen</label>
                    <input type="file" required id="image" name="my_file_field" accept="image/*"/>
                </div>
                <div class="mb-3">
                    <label class="form-label">Descripción</label>
                    <textarea class="form-control" rows="5" type="text" name="description" required></textarea>
                </div>                 
                <button type="submit" name="insert_data_success_stories" value="insert_data_success_stories" class="btn btn-block btn-primary">Guardar</button>
        </form>
        ';

        $content = '
            <div class="row">
                <div class="col-sm-3">
                    <div class="card m-0 p-0">
                        <div class="card-header" style="display:flex; align-items:center;">
                            Registro de datos
                        </div>
                        <div class="card-body">
                            ' . $form_content . '
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center" scope="col">#</th>
                                <th class="text-center" scope="col">Nombre</th>
                                <th class="text-center" scope="col">Nombre de la empresa</th>
                                <th class="text-center" scope="col">Descripción</th>
                                <th class="text-center" scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            ' . $table_content . '
                        </tbody>
                    </table>
                </div>
            </div>
        ';

        return $content;
    }

    public function menu_page_template()
    {
        $this->save_SKU_handling_charge();
        $this->save_Packing_shipping_weight();
        $this->saveItem_api();
        $this->update_SKU_handling_charge();
        $this->save_Discounts_volume();
        $this->update_Discounts_volume();
        $this->update_packing_shipping_weight();
        $this->editItem_api();
        $this->delete_SKU_handling_charge();
        $this->delete_Discounts_volume();
        $this->delete_Packing_shipping_weight();
        $this->deleteItem_api();
        $this->send_mail();
        $this->saveItem_mail();
        $this->editItem_mail();
        $this->saveItem_url();
        $this->editItem_url();
        $this->preEditItem_api();
        $this->preEditItem_sub();
        $this->preEditItem_order();
        $this->preEditItem();
        $this->save_data_success_stories();
        $this->update_data_success_stories();
        $this->delete_data_success_stories();
        $this->preEditItemCasoExito();

        echo '
        <div class ="p-4">            
            <ul class="nav nav-pills" role="tablist">
                <li class="nav-item  ">
                    <a class="nav-link ' . ($this->pestania == "home" ? "active" : "no-active") . ' " data-toggle="pill" href="#home" style="display:flex; align-items:center;">
                        <span class="dashicons dashicons-money-alt icono_centrado" style="margin-right:0.2rem"></span>
                        Cargo por manejo de SKU
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ' . ($this->pestania == "menu1" ? "active" : "no-active") . '" data-toggle="pill" href="#menu1" style="display:flex; align-items:center;">
                        <span class="dashicons dashicons-cart" style="margin-right:0.2rem"></span>
                        Descuentos por volumen
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ' . ($this->pestania == "menu2" ? "active" : "no-active") . '" data-toggle="pill" href="#menu2" style="display:flex; align-items:center;">
                        <span class="dashicons dashicons-insert" style="margin-right:0.2rem"></span>
                        Precios de enbalaje
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ' . ($this->pestania == "menu3" ? "active" : "no-active") . '" data-toggle="pill" href="#menu3" style="display:flex; align-items:center;">
                        <span class="dashicons dashicons-admin-network" style="margin-right:0.2rem"></span>
                        Datos adicionales
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ' . ($this->pestania == "menu4" ? "active" : "no-active") . '" data-toggle="pill" href="#menu4" style="display:flex; align-items:center;">
                        <span class="dashicons dashicons-email-alt" style="margin-right:0.2rem"></span>
                        Datos de correo
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ' . ($this->pestania == "menu5" ? "active" : "no-active") . '" data-toggle="pill" href="#menu5" style="display:flex; align-items:center;">
                        <span class="dashicons dashicons-share-alt2" style="margin-right:0.2rem"></span>
                        Casos de éxito
                    </a>
                </li>
            </ul>
            
            <div class="tab-content mt-3">
                <div id="home" class=" tab-pane ' . ($this->pestania == "home" ? "active" : "no-active") . ' ">            
                ' . $this->SKU_handling_charge_content() . '
                </div>
                <!--Contenido 2-->
                <div id="menu1" class="tab-pane ' . ($this->pestania == "menu1" ? "active" : "no-active") . '">
                    ' . $this->Discounts_volume_content() . '
                </div>
                <div id="menu2" class="tab-pane ' . ($this->pestania == "menu2" ? "active" : "no-active") . '">
                    ' . $this->Packing_shipping_weight_content() . '
                </div>      
                <div id="menu3" class="tab-pane ' . ($this->pestania == "menu3" ? "active" : "no-active") . '">
                    ' . $this->data_aditional_plugin() . '
                </div> 
                <div id="menu4" class="tab-pane ' . ($this->pestania == "menu4" ? "active" : "no-active") . '">
                    ' . $this->data_configuration_mail() . '
                </div> 
                <div id="menu5" class="tab-pane ' . ($this->pestania == "menu5" ? "active" : "no-active") . '">
                    ' . $this->data_casos_exito() . '
                </div>      
            </div>

        </div
        ';
    }

    public function menu_page_template_old()
    {
        // Llamadas a todas las funciones
        $this->save_SKU_handling_charge();
        $this->update_Discounts_volume();
        $this->save_Packing_shipping_weight();
        $this->saveItem_api();
        $this->update_SKU_handling_charge();
        $this->update_Discounts_volume();
        $this->update_packing_shipping_weight();
        $this->editItem_api();
        $this->delete_SKU_handling_charge();
        $this->delete_Discounts_volume();
        $this->delete_Packing_shipping_weight();
        $this->deleteItem_api();
        $this->send_mail();
        $this->saveItem_mail();
        $this->editItem_mail();
        $this->saveItem_url();
        $this->editItem_url();
        $this->preEditItem_api();
        $this->preEditItem_sub();
        $this->preEditItem_order();
        $this->preEditItem();


        echo '
        <div class="">
            <hr>
            <h2>WP Pikkop Air</h2>
            <hr> 
            <br>
            <!--############# Menu con NavPills inicio #############-->
            <ul class="nav nav-pills" role="tablist">
                <li class="nav-item  ">
                    <a class="nav-link ' . ($this->pestania == "home" ? "active" : "no-active") . ' " data-toggle="pill" href="#home" style="display:flex; align-items:center;">
                        <span class="dashicons dashicons-money-alt icono_centrado" style="margin-right:0.2rem"></span>
                        Weight Handling Fees
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ' . ($this->pestania == "menu1" ? "active" : "no-active") . '" data-toggle="pill" href="#menu1" style="display:flex; align-items:center;">
                        <span class="dashicons dashicons-cart" style="margin-right:0.2rem"></span>
                        Price E-commerce
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ' . ($this->pestania == "menu2" ? "active" : "no-active") . '" data-toggle="pill" href="#menu2" style="display:flex; align-items:center;">
                        <span class="dashicons dashicons-insert" style="margin-right:0.2rem"></span>
                        Price Subscription
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ' . ($this->pestania == "menu3" ? "active" : "no-active") . '" data-toggle="pill" href="#menu3" style="display:flex; align-items:center;">
                        <span class="dashicons dashicons-admin-network" style="margin-right:0.2rem"></span>
                        Apikey
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ' . ($this->pestania == "menu4" ? "active" : "no-active") . '" data-toggle="pill" href="#menu4" style="display:flex; align-items:center;">
                        <span class="dashicons dashicons-email-alt" style="margin-right:0.2rem"></span>
                        Send mail
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ' . ($this->pestania == "menu5" ? "active" : "no-active") . '" data-toggle="pill" href="#menu5" style="display:flex; align-items:center;">
                        <span class="dashicons dashicons-share-alt2" style="margin-right:0.2rem"></span>
                        URL redirect
                    </a>
                </li>
            </ul>
            <!--############# Menu con NavPills inicio #############-->

            <!--############# Tab panes #############-->
            <div class="tab-content">
            
                <!--Contenido 1-->    
                <div id="home" class=" tab-pane ' . ($this->pestania == "home" ? "active" : "no-active") . ' ">            
                    ' . $this->getContent_1() . '
                </div>
                <!--Contenido 2-->
                <div id="menu1" class="tab-pane ' . ($this->pestania == "menu1" ? "active" : "no-active") . '">
                    ' . $this->getContent_2() . '
                </div>       

                <!--Contenido 3///////////////////////////////////////////////////////////////////////////////////////-->
                <div id="menu2" class="tab-pane ' . ($this->pestania == "menu2" ? "active" : "no-active") . '"><br>';
        // Consulta a la BD para mostrar datos en la tabla principal-----------------------------
        global $wpdb;
        $tabla_form_sub = $wpdb->prefix . 'price';
        $datos_sub = $wpdb->get_results("SELECT * FROM $tabla_form_sub");

        // Consulta para obtener datos por medio del ID enviado con el boton editar INICIO-------------
        if (!empty($_POST) && $_POST['edit_sub']) {
            $id_sub = $_POST['busqueda_sub'];
        }
        $post_sub = $wpdb->get_row("SELECT * FROM $tabla_form_sub WHERE ID = $id_sub");
        $type = "suscripcion";
        $min = $post_sub->minn;
        $max = $post_sub->maxx;
        $picking_fees = $post_sub->picking_fees;
        $additional_items = $post_sub->additional_items;
        $promotional_inserts = $post_sub->promotional_inserts;
        $return_processing = $post_sub->return_processing;
        $return_processing_ai = $post_sub->return_processing_ai;
        $d_same_day = $post_sub->d_same_day;
        $d_ground = $post_sub->d_ground;
        $d_next_day = $post_sub->d_next_day;
        $r_ground = $post_sub->r_ground;
        $r_next_day = $post_sub->r_next_day;
        // Consulta para obtener datos por medio del ID enviado con el boton editar FINAL-------------

        echo '
                    <div class="container-fluid p-4">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="card m-0 p-0">
                                    <h5 class="card-header" style="display:flex; align-items:center;">
                                        <span class="dashicons dashicons-insert" style="margin-right:0.2rem"></span>
                                        New register
                                    </h5>
                                    <div class="card-body">
                                        <form  action="' . get_the_permalink() . '" method="POST">
                                            <div class="row">';
        echo '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">Min:</label>';
        if ($this->edit_state_Packing_shipping_weight == false) {
            echo '<input class="form-control form-control-sm" type="text" name="min" required>';
        } else {
            echo '<input class="form-control form-control-sm" type="text" name="min" value="' . $min . '" required>';
        }
        echo '
                                                </div>';

        echo '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">Max:</label>';
        if ($this->edit_state_Packing_shipping_weight == false) {
            echo '<input class="form-control form-control-sm" type="text" name="max" required>';
        } else {
            echo '<input class="form-control form-control-sm" type="text" name="max" value="' . $max . '" required>';
        }
        echo '
                                                </div>';

        echo '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">Picking fees:</label>';
        if ($this->edit_state_Packing_shipping_weight == false) {
            echo '<input class="form-control form-control-sm" type="text" name="picking_fees" required>';
        } else {
            echo '<input class="form-control form-control-sm" type="text" name="picking_fees" value="' . $picking_fees . '" required>';
        }
        echo '
                                                </div>';

        echo '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">Additional items:</label>';
        if ($this->edit_state_Packing_shipping_weight == false) {
            echo '<input class="form-control form-control-sm" type="text" name="additional_items" required>';
        } else {
            echo '<input class="form-control form-control-sm" type="text" name="additional_items" value="' . $additional_items . '" required>';
        }
        echo '
                                                </div>';

        echo '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">Promotional inserts:</label>';
        if ($this->edit_state_Packing_shipping_weight == false) {
            echo '<input class="form-control form-control-sm" type="text" name="promotional_inserts" required>';
        } else {
            echo '<input class="form-control form-control-sm" type="text" name="promotional_inserts" value="' . $promotional_inserts . '" required>';
        }
        echo '
                                                </div>';

        echo '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">Return processing:</label>';
        if ($this->edit_state_Packing_shipping_weight == false) {
            echo '<input class="form-control form-control-sm" type="text" name="return_processing" required>';
        } else {
            echo '<input class="form-control form-control-sm" type="text" name="return_processing" value="' . $return_processing . '" required>';
        }
        echo '
                                                </div>';

        echo '
                                                <div class="col-12">
                                                    <label class="mt-3" for="">Return processing AI:</label>';
        if ($this->edit_state_Packing_shipping_weight == false) {
            echo '<input class="form-control form-control-sm" type="text" name="return_processing_ai" required>';
        } else {
            echo '<input class="form-control form-control-sm" type="text" name="return_processing_ai" value="' . $return_processing_ai . '" required>';
        }
        echo '
                                                </div>';

        echo '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">D same day:</label>';
        if ($this->edit_state_Packing_shipping_weight == false) {
            echo '<input class="form-control form-control-sm" type="text" name="d_same_day" required>';
        } else {
            echo '<input class="form-control form-control-sm" type="text" name="d_same_day" value="' . $d_same_day . '" required>';
        }
        echo '
                                                </div>';

        echo '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">D Ground:</label>
                                                        ';
        if ($this->edit_state_Packing_shipping_weight == false) {
            echo '<input class="form-control form-control-sm" type="text" name="d_ground" required>';
        } else {
            echo '<input class="form-control form-control-sm" type="text" name="d_ground" value="' . $d_ground . '" required>';
        }
        echo '
                                                </div>';

        echo '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">D next day:</label>
                                                        ';
        if ($this->edit_state_Packing_shipping_weight == false) {
            echo '<input class="form-control form-control-sm" type="text" name="d_next_day" required>';
        } else {
            echo '<input class="form-control form-control-sm" type="text" name="d_next_day" value="' . $d_next_day . '" required>';
        }
        echo '
                                                </div>';

        echo '
                                                <div class="col-6">
                                                    <label class="mt-3" for="">R ground:</label>';
        if ($this->edit_state_Packing_shipping_weight == false) {
            echo '<input class="form-control form-control-sm" type="text" name="r_ground" required>';
        } else {
            echo '<input class="form-control form-control-sm" type="text" name="r_ground" value="' . $r_ground . '" required>';
        }
        echo '
                                                </div>';

        echo '
                                                <div class="col-12">
                                                    <label class="mt-3" for="">R next day:</label>';
        if ($this->edit_state_Packing_shipping_weight == false) {
            echo '<input class="form-control form-control-sm" type="text" name="r_next_day" required>';
        } else {
            echo '<input class="form-control form-control-sm" type="text" name="r_next_day" value="' . $r_next_day . '" required>';
        }
        echo '
                                                </div>';
        echo '
                                            </div> <!--Row-->';

        echo '
                                            <input type="text" name="type" value="' . $type . '" style="display:none" />';

        echo '
                                            <input type="text" name="valorid_sub" value="' . $id_sub . '" style="display:none" />';

        if ($this->edit_state_Packing_shipping_weight == false) {
            echo '<input type="submit" name="insert_sub" value="Save" class="mt-3 btn btn-primary btn-block mt-2">';
        } else {
            echo '<input type="submit" name="update_sub" value="Update" class="mt-3 btn btn-primary btn-block mt-2">';
        }
        echo '
                                        </form>
                                    </div> <!--Card-body-->
                                </div> <!--Card-->
                            </div> <!--Col-->
                            <div class="col-sm-9">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead class="thead-light ">
                                            <tr>
                                                <th>Min</th>
                                                <th>Max</th>
                                                <th>Picking fees</th>
                                                <th>Additional items</th>
                                                <th>Promotional inserts</th>
                                                <th>Return processing</th>
                                                <th>Return processing AI</th>
                                                <th>D same day</th>
                                                <th>D Ground</th>
                                                <th>D next day</th>
                                                <th>R ground</th>
                                                <th>Rnext day</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
        foreach ($datos_sub as $dato_sub) {
            $id_dato = $dato_sub->id;
            $type = $dato_sub->typed;

            if ($type == 'suscripcion') {
                $min = $dato_sub->minn;
                $max = $dato_sub->maxx;
                $picking_fees = $dato_sub->picking_fees;
                $additional_items = $dato_sub->additional_items;
                $promotional_inserts = $dato_sub->promotional_inserts;
                $return_processing = $dato_sub->return_processing;
                $return_processing_ai = $dato_sub->return_processing_ai;
                $d_same_day = $dato_sub->d_same_day;
                $d_ground = $dato_sub->d_ground;
                $d_next_day = $dato_sub->d_next_day;
                $r_ground = $dato_sub->r_ground;
                $r_next_day = $dato_sub->r_next_day;
                echo '
                                                    <form action="" method="POST">
                                                        <tr>
                                                            <td class="text-center">' . $min . '</td>
                                                            <td class="text-center">' . $max . '</td>
                                                            <td class="text-center">' . $picking_fees . '</td>
                                                            <td class="text-center">' . $additional_items . '</td>
                                                            <td class="text-center">' . $promotional_inserts . '</td>
                                                            <td class="text-center">' . $return_processing . '</td>
                                                            <td class="text-center">' . $return_processing_ai . '</td>
                                                            <td class="text-center">' . $d_same_day . '</td>
                                                            <td class="text-center">' . $d_ground . '</td>
                                                            <td class="text-center">' . $d_next_day . '</td>
                                                            <td class="text-center">' . $r_ground . '</td>
                                                            <td class="text-center">' . $r_next_day . '</td>
                                                            <td class="text-center">';
                echo '
                                                                <input type="submit" name="edit_sub" value="Edit" class="btn btn-info">
                                                                <input type="submit" name="borrar_sub" value="Delete" class="btn btn-danger">
                                                                <input type="text" name="busqueda_sub" value="' . $id_dato . '" style="display:none"/>
                                                            </td>
                                                        </tr>
                                                    </form>';
            }
        }
        echo '
                                        </tbody>
                                    </table>
                                </div> <!--Table-responsive-->
                            </div> <!--Col-->
                        </div> <!--Row-->
                    </div> <!--Container-fluid-->
                </div> <!--Seccion Menu 2-->
                <!--Contenido 3///////////////////////////////////////////////////////////////////////////////////////-->

                <!--Contenido 4///////////////////////////////////////////////////////////////////////////////////////-->
                <div id="menu3" class="tab-pane ' . ($this->pestania == "menu3" ? "active" : "no-active") . '"><br>';
        // Consulta a la BD para mostrar datos en la tabla principal-----------------------------
        global $wpdb;
        $tabla_form_api = $wpdb->prefix . 'data_aditional';
        $datos_key = $wpdb->get_results("SELECT * FROM $tabla_form_api");

        // Consulta para obtener datos por medio del ID enviado con el boton editar INICIO-------------
        if (!empty($_POST) && $_POST['update_api']) {
            //$this->pestania = "menu3";
            $idd_api = $_POST['busqueda_api'];
        }

        $post_api = $wpdb->get_row("SELECT * FROM $tabla_form_api WHERE ID = $idd_api");
        $key_api = $post_api->datta;
        // Consulta para obtener datos por medio del ID enviado con el boton editar FINAL-------------

        echo '
                    <div class="container-fluid p-4">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="card m-0 p-0">
                                    <h5 class="card-header" style="display:flex; align-items:center;">
                                        <span class="dashicons dashicons-insert" style="margin-right:0.2rem"></span>
                                        New Apikey
                                    </h5>
                                    <div class="card-body">
                                        <form  action="' . get_the_permalink() . '" method="POST">
                                            <label for="">Api key:</label>';
        if ($this->edit_state_api == false) {
            echo '<input class="form-control form-control-sm" type="text" name="key_api" required>';
        } else {
            echo '<input class="form-control form-control-sm" type="text" name="key_api" value="' . $key_api . '" required>';
        }

        echo '
                                                <input type="text" name="valorid_key" value="' . $idd_api . '" style="display:none"/>';

        if ($this->edit_state_api == false) {
            echo '<input type="submit" name="insert_api" value="Save" class="btn btn-primary btn-block mt-2">';
        } else {
            echo '<input type="submit" name="actualizar_api" value="Update" class="btn btn-primary btn-block mt-2">';
        }
        echo '
                                        </form>
                                    </div> <!--Body-->
                                </div> <!--Card-->
                            </div> <!--Col-->
                            <div class="col-sm-8">
                                <div> <!--Tabla Apikey-->
                                    <table class="table table-striped table-hover">
                                        <thead class="thead-light ">
                                            <tr>
                                                <th class="text-center">Api key</th>
                                                <th class="text-center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
        foreach ($datos_key as $dato_key) {
            $id = $dato_key->id;
            $dat = $dato_key->datta;
            echo '
                                                <form action="" method="POST">
                                                    <tr>
                                                        <td class="text-center">' . $dat . '</td>
                                                        <td class="text-center">';
            echo '
                                                            <input type="submit" name="update_api" value="Edit" class="btn btn-info">
                                                            <input type="submit" name="delete_api" value="Delete" class="btn btn-danger"> </br>
                                                            <input type="text" name="busqueda_api" value="' . $id . '" style="display:none"/>
                                                        </td>
                                                    </tr>
                                                </form>';
        }
        echo '
                                        </tbody>
                                    </table>
                                </div> <!--Tabla Weight handling fees-->
                            </div> <!--Col-->
                        </div> <!--Row-->
                    </div> <!--Container-fluid-->
                </div> <!--Seccion Menu 3-->
                <!--Contenido 4///////////////////////////////////////////////////////////////////////////////////////-->

                <!--Contenido 4///////////////////////////////////////////////////////////////////////////////////////-->
                <div id="menu4" class="tab-pane ' . ($this->pestania == "menu4" ? "active" : "no-active") . '"><br>
                
                <!--Contenido 4///////////////////////////////////////////////////////////////////////////////////////-->

                <!--Contenido 5///////////////////////////////////////////////////////////////////////////////////////-->
                <div id="menu5" class="tab-pane ' . ($this->pestania == "menu5" ? "active" : "no-active") . '"><br>
                    <div class="container-fluid p-4">
                        <div class="row">
                            <div class="col-sm-12"> 
                                <form  action="' . get_the_permalink() . '" method="POST">
                                    <div class="row">
                                        <div class="col-sm-6">';
        // Consulta a la BD para mostrar datos en la tabla principal-----------------------------
        global $wpdb;
        $tabla_form_url = $wpdb->prefix . 'url_redirect';
        $datos_url = $wpdb->get_results("SELECT * FROM $tabla_form_url");

        foreach ($datos_url as $dato_url) {
            $url_id = $dato_url->id;
            $url_contactar_asesor = $dato_url->contactar_asesor;
            $url_agendar_llamada = $dato_url->agendar_llamada;
            $url_imprimir_cotizacion = $dato_url->imprimir_cotizacion;
            $url_volver_cotizar = $dato_url->volver_cotizar;
            $url_linkedin = $dato_url->url_linkedin;
            $url_twitter = $dato_url->url_twitter;
            $url_facebook = $dato_url->url_facebook;
            $url_instagram = $dato_url->url_instagram;
            $url_youtube = $dato_url->url_youtube;
            $url_tiktok = $dato_url->url_tiktok;
        }
        $contenido_url = 1;

        if (!empty($datos_url)) {
            $contenido_url = 2;
        }
        echo '
                                            <div class="card m-0 p-0">
                                                <h5 class="card-header" style="display:flex; align-items:center;">
                                                    <span class="dashicons dashicons-admin-generic" style="margin-right:0.2rem"></span>
                                                    Email url settings
                                                </h5>
                                                <div class="card-body">';
        if ($contenido_url == 2) {
            echo '
                                                        <label for="">Contactar a un asesor de ventas</label>
                                                            <input class="form-control" type="url" name="contactar_asesor" value="' . $url_contactar_asesor . '">';
        } else {
            echo '
                                                        <label for="">Contactar a un asesor de ventas</label>
                                                            <input class="form-control" type="url" name="contactar_asesor" value="">';
        }

        if ($contenido_url == 2) {
            echo '
                                                        <label for="" class="mt-3">Agendar llamada con un experto</label>
                                                            <input class="form-control" type="url" name="agendar_llamada" value="' . $url_agendar_llamada . '">';
        } else {
            echo '
                                                        <label for="">Agendar llamada con un experto</label>
                                                            <input class="form-control" type="url" name="agendar_llamada" value="">';
        }

        if ($contenido_url == 2) {
            echo '
                                                        <label for="" class="mt-3">Imprimir cotizacion</label>
                                                            <input class="form-control" type="url" name="imprimir_cotizacion" value="' . $url_imprimir_cotizacion . '">';
        } else {
            echo '
                                                        <label for="">Imprimir cotizacion</label>
                                                            <input class="form-control" type="url" name="imprimir_cotizacion" value="">';
        }

        if ($contenido_url == 2) {
            echo '
                                                        <label for="" class="mt-3">Volver a cotizar</label>
                                                            <input class="form-control" type="url" name="volver_cotizar" value="' . $url_volver_cotizar . '">';
        } else {
            echo '
                                                        <label for="">Volver a cotizar</label>
                                                            <input class="form-control" type="url" name="volver_cotizar" value="">';
        }

        if ($contenido_url == 2) {
            echo '
                                                        <label for="" class="mt-3">Linkedin</label>
                                                            <input class="form-control" type="url" name="linkedin" value="' . $url_linkedin . '">';
        } else {
            echo '
                                                        <label for="">Linkedin</label>
                                                            <input class="form-control" type="url" name="linkedin" value="">';
        }

        if ($contenido_url == 2) {
            echo '
                                                        <label for="" class="mt-3">Twitter</label>
                                                            <input class="form-control" type="url" name="twitter" value="' . $url_twitter . '">';
        } else {
            echo '
                                                        <label for="">Twitter</label>
                                                            <input class="form-control" type="url" name="twitter" value="">';
        }

        if ($contenido_url == 2) {
            echo '
                                                        <label for="" class="mt-3">Facebook</label>
                                                            <input class="form-control" type="url" name="facebook" value="' . $url_facebook . '">';
        } else {
            echo '
                                                        <label for="">Facebook</label>
                                                            <input class="form-control" type="url" name="facebook" value="">';
        }

        if ($contenido_url == 2) {
            echo '
                                                        <label for="" class="mt-3">Instagram</label>
                                                            <input class="form-control" type="url" name="instagram" value="' . $url_instagram . '">';
        } else {
            echo '
                                                        <label for="">Instagram</label>
                                                            <input class="form-control" type="url" name="instagram" value="">';
        }

        if ($contenido_url == 2) {
            echo '
                                                        <label for="" class="mt-3">Youtube</label>
                                                            <input class="form-control" type="url" name="youtube" value="' . $url_youtube . '">';
        } else {
            echo '
                                                        <label for="">Youtube</label>
                                                            <input class="form-control" type="url" name="youtube" value="">';
        }

        if ($contenido_url == 2) {
            echo '
                                                        <label for="" class="mt-3">Tiktok</label>
                                                            <input class="form-control" type="url" name="tiktok" value="' . $url_tiktok . '">';
        } else {
            echo '
                                                        <label for="">Tiktok</label>
                                                            <input class="form-control" type="url" name="tiktok" value="">';
        }

        echo '
                                                    <input type="text" name="url_id" value="' . $url_id . '" style="display:none"/>';

        if ($contenido_url == 2) {
            echo '
                                                        <input type="submit" name="updateurl" value="Update" class="btn btn-primary btn-block mt-2">';
        } else {
            echo '
                                                        <input type="submit" name="saveurl" value="Save" class="btn btn-primary btn-block mt-2">';
        }

        echo '
                                                </div> <!--Card-body-->
                                            </div> <!--Card-->
                                        </div> <!--Col-sm-6-->
                                    </div> <!--Row-->
                                </form>
                            </div> <!--Col-sm-12-->
                        </div> <!--Row-->
                    </div> <!--Container-fluid-->
                </div> <!--Seccion Menu 4-->
                <!--Contenido 5///////////////////////////////////////////////////////////////////////////////////////-->

            </div> <!--Tab content-->
        </div> <!--Class final-->
        <!--############# ----------Tab panes-------- #############-->';
    }
}
new WPRK_Create_Admin_Page();
