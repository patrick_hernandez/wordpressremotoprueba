<?php

require './vendor/autoload.php';

class MyReadFilter implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter {

    public function readCell($column, $row, $worksheetName = '') {
        /** Ignoramos la columna 0 para evitar que se ingrese a la bd informacion ajena */
        if ($row > 1) {
            return true;
        }
        return false;
    }
}

?>