<?php 

/** Acceso a funcionalidades de WP */
define('WP_USE_THEMES', false);
require('../../../../../wp-blog-header.php');


// Registrar a Bootstrap como dependencia
//(nombre), (url), (dependencias), (version), (medi)
wp_register_style('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css', '', '5.0', 'all');

// Agregarlas
wp_enqueue_style( 'estilos', get_stylesheet_uri(), array('bootstrap'), '1.0', 'all');

// Scripts a usar
// Inicializa
wp_register_script('popper', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js', '', '1.16.1', true);
// Carga
wp_enqueue_script('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js', array('jquery', 'popper'), '5.0', true);

require 'vendor/autoload.php';


/** Almacenamiento de los archivos */
$nombre = $_FILES['archivo']['name'];
$guardado = $_FILES['archivo']['tmp_name'];
/** Comprobamos si existe la carpeta donde los almacenaremos
 * en caso de que no exista se creara\
 */
if (!file_exists('archivos')) {
    mkdir('archivos',0777,true);
    if (file_exists('archivos')) {
        if (move_uploaded_file($guardado, 'archivos/' . $nombre)) {
            echo "Subida correcta";
        } else {
            echo "Subida incorrecta";
        }
    }
} else {
    if (move_uploaded_file($guardado, 'archivos/' . $nombre)) {
        echo "Subida correcta";
    } else {
        echo "Subida incorrecta";
    }
}


class MyReadFilter implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter {

    public function readCell($column, $row, $worksheetName = '') {
        /** Ignoramos la columna 0 para evitar que se ingrese a la bd informacion ajena */
        if ($row > 1) {
            return true;
        }
        return false;
    }
}

/** Create a new Xls Reader  **/
$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();

/** Document */
$inputFileName = './archivos/prueba.xlsx';

/**  Identify the type of $inputFileName  **/
$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
/**  Create a new Reader of the type that has been identified  **/
$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

/** Read especific to function > 1 */
$reader->setReadFilter( new MyReadFilter() );

/**  Load $inputFileName to a Spreadsheet Object  **/
$spreadsheet = $reader->load($inputFileName);

$cantidad = $spreadsheet->getActiveSheet()->toArray();

// Insercion de datos mediante $wpdb
global $wpdb;
$tabla_form = $wpdb->prefix . 'handling_fees';
// $datos = $wpdb->get_results("SELECT * FROM $tabla_form");
$create_at = date('Y-m-d H:i:s');
$update_at = date('Y-m-d H:i:s');

/** Recorremos el array */
foreach($cantidad as $row) {
    if ($row[0] != "") {
        // Insercion en la BD
        $wpdb->insert(
            $tabla_form,
            array(
                'weight_per_order' => $row[0],
                'weight_based_fees' => $row[1],
                'create_at' => $create_at,
                'update_at' => $update_at
            )
        );
    }
}

echo "
        <div class='exito alert alert-success'>
            <strong>Thanks...</strong> Your data has been registered !! Thanks...
        </div>";

?>
