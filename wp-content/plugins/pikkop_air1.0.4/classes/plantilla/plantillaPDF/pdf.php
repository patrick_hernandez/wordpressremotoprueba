<?php

/** Plantilla para la impresion de PDF
 * Tiene como funcion principal getPlantilla que es llamada desde 
 */

function getPlantilla($data)
{
    // Inclodes de los archivos html y css
    $pikkop_logo  = (plugin_dir_path(__FILE__) . 'assets/pikkop_logo.png');
    $pikkop_cubo  = (plugin_dir_path(__FILE__) . 'assets/pikkop_cubo.png');

    // Plantilla solo de fullfilment
    $fullfillment = $data['changeInFF'] == 0 ? '' : '';
    
    // Plantilla solo de storage
    $storage = $data['changeInStorage'] == 0 ? '' : '
        
    <tr bgcolor="#ffffff">
    <td>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="font-size: 0; line-height: 0;" width="100">
                    &nbsp;
                </td>
                <td bgcolor="#ffffff" width="260" valign="top" style="
                        box-shadow: 0px 0px 21px -4px #1E1E1E;
                        border: solid 1px #f1f0f0;
                        border-radius: 6%;
                        padding: 40px;
                        ">
                    <p style="
                        font-size: 1.2rem;
                        color: #0b296b;
                        font-family: \'Exo\', sans-serif;
                        ">
                        <span style="text-align: left;">
                            Almacenaje
                        </span>
                        
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="font-size: 1.0rem;
                                        font-family: \'Exo\', sans-serif;
                                        font-weight: 500;
                                        text-align: left;
                                        color: #808285">
                                ¿Cuántos SKUs incluye tu inventario?
                                <br>
                                ¿Peso promedio de tus artículos?
                                <br>
                                ¿Cuántos artículos tendrás por inventario?
                                <br>
                                ¿Valor promedio de tus artículos?
                                <br>
                            </td>
                            <td class="mulish" style="font-size: 1.0rem;
                                            font-family: \'Exo\', sans-serif;
                                            font-weight: 500;
                                            text-align: right;
                                            color: #808285">
                                        '.$data['step1Sku'].'
                                        <!--Reemplazar-->
                                        <br>
                                        '.replace_($data['step1PromArticulos']).' kg
                                        <br>
                                        '.$data['step1ArticulosInventario'].'
                                        <br>
                                        $ '.replace_($data['step1ValPromArticulos']).'
                                    </td>
                        </tr>
                        <br>
                    </table>
                    <br>
                    <br>
                    <span style="
                                text-align: left; 
                                font-size: 1.2rem; 
                                color: #0b296b;
                                font-family: \'Exo\', sans-serif">
                        Maniobra
                    </span>
                    
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="font-size: 1.0rem;
                                        font-family: \'Exo\', sans-serif;
                                        font-weight: 500;
                                        text-align: left;
                                        color: #808285">
                                ¿Cuantos artículos se incluyen por pedido?
                                <br>
                                ¿Cuántos pedidos esperas procesar al día?
                                <br>
                            </td>
                            <td class="mulish" style="font-size: 1.0rem;
                                            font-family: \'Exo\', sans-serif;
                                            font-weight: 500;
                                            text-align: right;
                                            color: #808285">
                                        '.$data['step2ArticulosPedido'].'
                                        <!--Reemplazar-->
                                        <br>
                                        '.$data['step2PedidosDia'].'
                                    </td>
                        </tr>
                        <br>
                    </table>
                    <!-- <br>
                    <br>
                    <span style="
                                text-align: left; 
                                font-size: 1.2rem; 
                                color: #0b296b;
                                font-family: \'Exo\', sans-serif">
                        Tipo de productos
                    </span>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="font-size: 1.0rem;
                                        font-family: \'Exo\', sans-serif;
                                        font-weight: 500;
                                        text-align: left;
                                        color: #808285">
                                        '.$data['typeProductSales'].'
                                <br>
                            </td>
                        </tr>
                        <br>
                    </table> -->
                    </p>
                </td>
                <td style="font-size: 0; line-height: 0;" width="100">
                    &nbsp;
                </td>
            </tr>
        </table>
        <br>
    </td>
</tr>
        <tr bgcolor="#ffffff">
            <td>
                <table>
                    <tr>
                        <td style="font-size: 0; line-height: 0;" width="120">
                            &nbsp;
                        </td>
                        <td class="mulish centrar">
                            <p style="font-size: 1.2rem;
                                font-family: \'Exo\', sans-serif;
                                text-align: center;
                                padding-top: 18px;
                                font-weight: 700;
                                color: #0b296b;
                                ">
                                Con la información anterior a continuación te mostramos la
                                cotización
                                personalizada para cubrir tus necesidades
                            </p>
                        </td>
                        <td style="font-size: 0; line-height: 0;" width="120">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <br>
            </td>
        </tr>
        


        <tr bgcolor="#ffffff">
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="font-size: 0; line-height: 0;" width="100">
                    &nbsp;
                </td>
                <td bgcolor="#ffffff" width="260" valign="top" style="
                        box-shadow: 0px 0px 21px -4px #1E1E1E;
                        border: solid 1px #f1f0f0;
                        border-radius: 6%;
                        padding: 40px;
                        ">
                    <p style="
                        font-size: 1.2rem;
                        color: #0b296b;
                        font-family: \'Exo\', sans-serif;
                        ">
                        <p style="font-size: 1.2rem;
                            font-family: \'Mulish\', sans-serif;
                            text-align: center;
                            font-weight: 700;
                            color: #0b296b;
                            margin-top: 0px;
                            ">
                            Almacenaje por pieza
                        </p>
                        <br>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="font-size: 1.0rem;
                                    font-family: \'Exo\', sans-serif;
                                    font-weight: 500;
                                    text-align: left;
                                    color: #808285">
                                Recepción a detalle por pieza
                            </td>
                            <td style="font-size: 1.0rem;
                                    font-family: \'Exo\', sans-serif;
                                    font-weight: 500;
                                    text-align: right;
                                    color: #808285">
                                    $ '.replace_($data['recepcionPieza']).'
                            </td>
                        </tr>
                        <br>
                    </table>
                    <hr style="border: 1px solid #0b296b; border-style: dashed;">
                    <br>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="font-size: 1.0rem;
                                    font-family: \'Exo\', sans-serif;
                                    font-weight: 500;
                                    text-align: left;
                                    color: #808285">
                                Almacenaje por pieza por semana
                                <br>
                                NOTA: Aplica peso volumétrico y peso real.
                            </td>
                            <td style="font-size: 1.0rem;
                                    font-family: \'Exo\', sans-serif;
                                    font-weight: 500;
                                    text-align: right;
                                    color: #808285">
                                    $ '.replace_($data['almacenajeSemana']).'
                            </td>
                        </tr>
                        <br>
                    </table>
                    <br>
                    <p style="font-size: 1.2rem;
                        font-family: \'Mulish\', sans-serif;
                        text-align: center;
                        font-weight: 700;
                        color: #0b296b;
                        margin-top: 0px;
                        ">
                        Maniobra
                    </p>
                    <br>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="font-size: 1.0rem;
                                    font-family: \'Exo\', sans-serif;
                                    font-weight: 500;
                                    text-align: left;
                                    color: #808285">
                                Surtido de pedido promedio &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td style="font-size: 1.0rem;
                                    font-family: \'Exo\', sans-serif;
                                    font-weight: 500;
                                    text-align: right;
                                    color: #808285">
                                    $ '.replace_($data['surtidoPedido']).'
                            </td>
                        </tr>
                        <br>
                    </table>
                    <hr style="border: 1px solid #0b296b; border-style: dashed;">
                    <br>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="font-size: 1.0rem;
                                    font-family: \'Exo\', sans-serif;
                                    font-weight: 500;
                                    text-align: left;
                                    color: #808285">
                                Plaza adicional &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td style="font-size: 1.0rem;
                                    font-family: \'Exo\', sans-serif;
                                    font-weight: 500;
                                    text-align: right;
                                    color: #808285">
                                    $ '.replace_($data['piezaAdicional']).'
                            </td>
                        </tr>
                    </table>
                    <br>
                    <hr style="border: 1px solid #0b296b; border-style: dashed;">
                    <br>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="font-size: 1.0rem;
                                    font-family: \'Exo\', sans-serif;
                                    font-weight: 500;
                                    text-align: left;
                                    color: #808285">
                                Embalaje (material) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td style="font-size: 1.0rem;
                                    font-family: \'Exo\', sans-serif;
                                    font-weight: 500;
                                    text-align: right;
                                    color: #808285">
                                    $ '.replace_($data['embalaje']).'
                            </td>
                        </tr>
                        <br>
                    </table>
                    <br>
                    <p style="font-size: 1.2rem;
                        font-family: \'Mulish\', sans-serif;
                        text-align: center;
                        font-weight: 700;
                        color: #0b296b;
                        margin-top: 0px;
                        ">
                        Tarifas de envío
                    </p>
                    <br>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="font-size: 1.0rem;
                                    font-family: \'Exo\', sans-serif;
                                    font-weight: 500;
                                    text-align: left;
                                    color: #808285">
                                Mismo día &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <br>
                                Estándar
                                <br>
                                Día siguiente
                            </td>
                            <td style="font-size: 1.0rem;
                                    font-family: \'Exo\', sans-serif;
                                    font-weight: 500;
                                    text-align: right;
                                    color: #808285">
                                    $ '.replace_($data['envioMismoDia']).'
                                <br>
                                $ '.replace_($data['envioEstandar']).'
                                <br>
                                $ '.replace_($data['envioDiaSiguiente']).'
                            </td>
                        </tr>
                        <br>
                    </table>
                    </p>
                </td>
                <td style="font-size: 0; line-height: 0;" width="100">
                    &nbsp;
                </td>
            </tr>
        </table>
    </td>
</tr>
    ';

    // Plantilla principal (Contiene todo el cuerpo del PDF)
    $plantilla = '
        <body style="margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <table>
                            <!--Imagen de PIKKOP AIR-->
                            <tr>
                                <td align="center" bgcolor="#ffffff" style="padding: 40px 0 30px 0;">
                                    <img width="400" height="330" src="' . $pikkop_logo . '" alt="imagen-pikkop" />
                                </td>
                            </tr>
                            <tr>
                                <td class="exo" bgcolor="#ffffff" style="text-align: center; padding: 40px 30px 40px 30px;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <!--Titulo -> Gracias por cotizar con Pikkop Air-->
                                        <tr class="exo">
                                            <td id="exo"
                                                style="color: #0b296b;
                                                font-size: 3.0rem;
                                                text-align: center;">
                                                <b class="exo">Gracias por cotizar con <br>Pikkop Air!!</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="mulish"
                                                style="padding: 20px 0 30px 0;
                                                color: #808285;
                                                font-size: 1.2rem;
                                                line-height: 26px;
                                                text-align: center;">
                                                <b>El almacenaje con Pikkop Air es simple, confiable y excelente.</b>
                                                <br>
                                                Con un almacenaje local y fulfillment, tú negocio puede ofrecer envíos al
                                                día siguiente o el mismo día a tus clientes, y así incrementar tus ventas y
                                                fidelidad de cliente.
                                                <br>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="font-size: 0; line-height: 0;" width="150">
                                                            &nbsp;
                                                        </td>
                                                        <td class="mulish">
                                                            <br>
                                                            <p class="subtitulo3">
                                                                Te enviamos tú cotización personalizada de almacenaje y maniobra
                                                                de recibo.
                                                            </p>
                                                        </td>
                                                        <td style="font-size: 0; line-height: 0;" width="150">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr bgcolor="" style="background-color: #ffffff;">
                                            <td>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="font-size: 0; line-height: 0;" width="100">
                                                            &nbsp;
                                                        </td>
                                                        <td class="centrar mulish">
                                                            <p style="font-size: 1.2rem;
                                                                
                                                                text-align: center;
                                                                font-weight: 700;
                                                                color: #0b296b;
                                                                ">
                                                                Resúmen de los datos recopilados que obtuvimos sobre tú negocio
                                                                en la cotización:
                                                            </p>
                                                        </td>
                                                        <td style="font-size: 0; line-height: 0;" width="100">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br>
                                                <br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        ' . $fullfillment . '
                                    </table>
                                </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        ' . $storage . '
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td bgcolor="" style="background-color: #faf9fe; padding: 30px 30px 30px 30px;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td style="font-size: 0; line-height: 0;" width="100">
                                                &nbsp;
                                            </td>
                                            <td class="centrar mulish">
                                                <p style="color: #153643;
                                                    font-size: 2.0rem;
                                                    text-align: center;">
                                                    <b>¡Recuerda que tenemos tarifas fijas de almacenaje!</b>
                                                </p>
                                                <br>
                                                <p style="font-size: 1.2rem;
                                                    color: #808285;
                                                    text-align: center;">
                                                    Nuestras tarifas dependen del espacio que necesites para almacenar tus
                                                    productos
                                                </p>
                                            </td>
                                            <td style="font-size: 0; line-height: 0;" width="100">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        <tr>
                            <td bgcolor="" style="background-color: #faf9fe; padding: 30px 30px 30px 30px;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="visibility: hidden; text-align: center;">
                                            <br />
                                            <label style="font-size: 1.2rem;">

                                            </label>
                                        </td>
                                        <td class="mulish" style="text-align: center;">
                                            <img style="width: 6%; margin: 0px 4px;" src="' . $pikkop_cubo . '" /><br />
                                            <label
                                                style="color: #0b296b;">
                                                Chico
                                            </label>
                                        </td>
                                        <td class="mulish" style="text-align: center;">
                                            <img style="width: 8%; margin: 0px 4px;" src="' . $pikkop_cubo . '" /><br />
                                            <label
                                                style="color: #0b296b;">
                                                Mediano
                                            </label>
                                        </td>
                                        <td class="mulish" style="text-align: center;">
                                            <img style="width: 10%; margin: 0px 4px;" src="' . $pikkop_cubo . '" /><br />
                                            <label
                                                style="color: #0b296b; font-size: 1.2rem;">
                                                Grande
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="mulish"
                                            style="
                                                text-align: center;
                                                color: #0b296b;
                                                font-weight: 800;
                                                font-size: 1.2rem;
                                            ">
                                            Tamaño
                                        </td>
                                        <td class="mulish"
                                            style="
                                                text-align: center;
                                                color: #808285;
                                                font-weight: 500;
                                                font-size: 1.2rem;
                                            ">
                                            80x30x45
                                        </td>
                                        <td class="mulish"
                                            style="
                                                text-align: center;
                                                color: #808285;
                                                font-weight: 500;
                                                font-size: 1.2rem;
                                            ">
                                            80x60x45
                                        </td>
                                        <td class="mulish"
                                            style="
                                                text-align: center;
                                                color: #808285;
                                                font-weight: 500;
                                                font-size: 1.2rem;
                                            ">
                                            90x60x45
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="mulish"
                                            style="
                                            text-align: center;
                                            color: #0b296b;
                                            font-weight: 800;
                                            font-size: 1.2rem;
                                            ">
                                            Semanal
                                        </td>
                                        <td class="mulish"
                                            style="
                                                text-align: center;
                                                color: #808285;
                                                font-weight: 500;
                                                font-size: 1.2rem;
                                            ">
                                            $40.00
                                        </td>
                                        <td class="mulish"
                                            style="
                                                text-align: center;
                                                color: #808285;
                                                font-weight: 500;
                                                font-size: 1.2rem;
                                            ">
                                            $45.00
                                        </td>
                                        <td class="mulish"
                                            style="
                                                text-align: center;
                                                color: #808285;
                                                font-weight: 500;
                                                font-size: 1.2rem;
                                            ">
                                            $50.00
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="mulish"
                                            style="
                                            text-align: center;
                                            color: #0b296b;
                                            font-weight: 800;
                                            font-size: 1.2rem;
                                            ">
                                            Mensual
                                        </td>
                                        <td class="mulish"
                                            style="
                                                text-align: center;
                                                color: #808285;
                                                font-weight: 500;
                                                font-size: 1.2rem;
                                            ">
                                            $135.00
                                        </td>
                                        <td class="mulish"
                                            style="
                                                text-align: center;
                                                color: #808285;
                                                font-weight: 500;
                                                font-size: 1.2rem;
                                            ">
                                            $140.00
                                        </td>
                                        <td class="mulish"
                                            style="
                                                text-align: center;
                                                color: #808285;
                                                font-weight: 500;
                                                font-size: 1.2rem;
                                            ">
                                            $146.00
                                        </td>
                                    </tr>
                                </table>
                                <br>
                                <hr style="border: 1px solid black; border-style: dashed;">
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="" style="background-color: #faf9fe; padding: 30px 30px 30px 30px;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="font-size: 0; line-height: 0;" width="100">
                                            &nbsp;
                                        </td>
                                        <td class="mulish">
                                            <p style="color: #153643;
                                                font-size: 1.2rem;
                                                text-align: center;">
                                                <b>Tarifas fijas de recepción</b>
                                            </p>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td class="mulish">
                                                        <p style="font-size: 1.2rem;
                                                            color: #808285;
                                                            text-align: left;">
                                                            Recepción detallada por pieza <br>
                                                            <span style="font-size: 1.0rem;">
                                                                (pesamos la caja y los articulos)
                                                            </span>
                                                        </p>
                                                    </td>
                                                    <td class="mulish">
                                                        <p style="text-align: right;
                                                            font-size: 1.2rem;
                                                            color: #808285;">
                                                            $30.00
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="mulish">
                                                        <p style="text-align: left;
                                                            font-size: 1.2rem;
                                                            color: #808285;">
                                                            Recepción por lote sin revisión
                                                        </p>
                                                    </td>
                                                    <td class="mulish">
                                                        <p style="text-align: right;
                                                            font-size: 1.2rem;
                                                            color: #808285;">
                                                            $950.00
                                                        </p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="font-size: 0; line-height: 0;" width="100">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="" style="text-align: center; padding: 30px 30px 30px 30px; background-color: #0b296b;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="centrar mulish">
                                            <p style="
                                                text-align: center;
                                                color: white;
                                                font-size: 1.2rem;
                                                ">
                                                Copyright © 2021 Pikkop. Todos los derechos reservados
                                            </p>
                                            <br>
                                            <br>
                                            <p style="
                                                text-align: center;
                                                color: white;
                                                font-size: 1.2rem;
                                                ">
                                                <b>Pikkop</b>
                                                <br>
                                                Gutenberg 60 B <br>
                                                Veronica Anzures <br>
                                                México City. CDMX 11300 <br>
                                                México <br>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
    ';

    // Retornamos plantilla
    return $plantilla;
}

function formatPrice($FF_orderMonth, $price)
{
    if ($FF_orderMonth > 10000) {
        return 'Contactanos';
    } else {
        return number_format($price, 2, '.', ' ');
    }
}

function replace_($value)
{
    $vf = str_replace('_', '.', $value);
    $vf = number_format($vf,2,'.',' ');

    return $vf;
}
