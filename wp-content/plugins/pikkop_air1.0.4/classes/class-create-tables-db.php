<?php

function Sku_handling_charge($wpdb, $charset_collate)
{
    $charset_collate = $wpdb->get_charset_collate();
    $tabla_form = $wpdb->prefix . 'SKU_handling_charge';
    $query_tablaform = "CREATE TABLE IF NOT EXISTS $tabla_form (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        min float NOT NULL,
        max float NOT NULL,
        percentaje float NOT NULL,
        create_at datetime NOT NULL,
        update_at datetime NOT NULL,
        UNIQUE (id)
    ) $charset_collate";

    dbDelta($query_tablaform);
}

function Discounts_volume($wpdb, $charset_collate)
{
    $charset_collate = $wpdb->get_charset_collate();
    $tabla_form = $wpdb->prefix . 'discounts_volume';
    $query_tablaform = "CREATE TABLE IF NOT EXISTS $tabla_form (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        min float NOT NULL,
        max float NOT NULL,
        percentaje float NOT NULL,
        create_at datetime NOT NULL,
        update_at datetime NOT NULL,
        UNIQUE (id)
    ) $charset_collate";

    dbDelta($query_tablaform);
}

function Packing_shipping_weight($wpdb, $charset_collate)
{
    $charset_collate = $wpdb->get_charset_collate();
    $tabla_form = $wpdb->prefix . 'packing_shipping_weight';
    $query_tablaform = "CREATE TABLE IF NOT EXISTS $tabla_form (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        kgs int NOT NULL,
        embalaje float NOT NULL,
        envio_mismo_dia float NOT NULL,
        envio_estandar float NOT NULL,
        envio_dia_siguiente float NOT NULL,
        create_at datetime NOT NULL,
        update_at datetime NOT NULL,
        UNIQUE (id)
    ) $charset_collate";

    dbDelta($query_tablaform);
}

function Handling_fees($wpdb, $charset_collate)
{
    $tabla_form = $wpdb->prefix . 'handling_fees';
    $query_tablaform = "CREATE TABLE IF NOT EXISTS $tabla_form (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        weight_per_order float NOT NULL,
        weight_based_fees float NOT NULL,
        create_at datetime NOT NULL,
        update_at datetime NOT NULL,
        UNIQUE (id)
    ) $charset_collate";

    dbDelta($query_tablaform);
}

function Price($wpdb, $charset_collate)
{
    $tabla_form = $wpdb->prefix . 'price';
    $query_price = "CREATE TABLE IF NOT EXISTS $tabla_form (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        typed varchar(40) NOT NULL,
        minn float NOT NULL,
        maxx float NOT NULL,
        picking_fees float NOT NULL,
        additional_items float NOT NULL,
        promotional_inserts float NOT NULL,
        return_processing float NOT NULL,
        return_processing_ai float NOT NULL,
        d_same_day float NOT NULL,
        d_ground float NOT NULL,
        d_next_day float NOT NULL,
        r_ground float NOT NULL,
        r_next_day float NOT NULL,
        create_at datetime NOT NULL,
        update_at datetime NOT NULL,
        UNIQUE (id)
    ) $charset_collate";

    dbDelta($query_price);
}

function Data_aditional($wpdb, $charset_collate)
{
    $tabla_form = $wpdb->prefix . 'data_aditional';
    $query_data = "CREATE TABLE IF NOT EXISTS $tabla_form (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        datta varchar(40) NOT NULL,
        p_almacenaje float NOT NULL,
        p_maniobra float NOT NULL,
        p_envios float NOT NULL,
        url_video varchar(500) NOT NULL,
        create_at datetime NOT NULL,
        update_at datetime NOT NULL,
        UNIQUE (id)
    ) $charset_collate";

    dbDelta($query_data);
}

function Send_mail($wpdb, $charset_collate)
{
    $tabla_form = $wpdb->prefix . 'send_mail';
    $query_mail = "CREATE TABLE IF NOT EXISTS $tabla_form (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        mail_admin varchar(40) NOT NULL,
        mail_sender varchar(40) NOT NULL,
        name_sender varchar(40) NOT NULL,
        host varchar(40) NOT NULL,
        port varchar(40) NOT NULL,
        smtp_secure varchar(40) NOT NULL,
        password_sender varchar(100) NOT NULL,
        create_at datetime NOT NULL,
        update_at datetime NOT NULL,
        UNIQUE (id)
    ) $charset_collate";

    dbDelta($query_mail);
}

function Url_redirect($wpdb, $charset_collate)
{
    $tabla_form = $wpdb->prefix . 'url_redirect';
    $query_url = "CREATE TABLE IF NOT EXISTS $tabla_form (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        contactar_asesor varchar(40) NOT NULL,
        agendar_llamada varchar(40) NOT NULL,
        imprimir_cotizacion varchar(40) NOT NULL,
        volver_cotizar varchar(40) NOT NULL,
        url_linkedin varchar(40) NOT NULL,
        url_twitter varchar(40) NOT NULL,
        url_facebook varchar(40) NOT NULL,
        url_instagram varchar(40) NOT NULL,
        url_youtube varchar(40) NOT NULL,
        url_tiktok varchar(40) NOT NULL,
        create_at datetime NOT NULL,
        update_at datetime NOT NULL,
        UNIQUE (id)
    ) $charset_collate";

    dbDelta($query_url);
}

function Data_success_stories($wpdb, $charset_collate)
{
    $charset_collate = $wpdb->get_charset_collate();
    $tabla_form = $wpdb->prefix . 'data_success_stories';
    $query_tablaform = "CREATE TABLE IF NOT EXISTS $tabla_form (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        person_name varchar(50) NOT NULL,
        company_name varchar(80) NOT NULL,
        description varchar(250) NOT NULL,
        image varchar(250) NOT NULL,
        create_at datetime NOT NULL,
        update_at datetime NOT NULL,
        UNIQUE (id)
    ) $charset_collate";

    dbDelta($query_tablaform);
}