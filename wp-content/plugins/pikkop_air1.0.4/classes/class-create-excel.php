<?php 
define('WP_USE_THEMES', false);
require('../../../../wp-blog-header.php');

header("Content-Type: application/xls");
//productos.xls es el nombre del archivo
header("Content-Disposition: attachment; filename= consult.xls");
?>

<table class="table">

  <tr>
    <th>Weight Per order</th>
    <th>Weight based fees</th>
  </tr>

<tbody>

<?php

global $wpdb;
echo 'Gracias por la consulta';
$tabla_form = $wpdb->prefix . 'handling_fees';
$datos = $wpdb->get_results("SELECT * FROM $tabla_form");

foreach ($datos as $dato) {
  $weight_per_order = $dato->weight_per_order;
  $weight_based_fees = $dato->weight_based_fees;
  echo '
      <tr>
          <td>' . $weight_per_order . '</td>
          <td> $ ' . $weight_based_fees . '</td>
      </tr>';
}
echo '
  </tbody>
</table>
';

?>
