<?php

/**
 * This file will create Custom Rest API End Points.
 */
class WP_React_Settings_Rest_Route
{

    public function __construct()
    {
        add_action('rest_api_init', [$this, 'create_rest_routes']);
    }

    /**
     * crea la apis necesarias para obtener los datos de la BD
     */
    public function create_rest_routes()
    {
        register_rest_route('wprk/v1', '/settings', [
            'methods' => 'GET',
            'callback' => [$this, 'get_settings'],
            'permission_callback' => [$this, 'get_settings_permission']
        ]);
        register_rest_route('wprk/v1', '/settings', [
            'methods' => 'POST',
            'callback' => [$this, 'save_settings'],
            'permission_callback' => '__return_true'
        ]);
        register_rest_route('wprk/pikkop/v1', '/handling_fees', [
            'methods' => 'GET',
            'callback' => [$this, 'get_handling_fees'],
            'permission_callback' => [$this, 'get_settings_permission']
        ]);
        register_rest_route('wprk/pikkop/v1', '/data_aditional', [
            'methods' => 'GET',
            'callback' => [$this, 'get_data_aditonial'],
            'permission_callback' => [$this, 'get_settings_permission']
        ]);
        register_rest_route('wprk/pikkop/v1', '/discounts_volume', [
            'methods' => 'GET',
            'callback' => [$this, 'get_discounts_volume'],
            'permission_callback' => [$this, 'get_settings_permission']
        ]);
        register_rest_route('wprk/pikkop/v1', '/packing_shipping_weight', [
            'methods' => 'GET',
            'callback' => [$this, 'get_packing_shipping_weight'],
            'permission_callback' => [$this, 'get_settings_permission']
        ]);
        register_rest_route('wprk/pikkop/v1', '/SKU_handling_charge', [
            'methods' => 'GET',
            'callback' => [$this, 'get_sku_handling_charge'],
            'permission_callback' => [$this, 'get_settings_permission']
        ]);
        register_rest_route('wprk/pikkop/v1', '/get_quotation' . $this->createParamenterLinkPdf(), [
            'methods' => 'GET',
            'callback' => [$this, 'get_quotation'],
            'permission_callback' => [$this, 'get_settings_permission']
        ]);
        register_rest_route('wprk/pikkop/v1', '/price_ec_ff', [
            'methods' => 'GET',
            'callback' => [$this, 'get_price_ec_ff'],
            'permission_callback' => [$this, 'get_settings_permission']
        ]);
        register_rest_route('wprk/pikkop/v1', '/suscripcion', [
            'methods' => 'GET',
            'callback' => [$this, 'get_price_suscripcion'],
            'permission_callback' => [$this, 'get_settings_permission']
        ]);
        register_rest_route('wprk/v1', '/register_contact', [
            'methods' => 'POST',
            'callback' => [$this, 'set_contact_hubspot'],
            'permission_callback' => '__return_true'
        ]);
        register_rest_route('wprk/pikkop/v1', '/send_rate', [
            'methods' => 'POST',
            'callback' => [$this, 'send_email_rate'],
            'permission_callback' => '__return_true'
        ]);

        register_rest_route('wprk/pikkop/v1', '/data_success_stories', [
            'methods' => 'GET',
            'callback' => [$this, 'get_data_success_stories'],
            'permission_callback' => [$this, 'get_settings_permission']
        ]);
    }

    /**
     * Obtiene los precios para la categoria de ecommerce
     */
    public function get_price_ec_ff()
    {
        global $wpdb;
        $tabla_form = $wpdb->prefix . 'price';
        $datos  = $wpdb->get_results("SELECT * FROM $tabla_form WHERE typed ='EC_FF'");
        return rest_ensure_response($datos);
    }

    public function get_data_aditonial()
    {
        global $wpdb;
        $tabla_form = $wpdb->prefix . 'data_aditional';
        $datos  = $wpdb->get_results("SELECT url_video,p_almacenaje, p_maniobra, p_envios  FROM $tabla_form");
        return rest_ensure_response($datos);
    }

    /**
     * Obtiene los precios para la categoria de suscripciones
     */
    public function get_price_suscripcion()
    {
        global $wpdb;
        $tabla_form = $wpdb->prefix . 'price';
        $datos  = $wpdb->get_results("SELECT * FROM $tabla_form WHERE typed ='suscripcion'");
        return rest_ensure_response($datos);
    }

    /**
     * Crea un usuario en el portal de hubspot
     */
    public function set_contact_hubspot($req)
    {
        $arr = array(
            'properties' => array(
                array(
                    'property' => 'email',
                    'value' => sanitize_text_field($req['email'])
                ),
                array(
                    'property' => 'firstname',
                    'value' => sanitize_text_field($req['firstname'])
                ),
                array(
                    'property' => 'lastname',
                    'value' => sanitize_text_field($req['lastname'])
                ),
                array(
                    'property' => 'company',
                    'value' => sanitize_text_field($req['company'])
                )
            )
        );;
        $post_json = json_encode($arr);
        $endpoint = 'https://api.hubapi.com/contacts/v1/contact?hapikey=' . $this->get_apikey();
        $ch = @curl_init();
        @curl_setopt($ch, CURLOPT_POST, true);
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
        @curl_setopt($ch, CURLOPT_URL, $endpoint);
        @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //$this->send_email_suscription($req);
        $response = @curl_exec($ch);
        $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_errors = curl_error($ch);
        @curl_close($ch);
        if ($status_code == 200) {
            $this->send_email_suscription($req);
        }
        return rest_ensure_response($status_code);
    }

    /**Funcion para envio de correo de nueva suscripcion */
    public function send_email_suscription($req)
    {
        // Llamamos a plantilla HTML y los estilos para la misma
        // ambos se encuentran en la carpeta llamada plantilla
        $mensaje  = (plugin_dir_path(__FILE__) . 'plantillaSuscriptor/mensaje.html');
        $estilos = (plugin_dir_path(__FILE__) . 'plantillaSuscriptor/estilos.css');

        // Importaciones de imagenes
        $pikkop_logo = (plugin_dir_path(__FILE__) . 'plantilla/assets/pikkop_logo.png');

        // Iniciamos con la consulta a la BD para obtener los datos necesarios
        global $wpdb;
        $tabla_form_mail = $wpdb->prefix . 'send_mail';
        $datos_mail = $wpdb->get_results("SELECT * FROM $tabla_form_mail");

        foreach ($datos_mail as $dato_mail) {
            $mail_admin = $dato_mail->mail_admin;
            $mail_sender = $dato_mail->mail_sender;
            $name_sender = $dato_mail->name_sender;
            $host = $dato_mail->host;
            $port = $dato_mail->port;
            $smtp_secure = $dato_mail->smtp_secure;
            $password_sender = $dato_mail->password_sender;
        }
        // Iniciamos con el envio del email

        // Obtenemos los datos del formulario que son (destinatario, asunto y mensaje)
        $emailTo = $mail_admin;
        $Subject = "Nuevo Suscriptor";
        $bodyEmail_email = $req['email'];
        $bodyEmail_first = $req['firstname'];
        $bodyEmail_last = $req['lastname'];
        $bodyEmail_company = $req['company'];

        // Asignamos los datos de la BD a las variables necesarias para el envio
        $fromemail = $mail_sender;
        $fromname = $name_sender;
        $host = $host;
        $port = $port;
        $SMTPAuth = true;
        $_SMTPSecure = $smtp_secure;
        $password = $password_sender;

        // Instanciamos a PHPMailer
        $mail = new PHPMailer\PHPMailer\PHPMailer();

        // Le decimos a  PHPMailer que utilize SMTP
        $mail->isSMTP();

        // Para saber si es para produccion o debug (colocamos 1 para debbug)
        $mail->SMTPDebug = 0;

        // Desde donde enviaremos el correo electronico
        $mail->Host = $host;
        $mail->Port = $port;
        $mail->SMTPAuth = $SMTPAuth;
        $mail->SMTPSecure = $_SMTPSecure;
        $mail->Username = $fromemail;
        $mail->Password = $password;

        // Asignamos el remitente y el nombre del remitente
        $mail->setFrom($fromemail, $fromname);

        // Asignamos el email a el destinatario
        $mail->addAddress($emailTo);

        // Asunto
        $mail->isHTML(true);
        $mail->Subject = $Subject;

        // Carga de imagenes
        $mail->AddEmbeddedImage($pikkop_logo, 'pikkop_logo', 'pikkop_logo.png');

        // Mensaje inicio -------------------------------------------------------
        // cargar archivo css para cuerpo de mensaje
        $rcss = $estilos; //ruta de archivo css
        $fcss = fopen($rcss, "r"); //abrir archivo css
        $scss = fread($fcss, filesize($rcss)); //leer contenido de css
        fclose($fcss); //cerrar archivo css

        // Cargar archivo html   
        $shtml = file_get_contents($mensaje);

        // reemplazar sección de plantilla html con el css cargado y mensaje creado
        $incss  = str_replace('<style id="estilo"></style>', "<style>$scss</style>", $shtml);
        $cuerpo = str_replace('<span id="email"></span>', $bodyEmail_email, $incss);
        $cuerpo = str_replace('<span id="first"></span>', $bodyEmail_first, $cuerpo);
        $cuerpo = str_replace('<span id="last"></span>', $bodyEmail_last, $cuerpo);
        $cuerpo = str_replace('<span id="company"></span>', $bodyEmail_company, $cuerpo);

        // Envio del mensaje
        $mail->Body = $cuerpo;
        // Mensaje final -------------------------------------------------------

        if (!$mail->send()) {
            die();
        }
    }

    /**
     * Envia una cotización, con los datos obtenidos de la app de cotización
     */
    public function send_email_rate($req)
    {
        // Asunto del correo
        $asunto = utf8_decode('Cotización de servicios Pikkop Air');

        $linkPdf = $this->getLinkPdf($req);

        // Importacion de plantilla y estilos de plantilla
        $mensaje  = (plugin_dir_path(__FILE__) . 'plantilla/mensaje.html');
        $fulfillment = (plugin_dir_path(__FILE__) . 'plantilla/fulfillment.html');
        $storage = (plugin_dir_path(__FILE__) . 'plantilla/storage.html');
        $estilos = (plugin_dir_path(__FILE__) . 'plantilla/estilos.css');
        $pikkop_logo = (plugin_dir_path(__FILE__) . 'plantilla/assets/pikkop_logo.png');
        $pikkop_cubo = (plugin_dir_path(__FILE__) . 'plantilla/assets/pikkop_cubo.png');
        $pikkop_facebook = (plugin_dir_path(__FILE__) . 'plantilla/assets/pikkop-facebook.png');
        $pikkop_instagram = (plugin_dir_path(__FILE__) . 'plantilla/assets/pikkop-instagram.png');
        $pikkop_linkedin = (plugin_dir_path(__FILE__) . 'plantilla/assets/pikkop-linkedin.png');
        $pikkop_tiktok = (plugin_dir_path(__FILE__) . 'plantilla/assets/pikkop-tiktok.png');
        $pikkop_twitter = (plugin_dir_path(__FILE__) . 'plantilla/assets/pikkop-twitter.png');
        $pikkop_youtube = (plugin_dir_path(__FILE__) . 'plantilla/assets/pikkop-youtube.png');

        // Asignacion de valores a las variables necesarias
        // Destinatario
        $emailTo = $req['email'];
        // Asunto
        $Subject = $asunto;
        // Contenidos del mensaje
        $bodyEmail_altura = $req['dataStorage']['altura'];
        $bodyEmail_ancho = $req['dataStorage']['ancho'];
        $bodyEmail_largo = $req['dataStorage']['largo'];
        $bodyEmail_uniPerLot = $req['dataStorage']['unitPerLot'];
        $bodyEmail_peso = $req['dataStorage']['peso'];
        $bodyEmail_productSend = $req['step2PedidosDia'];
        $bodyEmail_rateType = $req['dataStorage']['rateType'];
        $bodyEmail_totalCaseInventory = $req['dataStorage']['totalCaseInventory'];
        $bodyEmail_totalInventory = $req['dataStorage']['totalInventory'];
        $bodyEmail_totalStock = $req['dataStorage']['totalStock'];
        $bodyEmail_typeProductSales = $req['dataStorage']['typeProductSales'];


        // Consulta a la BD para la obtencion de configuracion de envio
        global $wpdb;
        $tabla_form_mail = $wpdb->prefix . 'send_mail';
        $datos_mail = $wpdb->get_results("SELECT * FROM $tabla_form_mail");
        // Recorrido de BD y asignacion de valores a variables
        foreach ($datos_mail as $dato_mail) {
            $mail_sender = $dato_mail->mail_sender;
            $name_sender = $dato_mail->name_sender;
            $host = $dato_mail->host;
            $port = $dato_mail->port;
            $smtp_secure = $dato_mail->smtp_secure;
            $password_sender = $dato_mail->password_sender;
        }
        // Asignacion de valores obtenidos de la BD a variables locales
        $fromemail = $mail_sender;
        $fromname = $name_sender;
        $host = $host;
        $port = $port;
        $SMTPAuth = true;
        $_SMTPSecure = $smtp_secure;
        $password = $password_sender;

        // Tabla de urls ------
        $tabla_form_url = $wpdb->prefix . 'url_redirect';
        $datos_url = $wpdb->get_results("SELECT * FROM $tabla_form_url");
        // Recorrido de BD y asignacion de valores a variables
        foreach ($datos_url as $dato_url) {
            $url_id = $dato_url->id;
            $url_contactar_asesor = $dato_url->contactar_asesor;
            $url_agendar_llamada = $dato_url->agendar_llamada;
            $url_imprimir_cotizacion = $dato_url->imprimir_cotizacion;
            $url_volver_cotizar = $dato_url->volver_cotizar;
            $url_linkedin = $dato_url->url_linkedin;
            $url_twitter = $dato_url->url_twitter;
            $url_facebook = $dato_url->url_facebook;
            $url_instagram = $dato_url->url_instagram;
            $url_youtube = $dato_url->url_youtube;
            $url_tiktok = $dato_url->url_tiktok;
        }

        // Instanciamos a PHPMailer
        $mail = new PHPMailer\PHPMailer\PHPMailer();

        // Le decimos a  PHPMailer que utilize SMTP
        $mail->isSMTP();

        // Para saber si es para produccion o debug (colocamos 1 para debbug)
        $mail->SMTPDebug = 0;

        // Parametros para el envio del correo electronico
        $mail->Host = $host;
        $mail->Port = $port;
        $mail->SMTPAuth = $SMTPAuth;
        $mail->SMTPSecure = $_SMTPSecure;
        $mail->Username = $fromemail;
        $mail->Password = $password;

        // Asignamos el remitente y el nombre del remitente
        $mail->setFrom($fromemail, $fromname);

        // Asignamos el email a el destinatario
        $mail->addAddress($emailTo);

        // Asunto
        $mail->isHTML(true);
        $mail->Subject = $Subject;

        // Imagen -----------------------------------------------------------------
        $mail->AddEmbeddedImage($pikkop_logo, 'pikkop_logo', 'pikkop_logo.png');
        $mail->AddEmbeddedImage($pikkop_cubo, 'pikkop_cubo', 'pikkop_cubo.png');
        $mail->AddEmbeddedImage($pikkop_facebook, 'pikkop_facebook', 'pikkop_facebook.png');
        $mail->AddEmbeddedImage($pikkop_instagram, 'pikkop_instagram', 'pikkop_instagram.png');
        $mail->AddEmbeddedImage($pikkop_linkedin, 'pikkop_linkedin', 'pikkop_linkedin.png');
        $mail->AddEmbeddedImage($pikkop_tiktok, 'pikkop_tiktok', 'pikkop_tiktok.png');
        $mail->AddEmbeddedImage($pikkop_twitter, 'pikkop_twitter', 'pikkop_twitter.png');
        $mail->AddEmbeddedImage($pikkop_youtube, 'pikkop_youtube', 'pikkop_youtube.png');
        //$mail->AddEmbeddedImage($sImagen, 'imagen'); //ruta de archivo de image

        // Mensaje -------------------------------------------------------
        // cargar archivo css para cuerpo de mensaje
        $rcss = $estilos; //ruta de archivo css
        $fcss = fopen($rcss, "r"); //abrir archivo css
        $scss = fread($fcss, filesize($rcss)); //leer contenido de css
        fclose($fcss); //cerrar archivo css

        // Cargar archivo html
        $shtml = file_get_contents($mensaje);
        $fulfillment = file_get_contents($fulfillment);
        $storage = file_get_contents($storage);

        // reemplazar sección de plantilla html con el css cargado y mensaje creado
        $incss  = str_replace('<style id="estilo"></style>', "<style>$scss</style>", $shtml);

        $cuerpo = str_replace('[section_fulfillment]', $fulfillment, $incss);
        $cuerpo = str_replace('[section_storage]', $req['changeInStorage'] ? $storage : '', $cuerpo);
        $cuerpo = str_replace('<span id="uniPerLot"></span>', $bodyEmail_uniPerLot, $cuerpo);

        $cuerpo = str_replace('<span id="altura"></span>', $bodyEmail_altura, $cuerpo);
        $cuerpo = str_replace('<span id="ancho"></span>', $bodyEmail_ancho, $cuerpo);
        $cuerpo = str_replace('<span id="largo"></span>', $bodyEmail_largo, $cuerpo);
        $cuerpo = str_replace('<span id="peso"></span>', $bodyEmail_peso, $cuerpo);
        $cuerpo = str_replace('<span id="productSend"></span>', $bodyEmail_productSend, $cuerpo);
        //$cuerpo = str_replace('<span id="rateType"></span>', $bodyEmail_rateType, $cuerpo);
        $cuerpo = str_replace('<span id="totalCaseInventory"></span>', $bodyEmail_totalCaseInventory, $cuerpo);
        $cuerpo = str_replace('<span id="totalInventory"></span>', $bodyEmail_totalInventory, $cuerpo);
        $cuerpo = str_replace('<span id="totalStock"></span>', $bodyEmail_totalStock, $cuerpo);
        $cuerpo = str_replace('<span id="typeProductSales"></span>', $bodyEmail_typeProductSales, $cuerpo);
        $cuerpo = str_replace('[storage_priceWeek]', number_format($req['storage_priceWeek'], 2, '.', ' '), $cuerpo);
        $cuerpo = str_replace('[storage_priceMonth]', number_format($req['storage_priceMonth'], 2, '.', ' '), $cuerpo);
        $cuerpo = str_replace('[storage_pricePieceOrLotWeek]', number_format($req['storage_pricePieceOrLotWeek'], 2, '.', ' '), $cuerpo);
        $cuerpo = str_replace('[storage_pricePieceOrLotMonth]', number_format($req['storage_pricePieceOrLotMonth'], 2, '.', ' '), $cuerpo);
        // urls
        $cuerpo = str_replace('[contactar_asesor]', $url_contactar_asesor, $cuerpo);
        $cuerpo = str_replace('[agendar_llamada]', $url_agendar_llamada, $cuerpo);
        $cuerpo = str_replace('[imprimir_cotizacion]', $linkPdf, $cuerpo);
        $cuerpo = str_replace('[volver_cotizar]', $url_volver_cotizar, $cuerpo);
        $cuerpo = str_replace('[linkedin]', $url_linkedin, $cuerpo);
        $cuerpo = str_replace('[twitter]', $url_twitter, $cuerpo);
        $cuerpo = str_replace('[facebook]', $url_facebook, $cuerpo);
        $cuerpo = str_replace('[instagram]', $url_instagram, $cuerpo);
        $cuerpo = str_replace('[youtube]', $url_youtube, $cuerpo);
        $cuerpo = str_replace('[tiktok]', $url_tiktok, $cuerpo);


        $FF_OM = $req['dataFulFillment']['FF_orderMonth'];
        //Fulfillment
        $cuerpo = str_replace('[FF_orderMonth]', $FF_OM, $cuerpo);
        $cuerpo = str_replace('[FF_avWeightOrder]', $req['dataFulFillment']['FF_avWeightOrder'], $cuerpo);
        $cuerpo = str_replace('[FF_SKUOrder]', $req['dataFulFillment']['FF_SKUOrder'], $cuerpo);


        $cuerpo = str_replace('[step1Sku]', $req['step1Sku'], $cuerpo);
        $cuerpo = str_replace('[step1PromArticulos]', $this->formatPrice($FF_OM, $req['step1PromArticulos']), $cuerpo);
        $cuerpo = str_replace('[step1ArticulosInventario]', $req['step1ArticulosInventario'], $cuerpo);
        $cuerpo = str_replace('[step1ValPromArticulos]', $req['step1ValPromArticulos'], $cuerpo);
        $cuerpo = str_replace('[step2ArticulosPedido]', $req['step2ArticulosPedido'], $cuerpo);
        $cuerpo = str_replace('[step2PedidosDia]', $req['step2PedidosDia'], $cuerpo);

        $cuerpo = str_replace('[recepcionPieza]', $this->formatPrice($FF_OM, $req['recepcionPieza']), $cuerpo);
        $cuerpo = str_replace('[almacenajeSemana]', $this->formatPrice($FF_OM, $req['almacenajeSemana']), $cuerpo);

        $cuerpo = str_replace('[surtidoPedido]', $this->formatPrice($FF_OM, $req['surtidoPedido']), $cuerpo);
        $cuerpo = str_replace('[piezaAdicional]', $this->formatPrice($FF_OM, $req['piezaAdicional']), $cuerpo);
        $cuerpo = str_replace('[embalaje]', $this->formatPrice($FF_OM, $req['embalaje']), $cuerpo);
        $cuerpo = str_replace('[envioMismoDia]', $this->formatPrice($FF_OM, $req['precioEnvio']['envioMismoDia']), $cuerpo);
        $cuerpo = str_replace('[envioEstandar]', $this->formatPrice($FF_OM, $req['precioEnvio']['envioEstandar']), $cuerpo);
        $cuerpo = str_replace('[envioDiaSiguiente]', $this->formatPrice($FF_OM, $req['precioEnvio']['envioDiaSiguiente']), $cuerpo);

        $cuerpo = str_replace('[typeProductSales]', $bodyEmail_typeProductSales, $cuerpo);
        $cuerpo = str_replace('[d_next_day]', $this->formatPrice($FF_OM, $req['ff_price']['d_next_day']), $cuerpo);
        $cuerpo = str_replace('[d_ground]', $this->formatPrice($FF_OM, $req['ff_price']['d_ground']), $cuerpo);
        $cuerpo = str_replace('[d_next_day]', $this->formatPrice($FF_OM, $req['ff_price']['d_next_day']), $cuerpo);

        // Envio del mensaje
        $mail->Body = utf8_decode($cuerpo);
        //$mail->Body = $cuerpo;

        if (!$mail->send()) {
            die();
        }

        return rest_ensure_response($linkPdf);
    }

    /**
     * Crea un fragmento de la api para poder descargar en formato pdf de la cotización
     */
    public function createParamenterLinkPdf()
    {

        $paramers = '/(?P<step1Sku>[\w]+)';
        $paramers .= '/(?P<step1PromArticulos>[\w]+)';
        $paramers .= '/(?P<step1ArticulosInventario>[\w]+)';
        $paramers .= '/(?P<step1ValPromArticulos>[\w]+)';
        $paramers .= '/(?P<step2ArticulosPedido>[\w]+)';
        $paramers .= '/(?P<step2PedidosDia>[\w]+)';
        $paramers .= '/(?P<recepcionPieza>[\w]+)';
        $paramers .= '/(?P<almacenajeSemana>[\w]+)';
        $paramers .= '/(?P<surtidoPedido>[\w]+)';
        $paramers .= '/(?P<piezaAdicional>[\w]+)';
        $paramers .= '/(?P<embalaje>[\w]+)';
        $paramers .= '/(?P<envioMismoDia>[\w]+)';

        $paramers .= '/(?P<envioEstandar>[\w]+)';
        $paramers .= '/(?P<envioDiaSiguiente>[\w]+)';
        $paramers .= '/(?P<largo>[\w]+)';
        $paramers .= '/(?P<peso>[\w]+)';
        $paramers .= '/(?P<productSend>[\w]+)';
        $paramers .= '/(?P<rateType>[\w]+)';
        $paramers .= '/(?P<totalCaseInventory>[\w]+)';
        $paramers .= '/(?P<totalInventory>[\w]+)';
        $paramers .= '/(?P<totalStock>[\w]+)';
        // $paramers .= '/(?P<typeProductSales>[\w]+)';
        $paramers .= '/(?P<unitPerLot>[\w]+)';
        $paramers .= '/(?P<storage_priceMonth>[\w]+)';
        $paramers .= '/(?P<storage_pricePieceOrLotMonth>[\w]+)';
        $paramers .= '/(?P<storage_pricePieceOrLotWeek>[\w]+)';
        $paramers .= '/(?P<storage_priceWeek>[\w]+)';

        $paramers .= '/(?P<changeInStorage>[\w]+)';
        $paramers .= '/(?P<changeInFF>[\w]+)';

        return $paramers;
    }

    /**
     * Crea un link para que se pueda descargar la cotización en formato pdf
     */
    public function getLinkPdf($data)
    {
        // $type = $data['dataStorage']['typeProductSales'] == '' ? 'Sin descripción' : $data['dataStorage']['typeProductSales'];
        $link = $data['apiBase'] . '/wprk/pikkop/v1/get_quotation/';
        $link .= str_replace('.', '_', $data['step1Sku']) . '/';
        $link .= str_replace('.', '_', $data['step1PromArticulos']) . '/';
        $link .= str_replace('.', '_', $data['step1ArticulosInventario']) . '/';
        $link .= str_replace('.', '_', $data['step1ValPromArticulos']) . '/';
        $link .= str_replace('.', '_', $data['step2ArticulosPedido']) . '/';
        $link .= str_replace('.', '_', $data['step2PedidosDia']) . '/';
        $link .= str_replace('.', '_', $data['recepcionPieza']) . '/';
        $link .= str_replace('.', '_', $data['almacenajeSemana']) . '/';
        $link .= str_replace('.', '_', $data['surtidoPedido']) . '/';
        $link .= str_replace('.', '_', $data['piezaAdicional']) . '/';
        $link .= str_replace('.', '_', $data['embalaje']) . '/';
        $link .= str_replace('.', '_', $data['precioEnvio']['envioMismoDia']) . '/';
        $link .= str_replace('.', '_', $data['precioEnvio']['envioEstandar']) . '/';
        $link .= str_replace('.', '_', $data['precioEnvio']['envioDiaSiguiente']) . '/';
        $link .= str_replace('.', '_', $data['dataStorage']['largo']) . '/';
        $link .= str_replace('.', '_', $data['dataStorage']['peso']) . '/';
        $link .= str_replace('.', '_', $data['dataStorage']['productSend']) . '/';
        $link .= str_replace('.', '_', $data['dataStorage']['rateType']) . '/';
        $link .= str_replace('.', '_', $data['dataStorage']['totalCaseInventory']) . '/';
        $link .= str_replace('.', '_', $data['dataStorage']['totalInventory']) . '/';
        $link .= str_replace('.', '_', $data['dataStorage']['totalStock']) . '/';
        // $link .= str_replace('.', '_', $type) . '/';
        $link .= str_replace('.', '_', $data['dataStorage']['unitPerLot']) . '/';
        $link .= str_replace('.', '_', $data['storage_priceMonth']) . '/';
        $link .= str_replace('.', '_', $data['storage_pricePieceOrLotMonth']) . '/';
        $link .= str_replace('.', '_', $data['storage_pricePieceOrLotWeek']) . '/';
        $link .= str_replace('.', '_', $data['storage_priceWeek']) . '/';

        $changeInStorage = $data['changeInStorage'] ? 1 : 0;
        $link .= str_replace('.', '_', $changeInStorage) . '/';
        $changeInFF = $data['changeInFF'] ? 1 : 0;
        $link .= str_replace('.', '_', $changeInFF);


        return $link;
    }

    /**
     * Crea el archivo pdf con la informacion de la cotización que recibio el usuario en su correo
     */
    public function get_quotation($req)
    {

        try {
            // Requerimos a vendor
            require_once __DIR__ . '/Mpdf/vendor/autoload.php';
            // Plantilla HTML
            require_once __DIR__ . '/plantilla/plantillaPDF/pdf.php';
            // CSS de la plantilla
            $css = file_get_contents(__DIR__ . '/plantilla/plantillaPDF/styles.css');
            // Instanciamos a la la libreria Mpdf
            $mpdf = new \Mpdf\Mpdf([]);
            // Obtenemos la plantilla de plantillaPDF/pdf.php llamando a la funcion getPlantilla()
            $plantilla = getPlantilla($req);
            // Inyectamos el CSS y el HTML al contenido del PDF
            $mpdf->WriteHTML($css, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($plantilla, \Mpdf\HTMLParserMode::HTML_BODY);
            // Le otorgamos un nombre al documento y una salida
            $mpdf->Output('Cotizacion_pikkopAir.pdf', \Mpdf\Output\Destination::INLINE);
            // Retornamos el PDF
            return rest_ensure_response($mpdf);
        } catch (Exception $e) {
            // En caso de error, retornamos el mismo
            return rest_ensure_response($e->getMessage());
        }
    }

    public function formatPrice($FF_orderMonth, $price)
    {
        if ($FF_orderMonth > 10000) {
            return 'Contactanos';
        } else {
            return number_format($price, 2, '.', ' ');
        }
    }

    public function get_apikey()
    {
        global $wpdb;
        $tabla_form = $wpdb->prefix . 'data_aditional';
        $datos  = $wpdb->get_results("SELECT * FROM $tabla_form");
        return $datos[0]->datta;
    }

    public function get_handling_fees()
    {
        global $wpdb;
        $tabla_form = $wpdb->prefix . 'handling_fees';
        $datos = $wpdb->get_results("SELECT * FROM $tabla_form");
        return rest_ensure_response($datos);
    }

    public function get_discounts_volume()
    {
        global $wpdb;
        $tabla_form = $wpdb->prefix . 'discounts_volume';
        $datos = $wpdb->get_results("SELECT * FROM $tabla_form");
        return rest_ensure_response($datos);
    }

    public function get_packing_shipping_weight()
    {
        global $wpdb;
        $tabla_form = $wpdb->prefix . 'packing_shipping_weight';
        $datos = $wpdb->get_results("SELECT * FROM $tabla_form");
        return rest_ensure_response($datos);
    }

    public function get_data_success_stories()
    {
        global $wpdb;
        $tabla_form = $wpdb->prefix . 'data_success_stories';
        $datos = $wpdb->get_results("SELECT * FROM $tabla_form");
        return rest_ensure_response($datos);
    }

    public function get_sku_handling_charge()
    {
        global $wpdb;
        $tabla_form = $wpdb->prefix . 'SKU_handling_charge';
        $datos = $wpdb->get_results("SELECT * FROM $tabla_form");
        return rest_ensure_response($datos);
    }

    public function get_settings()
    {
        $firstname = get_option('wprk_settings_firstname');
        $lastname  = get_option('wprk_settings_lastname');
        $email     = get_option('wprk_settings_email');
        $response = [
            'firstname' => $firstname,
            'lastname'  => $lastname,
            'email'     => $email
        ];

        return rest_ensure_response($response);
    }

    public function get_settings_permission()
    {
        return true;
    }

    public function save_settings($req)
    {
        $firstname = sanitize_text_field($req['firstname']);
        $lastname  = sanitize_text_field($req['lastname']);
        $email     = sanitize_text_field($req['email']);
        update_option('wprk_settings_firstname', $firstname);
        update_option('wprk_settings_lastname', $lastname);
        update_option('wprk_settings_email', $email);
        return rest_ensure_response('success');
    }

    public function save_settings_permission()
    {
        return current_user_can('publish_posts');
    }
}
new WP_React_Settings_Rest_Route();
