import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Col, Form, Row, Alert } from 'react-bootstrap';
import './Blue-Form.css'

const BlueForm = () => {
    const [action, setAction] = useState('Me interesa');
    const [email, setemail] = useState('');
    const [firstname, setfirstname] = useState('');
    const [lastname, setlastname] = useState('');
    const [company, setcompany] = useState('');
    const [show, setShowAlert] = useState(false);
    const [properties, setProperties] = useState({
        type: "success",
        title: "Registro exitoso",
        message: "Se creo tu contacto, en breve se pondrán en contacto contigo."
    });

    const apiBase = appLocalizer.apiUrl.replace('wp-json', '?rest_route=');
    const url = `${apiBase}/wprk/v1/register_contact`;


    /**
     * Crea un nuevo contacto, que se envia a back para el proceso
     */
    const registerContact = async () => {
        setAction("Creando contacto");
        let data = {
            email,
            firstname,
            lastname,
            company,
        }

        try {
            let response = await axios.post(url, data, {
                headers: {
                    'content-type': 'application/json',
                    'X-WP-NONCE': appLocalizer.nonce
                }
            });

            setAction("Me interesa");
            setShowAlert(true);

            switch (response.data) {
                case 200:
                    setProperties({
                        type: "success",
                        title: "Registro exitoso",
                        message: "Se creo tu contacto, en breve se pondrán en contacto contigo."
                    });
                    clearInputs();
                    break;
                case 409:
                    setProperties({
                        type: "danger",
                        title: "Error",
                        message: "El correo electrónico ya se encuentra registrado."
                    });
                    break;
                case 400:
                    setProperties({
                        type: "warning",
                        title: "Error",
                        message: "Todos los campos son obligatorios."
                    });
                case 401:
                    setProperties({
                        type: "danger",
                        title: "Error",
                        message: "Algo salio mal, no se pudieron registrar tus datos."
                    });
                    break;
                case 403:
                    setProperties({
                        type: "danger",
                        title: "Error",
                        message: "Algo salio mal, no se pudieron registrar tus datos."
                    });
                    break;
                default:
                    setProperties({
                        type: "danger",
                        title: "Error",
                        message: "Algo salio mal, no se pudieron registrar tus datos."
                    });
                    break;

            }
        } catch (error) {
            setShowAlert(true);
            setAction("Me interesa");
            setProperties({
                type: "danger",
                title: "Error",
                message: "Algo salio mal, no se pudieron registrar tus datos."
            });
        }

        setTimeout(() => {
            setShowAlert(false);
        }, 3000);

    }

    const clearInputs = () => {
        setemail('');
        setfirstname('');
        setlastname('');
        setcompany('');
    }

    return (
        <React.Fragment>
            {
                show ? <AlertAction properties={properties} /> : null
            }
            <div className="d-flex justify-content-center">
                <div className="main-form">
                    <p className="main-form__title m-0">¿Estás listo para empezar? Hablemos.</p>
                    <Row className="pr-5 pl-5 pb-3 pt-0">
                        <Col xs={12} xl={6} md={6} >
                            <Form.Control
                                size="sm"
                                type="text"
                                placeholder="Nombre"
                                value={firstname}
                                onChange={(e) => setfirstname(e.target.value)}
                                className="m-2 input-line sub-title-section sub-title-section-value"
                            />
                        </Col>
                        <Col xs={12} xl={6} md={6} >
                            <Form.Control
                                size="sm"
                                type="text"
                                placeholder="Apellido"
                                value={lastname}
                                onChange={(e) => setlastname(e.target.value)}
                                className="m-2 input-line sub-title-section sub-title-section-value"
                            />
                        </Col>
                        <Col xs={12} xl={6} md={6} >
                            <Form.Control
                                size="sm"
                                type="text"
                                placeholder="Empresa"
                                value={company}
                                onChange={(e) => setcompany(e.target.value)}
                                className="m-2 input-line sub-title-section sub-title-section-value"
                            />
                        </Col>
                        <Col xs={12} xl={6} md={6} >
                            <Form.Control
                                size="sm"
                                type="text"
                                placeholder="Correo electrónico"
                                value={email}
                                onChange={(e) => setemail(e.target.value)}
                                className="m-2 input-line sub-title-section sub-title-section-value"
                            />
                        </Col>
                    </Row>
                    <div className="main-form__button-content">
                        <button className="button" onClick={registerContact}>{action}</button>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

const AlertAction = (props) => {
    const { properties } = props;
    return (
        <React.Fragment>
            <section className="pl-5 pr-4">
                <Alert variant={properties.type}>
                    {properties.message}
                </Alert>
            </section>
        </React.Fragment>
    )
}

export default BlueForm;

