import React, { useState, useEffect } from 'react';
import { Col, Row, Button, Form, Spinner } from 'react-bootstrap';
import ShippingRates from './OutputInformation/ShippingRates';
import Fulfillment from './Inputs/Fulfillment'
import Almacenaje from './Inputs/Almacenaje'
import CostoEnvio from './Inputs/CostoEnvio'
import StorageFees from './OutputInformation/StorageFees';
import TarifasEnvio from './OutputInformation/TarifasEnvio';
import axios from 'axios';
import Header from '../components/header/Header';
import CarouselComments from '../components/carouselComments/CarouselComments';
import BlueForm from './blueForm/BlueForm';
import GrayForm from './grayForm/GrayForm';
import './style_react.css'
import FixedRates from './OutputInformation/FixedRates';
import FixedReceptionRates from './OutputInformation/FixedReceptionRates';

const type = {
    EC: 1,
    SCP: 2,
    DST: 3
}

const apiBase = appLocalizer.apiUrl.replace('wp-json', '?rest_route=');
const urlSendMail = `${apiBase}/wprk/pikkop/v1/send_rate`;
const urlDiscountsVolume = `${apiBase}/wprk/pikkop/v1/discounts_volume`;
const urlPackingShippingWeight = `${apiBase}/wprk/pikkop/v1/packing_shipping_weight`;
const urlSkuHandlingCharge = `${apiBase}/wprk/pikkop/v1/SKU_handling_charge`;
const urlDataAditionsl = `${apiBase}/wprk/pikkop/v1/data_aditional`
const urlDataSuccessStories = `${apiBase}/wprk/pikkop/v1/data_success_stories`;

const tarifaKg = 3.14;
const tArticuloM = 2.70;

const Settings = () => {
    /**
     * price ecommerce and subscription
     */
    const [data_EC_FF, setData_EC_FF] = useState([]);
    const [data_suscription, setData_suscription] = useState([]);

    /**
     * state for fulfillment
     */
    const [FF_orderMonth, setFF_orderMonth] = useState(200);
    const [FF_avWeightOrder, setFF_avWeightOrder] = useState(0.25);
    const [FF_SKUOrder, setFF_SKUOrder] = useState(1);
    const [dataFulFillment, setDataFulFillment] = useState({});
    const [total, setTotal] = useState(0); // Total calculed fulfillmente
    const [hours, setHours] = useState(0);

    const [porcentajeSKU, setPorcentajeSKU] = useState([
        {
            min: 0,
            max: 0,
            percentaje: 0
        }
    ]);
    const [discountsVolume, setDiscountsVolume] = useState([{
        min: 0,
        max: 0,
        percentaje: 0
    }]);
    const [embalajeEnvio, setEmbalajeEnvio] = useState([{
        kgs: 0,
        embalaje: 0,
        envio_mismo_dia: 0,
        envio_estandar: 0,
        envio_dia_siguiente: 0
    }]);
    const [dataAditional, setDataAditional] = useState({
        "p_almacenaje": 0,
        "p_maniobra": 0,
        "p_envios": 0
    })


    const [step1Sku, setStep1Sku] = useState(1);
    const [step1PromArticulos, setStep1PromArticulos] = useState(0.25);
    const [step1ArticulosInventario, setStep1ArticulosInventario] = useState(1);
    const [step1ValPromArticulos, setStep1ValPromArticulos] = useState(1);

    const [step2ArticulosPedido, setStep2ArticulosPedido] = useState(1);
    const [step2PedidosDia, setStep2PedidosDia] = useState(1);
    const [resultadoSKU, setResultadoSKU] = useState(0);
    const [piezasDias, setPiezasDias] = useState(0);
    const [descuentoVolumen, setDescuentoVolumen] = useState(0);

    const [recepcionPieza, setRecepcionPieza] = useState(0);
    const [almacenajeSemana, setAlmacenajeSemana] = useState(0);
    const [surtidoPedido, setSurtidoPedido] = useState(0);
    const [piezaAdicional, setPiezaAdicional] = useState(0);
    const [embalaje, setEmbalaje] = useState(0);
    const [precioEnvio, setPrecioEnvio] = useState({
        envioMismoDia: 50,
        envioEstandar: 170,
        envioDiaSiguiente: 228
    });

    //Type of quote
    const [typeSelect, onSelectType] = useState(type.EC);

    //Tarifas BD
    const [handling_fees, setHandling_fees] = useState([]);
    const [price, setPrice] = useState({}); //Precio base
    const [priceHF, setPriceHF] = useState(0) //Tarifa seleccionada

    const [priceWeek, setPriceWeek] = useState(0);
    const [priceMonth, setPriceMonth] = useState(0);
    const [pricePieceOrLotWeek, setPricePieceOrLotWeek] = useState(0);
    const [pricePieceOrLotMonth, setPricePieceOrLotMonth] = useState(0);

    const [dataStorage, setDataStorage] = useState({});

    const [changeInFF, setChangeInFF] = useState(false);
    const [changeInStorage, setChangeInStorage] = useState(false);
    const [dataSuccessStories, setDataSuccessStories] = useState([]);


    useEffect(() => {
        getAllDataInfo();
    }, [])

    const getAllDataInfo = async () => {
        let discountsVolume = await axios.get(urlDiscountsVolume);
        setDiscountsVolume(discountsVolume.data);
        let skuHandlingCharge = await axios.get(urlSkuHandlingCharge);
        setPorcentajeSKU(skuHandlingCharge.data)
        let packingShippingWeight = await axios.get(urlPackingShippingWeight);
        setEmbalajeEnvio(packingShippingWeight.data);
        let dataAditional = await axios.get(urlDataAditionsl);
        setDataAditional(dataAditional.data[0])
        let dataSuccessStories = await axios.get(urlDataSuccessStories);
        setDataSuccessStories(dataSuccessStories.data);
    }

    useEffect(() => {
        let timeSaving = (FF_orderMonth * 15) / 60;
        setHours(timeSaving);
        getTotal();
    }, [FF_orderMonth]);

    useEffect(() => {
        getPorcentajeSKU();
    }, [step1Sku]);

    const getPorcentajeSKU = async () => {
        porcentajeSKU.map((obj, i) => {
            if (step1Sku >= parseFloat(obj.min) && step1Sku <= parseFloat(obj.max)) {
                setResultadoSKU(parseFloat(obj.percentaje));
            }
        })
    }

    const getEmbalajeEnvio = async () => {
        let peso = step1PromArticulos * step2ArticulosPedido;
        embalajeEnvio.map((obj, i) => {
            if (Number(peso) >= Number(obj.kgs) && Number(peso) <= Number(obj.kgs)) {
                setEmbalaje(parseFloat(obj.embalaje))
                // setPrecioEnvio({ envioMismoDia: parseFloat(obj.envio_mismo_dia), envioEstandar: parseFloat(obj.envio_estandar), envioDiaSiguiente: parseFloat(obj.envio_dia_siguiente) })
            }
        })
    }

    useEffect(() => {
        let opPiezasDias = step2ArticulosPedido * step2PedidosDia;
        setPiezasDias(opPiezasDias);
    }, [step2ArticulosPedido, step2PedidosDia]);

    useEffect(() => {
        getPorcentajeDescuentoVolumen();
    }, [piezasDias]);

    const getPorcentajeDescuentoVolumen = async () => {
        discountsVolume.map((obj, i) => {
            if (piezasDias >= parseFloat(obj.min) && piezasDias <= parseFloat(obj.max)) {
                setDescuentoVolumen(parseFloat(obj.percentaje));
            }
        })
    }

    useEffect(() => {
        let porcentajeResultadoSKU = resultadoSKU / 100;
        let porcentajeDescuentoVolumen = descuentoVolumen / 100;

        let opPiezaAdicional = (tArticuloM * (1.1 + porcentajeResultadoSKU)) * (1 - porcentajeDescuentoVolumen)
        let recepcion = (tArticuloM * (1 + porcentajeResultadoSKU)) * (1 - porcentajeDescuentoVolumen)
        let opSurtidoPedido = ((step2ArticulosPedido * tArticuloM) * (1.1 + porcentajeResultadoSKU)) * (1 - porcentajeDescuentoVolumen)
        let opAlmacenajeSemana = (step1PromArticulos * (tarifaKg * (1 + porcentajeResultadoSKU))) * (1 - porcentajeDescuentoVolumen)

        setAlmacenajeSemana(opAlmacenajeSemana)
        setSurtidoPedido(opSurtidoPedido)
        setRecepcionPieza(recepcion);
        setPiezaAdicional(opPiezaAdicional)
        getEmbalajeEnvio();
    }, [step1Sku, piezasDias, step1PromArticulos, step2ArticulosPedido, step2PedidosDia, step1ArticulosInventario]);

    const getPrice_orderMonnth = () => {
        let _price = null;
        switch (typeSelect) {
            case type.EC:
                _price = data_EC_FF.filter(OM => Number(FF_orderMonth) >= Number(OM.minn) && Number(FF_orderMonth) <= Number(OM.maxx))[0];
                break;
            case type.SCP:
                _price = data_suscription.filter(OM => Number(FF_orderMonth) >= Number(OM.minn) && Number(FF_orderMonth) <= Number(OM.maxx))[0];
                break;
            case type.DST:
                _price = data_EC_FF.filter(OM => Number(FF_orderMonth) >= Number(OM.minn) && Number(FF_orderMonth) <= Number(OM.maxx))[0];
                break;
        }

        if (_price !== undefined) {
            setPrice(_price);
            return Number(_price.picking_fees);
        }
        return 0;
    }

    const getPricePerHandlingFees = () => {
        let _priceHF = handling_fees.filter(PHF => Number(PHF.weight_per_order) === Number(FF_avWeightOrder))[0];
        if (_priceHF !== undefined) {
            setPriceHF(_priceHF);
            return _priceHF.weight_based_fees
        }
        return 0
    }

    const getTotal = () => {
        let Picking_Fees = getPrice_orderMonnth();
        let weight_based_fees = getPricePerHandlingFees();

        let _total = Number(Picking_Fees) + Number(weight_based_fees)

        let addFrom = typeSelect === type.SCP ? 5 : 1;

        if (Number(FF_SKUOrder) > addFrom) {
            let additional_price = price.additional_items;
            let total_additional = 0;

            if (typeSelect === type.SCP) {
                total_additional = getTotalAdditionalBySubscriotion(Number(additional_price), addFrom, Number(weight_based_fees));
            } else {
                total_additional = (Number(FF_SKUOrder) * Number(additional_price)) - Number(additional_price)
            }

            _total += total_additional;
        }

        setDataFulFillment({
            FF_orderMonth,
            FF_avWeightOrder,
            FF_SKUOrder,
        });
        setTotal(_total);
    }

    const getTotalAdditionalBySubscriotion = (additional_price, addFrom, weight_based_fees) => {
        let additional = additional_price;
        if (Number(FF_SKUOrder) > (addFrom + 1)) {
            let razon = weight_based_fees * (Number(FF_SKUOrder) - 5) - weight_based_fees;
            additional += ((additional_price * Number(FF_SKUOrder) - 10 + razon)) - additional_price;
        }
        return additional;
    }

    return (
        <React.Fragment>
            <div className="container main-pikkop">

                <Header dataAditional={dataAditional} />

                <div style={{ marginBottom: "30px", marginTop: "30px" }}>
                    <GrayForm />
                </div>

                <div className="d-flex justify-content-center">
                    <label className="title-lb-1 m-0" style={{ textAlign: 'center' }}>¡Revoluciona tu e-commerce!</label>
                </div>

                <div className="d-flex justify-content-center pb-2" style={{ marginBottom: "30px" }}>
                    <label className="title-lb-2 m-0" style={{ textAlign: 'center' }}>Cotiza en 3 pasos la logística a tu medida</label>
                </div>

                {/* Fulfillment */}
                <Row onClick={() => { setChangeInFF(true) }}>
                    <Col lg={6} xl={6} id="contenido-almacenaje" className="pt-2">
                        <section style={{ width: "100%" }} >
                            <Fulfillment
                                setStep1PromArticulos={setStep1PromArticulos}
                                setFF_SKUOrder={setFF_SKUOrder}
                                setStep1Sku={setStep1Sku}
                                setStep1ArticulosInventario={setStep1ArticulosInventario}
                                setStep1ValPromArticulos={setStep1ValPromArticulos}
                                FF_avWeightOrder={FF_avWeightOrder}
                            />
                        </section>
                    </Col>
                    <Col lg={6} xl={6} className="pt-2 d-flex align-items-center">
                        <Row>
                            <Col md={12}>
                                <ShippingRates
                                    recepcionPieza={recepcionPieza}
                                    almacenajeSemana={almacenajeSemana}
                                />
                            </Col>

                            <Col md={12}>
                                <section className="p-5"></section>
                            </Col>
                        </Row>
                    </Col>
                </Row>

                {/* Almacenaje y recepción */}
                <Row className="mt-4 section-storage" onClick={() => { setChangeInStorage(true) }}>
                    <Col lg={6} xl={6} id="contenido-maniobra" className="pt-2">
                        <section style={{ width: "100%" }}>
                            <Almacenaje
                                setStep2ArticulosPedido={setStep2ArticulosPedido}
                                setStep2PedidosDia={setStep2PedidosDia}
                            />
                        </section>
                    </Col>
                    <Col lg={6} xl={6} className="pt-2 d-flex align-items-center">
                        <Row>
                            <Col md={12}>
                                <StorageFees
                                    priceMonth={priceMonth}
                                    priceWeek={priceWeek}
                                    surtidoPedido={surtidoPedido}
                                    piezaAdicional={piezaAdicional}
                                    embalaje={embalaje}
                                    pricePieceOrLotWeek={pricePieceOrLotWeek}
                                    pricePieceOrLotMonth={pricePieceOrLotMonth}
                                />
                            </Col>
                            <Col md={12}>
                                <section className="p-5"></section>
                            </Col>
                        </Row>
                    </Col>
                </Row>

                {/* PASO TRES */}
                <br />
                <Row className="mt-4 section-storage" onClick={() => { setChangeInStorage(true) }}>
                    <Col lg={6} xl={6} id="contenido-envios" className="pt-2">
                        <section style={{ width: "100%" }}>
                            <CostoEnvio
                                setPriceWeek={setPriceWeek}
                                setPriceMonth={setPriceMonth}
                                setPricePieceOrLotWeek={setPricePieceOrLotWeek}
                                setPricePieceOrLotMonth={setPricePieceOrLotMonth}
                                setDataStorage={setDataStorage}
                            />
                        </section>
                    </Col>
                    <Col lg={6} xl={6} className="pt-2 d-flex align-items-center">
                        <Row>
                            <Col md={12}>
                                <TarifasEnvio
                                    priceMonth={priceMonth}
                                    priceWeek={priceWeek}
                                    precioEnvio={precioEnvio}
                                    pricePieceOrLotWeek={pricePieceOrLotWeek}
                                    pricePieceOrLotMonth={pricePieceOrLotMonth}
                                />
                            </Col>
                            <Col md={12}>
                                <section className="p-4">
                                    <SendRate
                                        id='tarifasEnvio'
                                        typeSelect={typeSelect}
                                        block_checkFF={false}
                                        ff_price={price}
                                        ff_total={total}
                                        storage_priceMonth={priceMonth}
                                        storage_priceWeek={priceWeek}
                                        storage_pricePieceOrLotWeek={pricePieceOrLotWeek}
                                        storage_pricePieceOrLotMonth={pricePieceOrLotMonth}
                                        dataStorage={dataStorage}
                                        dataFulFillment={dataFulFillment}
                                        changeInFF={changeInFF}
                                        changeInStorage={changeInStorage}
                                        setChangeInFF={setChangeInFF}
                                        setChangeInStorage={setChangeInStorage}

                                        step1Sku={step1Sku}
                                        step1PromArticulos={step1PromArticulos}
                                        step1ArticulosInventario={step1ArticulosInventario}
                                        step1ValPromArticulos={step1ValPromArticulos}

                                        step2ArticulosPedido={step2ArticulosPedido}
                                        step2PedidosDia={step2PedidosDia}

                                        recepcionPieza={recepcionPieza}
                                        almacenajeSemana={almacenajeSemana}
                                        surtidoPedido={surtidoPedido}
                                        piezaAdicional={piezaAdicional}
                                        embalaje={embalaje}
                                        precioEnvio={precioEnvio}
                                    />
                                </section>
                            </Col>
                        </Row>
                    </Col>
                </Row>

                <FixedRates />
                <FixedReceptionRates />
                <BlueForm />
                {
                    dataSuccessStories.length > 0 ? <CarouselComments dataSuccessStories={dataSuccessStories} /> : null
                }

            </div>
        </React.Fragment >
    )

}


/**
 * Enviamos la cotizacíon de fulfillment y/o tarifas y almacenaje, en el caso que se haya detectado algún
 * cambio
 * @param {*} props 
 * @returns 
 */
const SendRate = (props) => {

    const { dataFulFillment, dataStorage, ff_price, ff_total, block_checkFF, id, typeSelect, setChangeInFF, setChangeInStorage } = props
    const { storage_priceMonth, storage_priceWeek, storage_pricePieceOrLotWeek, storage_pricePieceOrLotMonth,
        step1Sku, step1PromArticulos, step1ArticulosInventario, step1ValPromArticulos, step2ArticulosPedido, step2PedidosDia, recepcionPieza, almacenajeSemana, surtidoPedido, piezaAdicional, embalaje, precioEnvio } = props;

    const emailRexes = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    const [email, setEmail] = useState('')
    const [emaiValid, setEmailValid] = useState(true);
    const [sendRateEmail, setSendRateEmail] = useState(false);
    const [error, setError] = useState(false);
    const [msjSuccess, setMsjSuccess] = useState(false);

    const sendRate = async () => {

        if (emailRexes.test(email)) {
            try {
                setSendRateEmail(true)
                props.email = email;
                props.apiBase = apiBase;
                console.log("Enviando", props)

                await axios.post(urlSendMail, props, {
                    headers: {
                        'content-type': 'application/json',
                        'X-WP-NONCE': appLocalizer.nonce
                    }
                });

                setMsjSuccess(true);
                setTimeout(() => {
                    setMsjSuccess(false);
                }, 3000);

                setSendRateEmail(false);
                setEmail('');
            } catch (e) {
                setError(true);
                setSendRateEmail(false);
                setTimeout(() => { setError(false); }, 3000);
                console.log("Error mail", e);
            }
            setChangeInFF(false);
            setChangeInStorage(false);
        } else {
            setEmailValid(false);
            setTimeout(() => {
                setEmailValid(true);
            }, 3000);
        }

    }

    return (
        <React.Fragment>

            <Form.Row className="align-items-center">
                <Col lg={12} xl={12}><Form.Label className="sendRate"> Recibe la cotización por mail </Form.Label></Col>
                <Col lg={9} xl={9} className="pb-2">
                    <Form.Control
                        type="email"
                        className="p-0 input-line_mail"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="Correo electrónico"
                    />
                    {!emaiValid ? <label style={{ color: "red" }}>El correo electrónico no es valido.</label> : null}
                    {error ? <label style={{ color: "red" }}>Ocurrio un error al enviar tu cotización</label> : null}
                    {msjSuccess ? <label style={{ color: "#0B296B" }}>La cotización se envió de manera exitosa</label> : null}
                </Col>
                <Col lg={3} xl={3} className="pb-2">
                    {!sendRateEmail ?
                        <Button className="btn-block btn-sendRate" onClick={sendRate}> Enviar</Button> :
                        <Button className="btn-block btn-sendRate" onClick={sendRate}>
                            Enviando
                            <Spinner
                                className="ml-2"
                                as="span"
                                animation="border"
                                size="sm"
                                role="status"
                                aria-hidden="true" />
                        </Button>
                    }
                </Col>
            </Form.Row>

        </React.Fragment>
    );

}

export default Settings;