import React, { useState, useEffect } from "react";
import { Col, Row, Button, Spinner } from "react-bootstrap";
import "../style_react.css";
import "./style_header.css";
import almacenaje from "../../images/almacenaje.png";
import maniobra from "../../images/maniobra.png";
import nacionales from "../../images/nacionales.png";
const Header = (props) => {
	const { dataAditional } = props;
	return (
		<React.Fragment>
			<Row className="sub-title-section text-center">
				<Col className="col-md-12">
					<section className="">
						<label className="title-main-pikkop m-0">PIKKOP <span className='title2-main-pikkop'>AIR</span></label>
					</section>
					<section>
						<label className="subtitle-main-pikkop m-0">
							¡Automatiza la logística de tu negocio!
						</label>
						<br />
						<div className="text-center">
							<label className="main-pikkop-subtitle m-0">
								Lleva tu e-commerce como los grandes, crece rápido y seguro
							</label>
						</div>
						<br />
					</section>
				</Col>
			</Row>

			<Row className="sub-title-section">
				<Col className="col-md-12 rowSinMargin paddingLeft0 paddingRight0">
					<div className="col-sm-12 col-md-12 col-lg-7">
						{dataAditional.url_video ?
							<div className="h_iframe">
								<iframe
									src={dataAditional.url_video}
									frameborder="0"
									allowfullscreen
								/>
							</div> :
							<div className="h_iframe d-flex align-items-center">
								<div style={{ width: 'inherit' }}>
									<div className="d-flex justify-content-center">
										<Spinner animation="grow" />
									</div>
								</div>
							</div>
						}
					</div>
					<Col className="col-xs-12 col-sm-12 col-md-12 col-lg-5 paddingRight0 px-4">

						<div className="row costo-desde">Costo desde</div>
						<div className="row type_card">
							<Col xl={12} className="col-md-12" style={{ paddingLeft: "0", marginBottom: "10px" }}
							>
								<img style={{ width: "8%" }} src={almacenaje} />
								<label className="title-card-main-pikkop m-0">
									Almacenaje
								</label>
							</Col>
							<Col className="col-md-7">
								<Col className="col-md-12">
									<label className="main-pikkop-PrecioCard">${dataAditional.p_almacenaje} pesos</label>
									<br />
									<label className="main-pikkop-miniTexCard m-0">
										semanales por posición 90x60x45{" "}
									</label>
								</Col>
							</Col>
							<Col className="col-md-5 content-center">
								<a style={{ textDecoration: 'none' }} href="#contenido-almacenaje">
									<Button className="btn-block btn-meInteresa">Me interesa</Button>
								</a>
							</Col>
						</div>

						<div className="row costo-desde">Costo desde</div>
						<div className="row type_card">
							<Col className="col-md-7 rowSinMargin">
								<Col
									className="col-md-12"
									style={{
										paddingLeft: "0",
										marginBottom: "10px",
										paddingRight: "0px",
									}}
								>
									<img style={{ width: "20%" }} src={maniobra} />
									<label className="title-card-main-pikkop m-0">
										Maniobra de pedido
									</label>
								</Col>
								<Col className="col-md-12 row">
									<Col className="col-md-6">
										<label className="main-pikkop-PrecioCard">${dataAditional.p_maniobra} </label>
									</Col>
									<Col className="col-md-6">
										<label className="main-pikkop-miniTexCard m-0 mt-2">
											Por pieza
										</label>
										<br />
									</Col>
								</Col>
							</Col>
							<Col className="col-md-5 content-center">
								<a style={{ textDecoration: 'none' }} href="#contenido-maniobra">
									<Button className="btn-block btn-meInteresa">Me interesa</Button>
								</a>
							</Col>
						</div>

						<div className="row costo-desde">Costo desde</div>
						<div className="row type_card">
							<Col className="col-md-7">
								<Col
									className="col-md-12"
									style={{
										paddingLeft: "0",
										marginBottom: "10px",
										paddingRight: "0px",
									}}
								>
									<img style={{ width: "20%" }} src={nacionales} />
									<label
										className="title-card-main-pikkop m-0"
										style={{ fontSize: "12pt" }}
									>
										Envíos nacionales desde
									</label>
								</Col>
								<Col className="col-md-12">
									<label className="main-pikkop-PrecioCard">${dataAditional.p_envios} pesos</label>
								</Col>
							</Col>
							<Col className="col-md-5 content-center">
								<a style={{ textDecoration: 'none' }} href="#contenido-envios">
									<Button className="btn-block btn-meInteresa"> Me interesa</Button>
								</a>
							</Col>
						</div>
					</Col>
				</Col>
			</Row>
		</React.Fragment>
	);
};

export default Header;
