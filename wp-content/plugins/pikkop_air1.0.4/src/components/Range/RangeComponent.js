import React, { useState, useEffect } from 'react';
import { Col, Row } from 'react-bootstrap';
import ReactTooltip from 'react-tooltip';
import '../style_react.css'
import './style_range.css'
const RangeComponent = (props) => {
    const { title, minValue, maxValue, step, setValueInMain, unitType } = props;

    const [value, setValue] = useState(minValue)
    const [progressRange, setProgressRange] = useState('-webkit-gradient(linear, left top, right top, color-stop(0%, #212749), color-stop(0%, #f0f1f0))')

    const handleChange = (event) => {
        const value = event.target.value
        if (value >= minValue && value <= maxValue) {
            setValue(value);
            setValueInMain(value)
            let progress = calculateProgress(value);
            setProgressRange(`-webkit-gradient(linear, left top, right top, color-stop(${progress}%, #212749), color-stop(${progress}%, #f0f1f0))`)
        }
    }

    const calculateProgress = (_value) => {
        return ((_value - minValue) / (maxValue - minValue)) * 100;
    }

    const formatoMexico = (number) => {
        const exp = /(\d)(?=(\d{3})+(?!\d))/g;
        const rep = '$1,';
        return number.toString().replace(exp, rep);
    }


    return (
        <React.Fragment>
            <Row className="sub-title-section">
                <Col className="col-md-6">
                    {
                        title === "¿Cuántos SKUs incluye tu inventario?" ? (
                            <label className="title-range__pikkop" data-tip data-for='tooltipSection2' style={{ cursor: "help" }}>{title}</label>
                        ) : (
                            <label className="title-range__pikkop">{title}</label>
                        )
                    }
                </Col>
                <Col className="col-md-6">
                    <div className="text-right">
                        <input
                            className='sub-title-section-value text-right input-form'
                            value={value}
                            type='number'
                            onChange={handleChange}
                            min={minValue}
                            max={maxValue}
                        /><span style={{fontWeight:'bold'}} className='ml-2'>{unitType}</span>
                        {/* <label className="sub-title-section-value text-right input-form">
                            {formatoMexico(value)} {unitType}
                        </label> */}
                    </div>
                </Col>
            </Row>
            <ReactTooltip
                id="tooltipSection2"
                effect="solid"
                place="top"
                // backgroundColor="#000"
                border={false}
                type="dark"
            >
                <span>Es el número de referencia único de un producto, según aparece registrado en el sistema de la empresa. Puede variar por tipo, color o tamaño.</span>
            </ReactTooltip>
            <input
                style={{
                    background: 'transparent',
                    backgroundImage: progressRange
                }}
                id="sliderId"
                className="inputR text-secondary slider"
                name="sliderName"
                type="range"
                min={minValue}
                max={maxValue}
                value={value}
                step={step}
                onChange={handleChange}
            />
        </React.Fragment>
    )

}

export default RangeComponent;