import React, { useState, useEffect } from 'react';
import '../style_react.css'
import './style_carouselComments.css'
import CommentStructure from "./CommentStructure";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

const CarouselComments = (props) => {
    const { dataSuccessStories } = props;
    return (
        <React.Fragment>
            <div className="col-md-12 div-pikkop-comments-tittle">
                <label className="main-pikkop-tittle-comments m-0">Casos de éxito</label>
            </div>
            <Carousel showThumbs={false} showStatus={false} infiniteLoop={true} dynamicHeight={false} autoPlay={true} showIndicators={false} stopOnHover={false} swipeable={true} interval={5100} transitionTime={2000} className="presentation-mode">
                {dataSuccessStories.map((comment, i) => (
                    <CommentStructure key={i} info={comment} />
                ))}
            </Carousel>
        </React.Fragment>
    );
}

export default CarouselComments;