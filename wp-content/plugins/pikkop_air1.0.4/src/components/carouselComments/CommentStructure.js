import React from "react";
import { Col, Row } from "react-bootstrap";
import './style_carouselComments.css'

const imgRouded = {
    borderRadius: "50%",
    width: "200px;",
    height: "150px;",
}

const CommentStructure = ({ info }) => {
    return (
        <Row className="my-slide content">
            <Col xs={12} lg={3} md={3} className="col-md-3">
                <img style={{
                    borderRadius: "50%",
                    width: "200px",
                    height: "200px"
                }} src={info.image} />
            </Col>
            <Col className="col-md-6 vl">
                <label className="title-main-person m-0">{info.person_name}</label><br />
                <label className="main-pikkop-company m-0">{info.company_name}</label><br />
                <label className="main-pikkop-comments m-0">“{info.description}”</label>
            </Col>
        </Row>
    );
};

export default CommentStructure;
