import React, { useState, useEffect } from 'react';
import ReactTooltip from 'react-tooltip';
import RangeComponent from '../Range/RangeComponent';

const Fulfillment = (props) => {

    const { setStep1Sku, setFF_SKUOrder, setStep1ArticulosInventario, setStep1PromArticulos, FF_avWeightOrder, setStep1ValPromArticulos } = props;

    const [stepAvWeightOrder, setStepAvWeightOrder] = useState(0.25)
    const [minValueAvWeightOrder, setMinValueAvWeightOrder] = useState(0.25)

    /**
     * Cambia el rango en el se irá incrementando la recta
     * si el parametro value es > 1 el rango incrementara en una unidada, si es <= a 1
     * incrementara en 0.25
     * @param {*} value 
     */
    const setAvWeightOrder_and_changeStep = (value) => {
        if (FF_avWeightOrder >= 1) {
            setStepAvWeightOrder(1);
            setMinValueAvWeightOrder(1)

        }
        if (Number(value) < 1 || Number(value) === 1) {
            setStepAvWeightOrder(0.25);
            setMinValueAvWeightOrder(0.25)
        }
        setStep1PromArticulos(value);
    }

    return (
        <React.Fragment>
            <label className="title-section">
                <div className="title-section">
                    <div className="title-section-number">
                        1
                    </div>
                    <span className="title-main-sections">Almacenaje</span>
                </div>
            </label>

            <RangeComponent title="¿Cuántos SKUs incluye tu inventario?"
                minValue={1}
                maxValue={1000}
                // step={100}
                unitType={""}
                setValueInMain={setStep1Sku}
            />
            <RangeComponent title="¿Peso promedio de tus artículos?"
                minValue={minValueAvWeightOrder}
                maxValue={5}
                step={stepAvWeightOrder}
                unitType={"kg"}
                setValueInMain={setAvWeightOrder_and_changeStep}
            />
            <RangeComponent title="¿Cuántos artículos tendrás por inventario?"
                minValue={1}
                maxValue={1000}
                step={1}
                setValueInMain={setStep1ArticulosInventario}
            />
            <RangeComponent title="¿Valor promedio de tus artículos?"
                minValue={1}
                maxValue={1000}
                // step={100}
                unitType={""}
                setValueInMain={setStep1ValPromArticulos}
            />
        </React.Fragment>
    )

}

export default Fulfillment;