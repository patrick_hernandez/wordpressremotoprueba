import React, { useEffect, useState } from 'react';
import { Col, Form, Row } from 'react-bootstrap';
import calculationStorage from "../../JSON/dataCalculationStorage.json";
const CostoEnvio = (props) => {

    const { setPriceWeek, setPriceMonth, setPricePieceOrLotMonth, setPricePieceOrLotWeek, setDataStorage } = props;


    const [totalInventory, setTotalInventory] = useState(200);
    const [totalStock, setTotalStock] = useState(10);
    const [rateType, setRateType] = useState('pieza');

    const [unitPerLot, setUnitPerLot] = useState(1);
    const [totalCaseInventory, setTotalCaseInventory] = useState(0);

    const [largo, setLargo] = useState(20);
    const [altura, setAltura] = useState(20);
    const [ancho, setAncho] = useState(20);
    const [peso, setPeso] = useState(20);

    const [productSend, setProductSend] = useState(10);
    const [typeProductSales, setTypeProductSales] = useState('');

    const [volume, setVolume] = useState(0);
    const [weight, setWeight] = useState(0);
    const [LowerValue, setLowerValue] = useState(0);

    useEffect(() => {
        selectLowerValueToDeterminePrice();
    }, [largo, ancho, altura]);

    useEffect(() => {
        selectLowerValueToDeterminePrice();
    }, [peso, typeProductSales, productSend,]);

    useEffect(() => {
        selectLowerValueToDeterminePrice();

        if (rateType === 'pieza') {
            setTotalCaseInventory(totalInventory)
            setUnitPerLot(1)
        }

    }, [rateType, totalInventory, totalCaseInventory, totalStock]);

    const calculateVolume = () => {
        let vol = (Number(largo) / 100) * (Number(altura) / 100) * (Number(ancho) / 100);
        return calculationStorage.maxVolAllowed / vol;

    }

    const calculateWeight = () => {
        return calculationStorage.maxWeightAllowed / Number(peso);
    }

    /**
     * Valida el valor más bajo para determinar el precio que se utiliazara
     */
    const selectLowerValueToDeterminePrice = () => {
        let vol = calculateVolume();
        let _weight = calculateWeight();
        setVolume(vol);
        setWeight(_weight);
        calculateStoragePrice(vol > _weight ? _weight : vol)
        setLowerValue(vol > _weight ? _weight : vol);
        buildData();
    }

    /**
     * Calcula el precio del almacenamiento 
     * @param {*} dataSelect 
     */
    const calculateStoragePrice = (dataSelect) => {
        let priceWeek = calculationStorage.storageByWeek / dataSelect;
        let priceMonth = calculationStorage.storageByMonth / dataSelect;
        setPriceWeek(priceWeek);
        setPriceMonth(priceMonth);
        calculatePriceByPieceOrLot(priceWeek, priceMonth);
    }

    /**
     * Calcula el precio de lote según sea el tipo de tarifa seleccionada
     * @param {*} priceWeek 
     * @param {*} priceMonth 
     */
    const calculatePriceByPieceOrLot = (priceWeek, priceMonth) => {
        if (rateType === 'pieza') {
            let totalPricePieceWeek = totalInventory * priceWeek;
            let totalPricePieceMonth = totalInventory * priceMonth;
            setPricePieceOrLotWeek(totalPricePieceWeek)
            setPricePieceOrLotMonth(totalPricePieceMonth);
        }
        if (rateType === 'lote') {
            let totalPricelotWeek = totalCaseInventory * priceWeek;
            let totalPricelotMonth = totalCaseInventory * priceMonth;
            setPricePieceOrLotWeek(totalPricelotWeek);
            setPricePieceOrLotMonth(totalPricelotMonth);
        }
    }

    /**
     * Construye un json de la cotización de almacenaje y maniobra de recibo para ser enviado por mail
     */
    const buildData = () => {
        setDataStorage({
            totalInventory,
            totalStock,
            rateType,
            unitPerLot,
            totalCaseInventory,
            largo,
            altura,
            ancho,
            peso,
            productSend,
            typeProductSales
        });
    }

    return (
        <React.Fragment>
            <label className="title-section">
                <div className="title-section">
                    <div className="title-section-number">
                        3
                    </div>
                    <span className="title-main-sections">Costo de envío</span>
                </div>
            </label>
            
            <Form>
                <Form.Group>
                    <Form.Label className="mr-3">¿Qué producto manejas?</Form.Label><br />
                    <Form.Control
                        type="text"
                        value={typeProductSales}
                        placeholder=""
                        onChange={(e) => setTypeProductSales(e.target.value)}
                        className="p-0 input-line_mail"
                    />
                </Form.Group>
            </Form>

        </React.Fragment>
    )
}


const SelectTypeRate = (props) => {

    const { unitPerLot, setUnitPerLot, setRateType, setTotalCaseInventory, totalCaseInventory, totalInventory } = props
    const [optionSelect, setOption] = useState('pieza')
    const [boolPieza, setBoolPieza] = useState(true)
    const [boolLote, setBoolLote] = useState(false);

    const onSelectType = (type) => (e) => {
        setOption(type);
        setRateType(type);
        setBoolPieza(!boolPieza);
        setBoolLote(!boolLote);
    }

    return (
        <React.Fragment>
            <div className="mb-3">
                <label>¿Buscas tarifa por pieza o por lote?</label><br />
                <Form.Check className="title-section-title ml-2" defaultChecked={boolPieza} onClick={onSelectType("pieza")} inline label="Por pieza" name="typeRate" type="radio" />
                <Form.Check className="title-section-title ml-2" defaultChecked={boolLote} onClick={onSelectType("lote")} inline label="Por lote" name="typeRate" type="radio" />
            </div>
            {
                optionSelect != undefined && optionSelect != 'pieza' ? <div className="content-rate">
                    <ContenRate
                        unitPerLot={unitPerLot}
                        setUnitPerLot={setUnitPerLot}
                        totalCaseInventory={totalCaseInventory}
                        totalInventory={totalInventory}
                        setTotalCaseInventory={setTotalCaseInventory} />
                </div> : null
            }

        </React.Fragment>
    )

}

const ContenRate = (props) => {

    const { unitPerLot, setUnitPerLot, setTotalCaseInventory, totalCaseInventory, totalInventory } = props;

    const calculateTotalCaseInventory = (e) => {
        const { value } = e.target;
        setUnitPerLot(value);
        let _totalCaseIntentory = Number(totalInventory) / Number(value);
        setTotalCaseInventory(Number(_totalCaseIntentory.toFixed(2)));
    }

    useEffect(() => {
        let _totalCaseIntentory = Number(totalInventory) / Number(unitPerLot);
        setTotalCaseInventory(Number(_totalCaseIntentory.toFixed(2)));
    }, [totalInventory]);

    return (
        <React.Fragment>
            <Row>
                <Col md={7} sm={8} className="text-center">
                    <Form.Label >¿Cuántas unidades por lote? </Form.Label>
                </Col>
                <Col md={4} sm={3}>
                    <Form.Control
                        size="sm"
                        type="number"
                        min="1"
                        value={unitPerLot}
                        onChange={calculateTotalCaseInventory}
                        className="p-0 mr-1 ml-1 input-line text-right sub-title-section sub-title-section-value"
                    />
                </Col>
                <Col md={7} sm={8} className="text-center">
                    <Form.Label >¿Cuántos lotes en inventario? </Form.Label>
                </Col>
                <Col md={4} sm={3}>
                    <Form.Control
                        size="sm"
                        type="number"
                        min="1"
                        onChange={(e) => console.log("Calculated..")}
                        value={totalCaseInventory}
                        className="p-0 mr-1 ml-1 input-line text-right sub-title-section sub-title-section-value"
                    />
                </Col>
            </Row>
        </React.Fragment>
    )
}

const RangeDetails = (props) => {
    const { title, min, max, value, onChange, step, typeUniy } = props;

    const [progressRange, setProgressRange] = useState('-webkit-gradient(linear, left top, right top, color-stop(0%, #0B296B), color-stop(0%, #f0f1f0))')


    useEffect(() => {
        let progress = calculateProgress(value);
        setProgressRange(`-webkit-gradient(linear, left top, right top, color-stop(${progress}%, #0B296B), color-stop(${progress}%, #f0f1f0))`)
    }, []);

    const handleChange = (event) => {
        const _value = event.target.value
        onChange(_value)
        let progress = calculateProgress(_value);
        setProgressRange(`-webkit-gradient(linear, left top, right top, color-stop(${progress}%, #0B296B), color-stop(${progress}%, #f0f1f0))`)

    }

    const calculateProgress = (_value) => {
        return ((_value - min) / (max - min)) * 100;
    }

    return (
        <React.Fragment>
            <td width="15%" className="sub-title-section title-section-title table-border-any">{title}</td>
            <td width="65%" className="table-border-any">
                <input
                    style={{
                        background: 'transparent',
                        backgroundImage: progressRange
                    }}
                    className="inputR"
                    name="sliderName"
                    type="range"
                    min={min}
                    max={max}
                    value={value}
                    step={step}
                    onChange={handleChange}
                /></td>
            <td width="20%" className="table-border-any text-right" style={{ fontWeight: "bold" }}>{value} {typeUniy}</td>
        </React.Fragment>
    )

}


export default CostoEnvio;