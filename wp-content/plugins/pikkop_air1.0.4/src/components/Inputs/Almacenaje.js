import React from 'react';
import RangeComponent from '../Range/RangeComponent';

const Almacenaje = (props) => {

    const { setStep2ArticulosPedido, setStep2PedidosDia } = props; 

    return (
        <React.Fragment>
            <label className="title-section">
                <div className="title-section">
                    <div className="title-section-number">
                        2
                    </div>
                    <span className="title-main-sections">Maniobra</span>
                </div>
            </label>

            <RangeComponent
                title="¿Cuantos artículos se incluyen por pedido?"
                minValue={1}
                maxValue={5}
                step={1}
                setValueInMain={setStep2ArticulosPedido}
            />
            <RangeComponent
                title="¿Cuántos pedidos esperas procesar al día?"
                minValue={1}
                maxValue={1000}
                step={1}
                setValueInMain={setStep2PedidosDia}
            />
        </React.Fragment>
    )
}

export default Almacenaje;