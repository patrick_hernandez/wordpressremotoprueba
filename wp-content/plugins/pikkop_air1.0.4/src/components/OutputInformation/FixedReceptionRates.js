import React, { useState, useEffect } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import '../style_react.css'

const FixedReceptionRates = () => {

    return (
        <React.Fragment>
            <section className="text-center pt-4 ">
                <label className="main-pikkop-subtitle-tarifas">Tarifas fijas de recepción</label>
                <Row className="main-pikkop-subtitle main-pikkop-frr m-0 d-flex justify-content-center">
                    <Col xs={8} xl={7} md={7} className="">
                        <label className="m-0">
                            Recepción detallada por pieza
                            <br/><span style={{fontSize:"14px"}}>(Pesamos la caja y los artículos)</span>
                        </label>
                    </Col>
                    <Col xs={4} xl={3} md={3} className="text-left">
                        <label >
                            $3.00
                        </label>
                    </Col>
                    <Col xs={8} xl={7} md={7} className="mt-2">
                        <label>
                            Recepción por lote sin revisión
                        </label>
                    </Col>
                    <Col xs={4} xl={3} md={3} className="mt-2 text-left">
                        <label >
                            $15.00
                        </label>
                    </Col>
                </Row>
            </section>
        </React.Fragment>
    )

}

export default FixedReceptionRates;