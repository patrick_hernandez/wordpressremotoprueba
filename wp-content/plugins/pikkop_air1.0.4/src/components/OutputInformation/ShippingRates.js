import React from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import ReactTooltip from 'react-tooltip';
import '../style_react.css'

const ShippinngRates = (props) => {

    const { recepcionPieza, almacenajeSemana } = props;

    return (
        <React.Fragment>
            <Row>
                <Col md={12}>
                    <Card style={{ boxShadow: '0px 0px 12px #1515154D' }}>
                        <Card.Body className="p-5">
                            <Card.Title className="title-card-almacenaje" style={{ textAlign: 'center', marginBottom: "30px" }}>Almacenaje por pieza</Card.Title>
                            <Row className="subtitle-output-info mb-3 ">

                                <Col xs={7} xl={6} md={6} className="text-main-cards" data-tip data-for='tooltipSection1' style={{cursor: "help"}}>Recepción a detalle por pieza</Col>
                                <Col xs={5} xl={4} md={6} className="text-right sub-title-section-value">
                                    {
                                        <label className="m-0">$ {recepcionPieza !== undefined ? Number(recepcionPieza).toFixed(2) : '0.00'}</label>
                                    }
                                </Col>
                                <Col xs={12} xl={12} md={12}>
                                    <div className=" div-limit"></div>
                                </Col>
                            </Row>
                            <ReactTooltip
                                id="tooltipSection1"
                                effect="solid"
                                place="top"
                                // backgroundColor="#000"
                                border={false}
                                type="dark"
                            >
                                <span>Es el costo por contabilizar cada una de las piezas al momento de recibir el inventario, corroborando que no haya piezas faltantes o dañadas.</span>
                            </ReactTooltip>

                            <Row className="subtitle-output-info mb-3 ">
                                <Col xs={7} xl={6} md={6} className="text-main-cards">Almacenaje por pieza por semana </Col>
                                <Col xs={5} xl={4} md={6} className="text-right sub-title-section-value">
                                    {
                                        <label className="m-0">$ {almacenajeSemana !== undefined ? Number(almacenajeSemana).toFixed(2) : '0.00'}</label>
                                    }
                                </Col>
                            </Row>

                            <Row className="subtitle-output-info">
                                <Col xs={12} xl={12} md={12}>
                                    {/* NOTA: Aplica peso volumétrico y peso real. */}
                                    <label className="text-card-almacenaje m-0">NOTA: </label>
                                    <label className="text-card-almacenaje-2 m-0" style={{ paddingLeft: "7px" }}>Aplica peso volumétrico y peso real.</label>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </React.Fragment>
    )
}

export default ShippinngRates;