import React, { useState, useEffect } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import cube from '../../images/cube.svg'
import '../style_react.css'

const FixedRates = () => {

    return (
        <React.Fragment>
            <section className="text-center pt-4 block-tarifas">
                <label className="main-pikkop-title m-0 mt-2">Tarifas de almacenaje</label>
                <div className="text-center subtitle-tarifas">
                    <label className="main-pikkop-subtitle m-0">
                        Nuestras tarifas dependen del espacio que necesites 
                        para almacenar tus productos.
                    </label>
                </div>
                <br />
            </section>
            <section className="pt-0 div-limit">
                <InfoRate />
            </section>

        </React.Fragment>
    )

}

const InfoRate = () => {

    return (
        <React.Fragment>
            <table className="table-border-any-cubes">
                <tbody>
                    <tr>
                        <td className="table-border-any table-hidden text-center">
                            <img className="icon-pikkop cube-sm" src={cube} /><br />
                            <label className="title-section-tarifas title-seccion-table">Chico</label>
                        </td>
                        <td className="table-border-any text-center">
                            <img className="icon-pikkop cube-sm" src={cube} /><br />
                            <label className="title-section-tarifas title-seccion-table">Chico</label>
                        </td>
                        <td className="table-border-any text-center">
                            <img className="icon-pikkop cube-nor" src={cube} /><br />
                            <label className="title-section-tarifas title-seccion-table">Mediano</label>
                        </td>
                        <td className="table-border-any text-center">
                            <img className="icon-pikkop cube-xl" src={cube} /><br />
                            <label className="title-section-tarifas title-seccion-table">Grande</label>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table className="table-border-any">
                <tbody>
                    <tr>
                        <td className="table-border-any title-section-table text-right"><span className="title-tab">Tamaño</span></td>
                        <td className="table-border-any title-section-title">80x30x45</td>
                        <td className="table-border-any title-section-title">80x60x45</td>
                        <td className="table-border-any title-section-title">90x60x45</td>
                    </tr>
                    <tr>
                        <td className="table-border-any title-seccion-table text-right"><span className="title-tab">Semanal</span></td>
                        <td className="table-border-any title-section-title">$40.00</td>
                        <td className="table-border-any title-section-title">$45.00</td>
                        <td className="table-border-any title-section-title">$50.00</td>
                    </tr>
                    <tr>
                        <td className="table-border-any title-seccion-table text-right"><span className="title-tab">Mensual</span></td>
                        <td className="table-border-any title-section-title">$135.00</td>
                        <td className="table-border-any title-section-title">$140.00</td>
                        <td className="table-border-any title-section-title">$146.00</td>
                    </tr>
                </tbody>
            </table>
        </React.Fragment>
    )

}

export default FixedRates;