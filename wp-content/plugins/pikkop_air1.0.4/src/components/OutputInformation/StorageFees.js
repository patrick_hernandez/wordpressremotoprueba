import React, { useState, useEffect } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import ReactTooltip from 'react-tooltip';
import '../style_react.css'

const StorageFees = (props) => {

    const { priceMonth, priceWeek, pricePieceOrLotWeek, pricePieceOrLotMonth, surtidoPedido, piezaAdicional, embalaje } = props

    const formatoMexico = (number) => {
        const exp = /(\d)(?=(\d{3})+(?!\d))/g;
        const rep = '$1,';
        return number.toString().replace(exp,rep);
    }

    return (
        <React.Fragment>
            <Card style={{ boxShadow: '0px 0px 12px #1515154D', width: "100%" }}>
                <Card.Body className="p-5">
                    {/* <Card.Title className="title-output-info">Tarifas de almacenaje (pieza/lote)</Card.Title> */}
                    <Row className="subtitle-output-info mb-3 ">
                        <Col xs={7} xl={6} md={6} className="text-main-cards" data-tip data-for='tooltipSection2' style={{cursor: "help"}}>Surtido de pedido promedio</Col>
                        <Col xs={5} xl={4} md={6} className="text-right text-info-price">
                            <label className="m-0 sub-title-section-value">$ {formatoMexico(surtidoPedido.toFixed(2))}</label>
                        </Col>
                        
                        <Col xs={12} xl={12} md={12}>
                            <div className=" div-limit"></div>
                        </Col>
                    </Row>
                    <ReactTooltip
                        id="tooltipSection3"
                        effect="solid"
                        place="top"
                        // backgroundColor="#000"
                        border={false}
                        type="dark"
                    >
                        <span>El costo del armado de pedido por el número de piezas promedio. Si las piezas promedio por pedido son 3. Este costo ya incluye todas las piezas.</span>
                    </ReactTooltip>
                    {/* <Card.Title className="title-output-info">Total (pieza/lote)</Card.Title> */}
                    <Row className="subtitle-output-info mb-3 ">
                        <Col xs={7} xl={6} md={6} className="text-main-cards">Pieza adicional</Col>
                        <Col xs={5} xl={4} md={6} className="text-right text-info-price">
                            <label className="m-0 sub-title-section-value">$ {formatoMexico(piezaAdicional.toFixed(2))}</label>
                        </Col>
                        {/* <Col xs={7} xl={6} md={6}>Mensual</Col>
                        <Col xs={5} xl={4} md={6} className="text-right text-info-price">
                            <label className="m-0 sub-title-section-value">$ {formatoMexico(pricePieceOrLotMonth.toFixed(2))}</label>
                        </Col> */}
                        <Col xs={12} xl={12} md={12}>
                            <div className=" div-limit"></div>
                        </Col>
                    </Row>
                    <Row className="subtitle-output-info mb-3 ">
                        <Col xs={7} xl={6} md={6} className="text-main-cards">Embalaje (material)</Col>
                        <Col xs={5} xl={4} md={6} className="text-right text-info-price">
                            <label className="m-0 sub-title-section-value">$ {formatoMexico(embalaje.toFixed(2))}</label>
                        </Col>
                    </Row>
                    {/* <Row className="subtitle-output-info">
                        <Col xs={12} xl={6} md={6} className="text-center title-output-info">Seguro de envío</Col>
                        <Col xs={12} xl={4} md={6} className="text-center text-info-price">
                            <label className="m-0" style={{ fontWeight: "bold", fontSize: "18px" }}>¡Contáctanos!</label>
                        </Col>
                    </Row> */}
                </Card.Body>
            </Card>
        </React.Fragment>
    )

}

export default StorageFees;