import React, { useState, useEffect } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import '../style_react.css'

const TarifasEnvio = (props) => {

    const { precioEnvio } = props

    const formatoMexico = (number) => {
        const exp = /(\d)(?=(\d{3})+(?!\d))/g;
        const rep = '$1,';
        return number.toString().replace(exp, rep);
    }

    return (
        <React.Fragment>
            <Card style={{ boxShadow: '0px 0px 12px #1515154D', width: "100%" }}>
                <Card.Body className="p-5">
                    <Card.Title className="title-cards-envio">Tarifas de envío</Card.Title>
                    <Row className="subtitle-output-info mb-3 ">
                        <Col xs={7} xl={6} md={6} className="text-card-almacenaje-3">Mismo día</Col>
                        <Col xs={5} xl={4} md={6} className="text-right text-info-price">
                            <label className="m-0 sub-title-section-value">$ {formatoMexico(precioEnvio.envioMismoDia.toFixed(2))}</label>
                        </Col>
                    </Row>
                    <Row className="subtitle-output-info mb-3 ">

                        <Col xs={7} xl={6} md={6} className="text-card-almacenaje-3">Estándar</Col>
                        <Col xs={5} xl={4} md={6} className="text-right text-info-price">
                            <label className="m-0 sub-title-section-value">$ {formatoMexico(precioEnvio.envioEstandar.toFixed(2))}</label>
                        </Col>
                    </Row>
                    <Row className="subtitle-output-info mb-3 ">
                        <Col xs={7} xl={6} md={6} className="text-card-almacenaje-3">Día siguiente</Col>
                        <Col xs={5} xl={4} md={6} className="text-right text-info-price">
                            <label className="m-0 sub-title-section-value">$ {formatoMexico(precioEnvio.envioDiaSiguiente.toFixed(2))}</label>
                        </Col>

                    </Row>


                </Card.Body>
            </Card>
        </React.Fragment>
    )

}

export default TarifasEnvio;